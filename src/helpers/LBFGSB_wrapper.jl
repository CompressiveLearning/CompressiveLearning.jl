# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

using LBFGSB

"""
	LBFGSBw 

fcn!(g, x) must compute and store in g ∇f(x), and return f(x)
returns (x*, fcn(x*))
"""
function LBFGSBw( fcn!, x0, lb=-Inf, ub=Inf ; 
		  m=5, factr=1e5, pgtol=1e-8, maxfun=5000, maxiter=5000, verbose=-1 )
 	# n::Ref{Cint}: the dimension of the problem.
	n = length(x0)
	nRef = Ref{Cint}(n)

    # m::Ref{Cint}: the maximum number of variable metric corrections used to define
    # the limited memory matrix
	mRef = Ref{Cint}(m)

    # x::Vector{Cdouble}: an approximation to the solution, of length n.
	x = convert(Vector{Cdouble}, vec(x0))
	if lb==Inf || lb==-Inf
		lb = repeat([lb], outer=(n,))
	end
	if ub==Inf || ub==-Inf
		ub = repeat([ub], outer=(n,))
	end
	lb = vec(lb)
	ub = vec(ub)
	if length(lb) != n || length(ub) != n
		error("Lower and upper bounds must have the same dimension than x0 or be ±Inf.")
	end

    # l::Vector{Cdouble}: the lower bound on x, of length n.
	l = convert(Vector{Cdouble}, lb)
    # u::Vector{Cdouble}: the upper bound on x, of length n.
	u = convert(Vector{Cdouble}, ub)
	if !(length(l)==length(u))
		error("Lower and upper bounds must have the same size. Fill with ±Inf if needed.")
	end

    # nbd::Vector{Cint}: bounds (0 unbounded, 1 lower only, 2 both, 3 upper only)
	cvint = x -> convert(Array{Cint}, x)
	nbd = cvint(isfinite.(l)) + cvint(isfinite.(u)) + convert(Cint,2)*cvint(isinf.(l)) .* cvint(isfinite.(u))
     
    # f::Ref{Cdouble}: the value of the function at x. 
	f = 0.0
	fRef = Ref{Cdouble}(f)
    # g::Vector{Cdouble}: the value of the gradient at x.
	# g = zeros(Cdouble, n)
	g = Array{Cdouble}(undef, n)
    # factr::Ref{Cdouble}: stop when (f^k-f^{k+1})/max{|f^k|,|f^{k+1}|,1}<=factr*epsmch
    # Typically 1.e12 (low accuracy), 1.e7 (moderate), 1.e1
	factr = Ref{Cdouble}(factr)
    # pgtol::Ref{Cdouble}: the iteration will stop when max{|proj g_i | i = 1, ..., n}
    # = pgtol where pg_i is the ith component of the projected gradient.
	pgtol = Ref{Cdouble}(pgtol)
    # wa::Vector{Cdouble}: the working array, length (2mmax + 5)nmax + 12mmax^2 + 2mmax.
	wa = zeros(Cdouble, 2m*n + 5n + 11m*m + 8m)
    # iwa::Vector{Cint}: the integer working array of length 3n
	iwa = zeros(Cint, 3*n)
    # task::Vector{Cuchar}: a working string of characters of length 60 indicating the
    # urrent job when entering and quitting this function.
	task = fill(Cuchar(' '), 60)    # fortran's blank padding
    # iprint::Ref{Cint}: it controls the frequency and type of output generated:
	iprint = Ref{Cint}(verbose)    	# print output at every iteration
     
    # csave::Vector{Cuchar}: a working string of characters of length 60.
	csave = fill(Cuchar(' '), 60)   # fortran's blank padding
    # lsave::Vector{Bool}: a logical working array of dimension 4. if task = NEW_X,
    # he following information is available:
	lsave = zeros(Cint, 4)
    # isave::Vector{Cint}: an integer working array of dimension 44. if task = NEW_X,
    # he following information is available:
	isave = zeros(Cint, 44)
    # dsave::Vector{Cdouble}: a double precision working array of dimension 29. if
	dsave = zeros(Cdouble, 29)

	# ===== Main loop =====

	task[1:5] = b"START"
	# Beginning of the loop
    while true
    	setulb(nRef, mRef, x, l, u, nbd, fRef, g, factr, pgtol, wa, iwa, task, iprint, csave, lsave, isave, dsave)
        if task[1:2] == b"FG" # compute fun & grad
            fRef[] = fcn!(g, x)
        elseif task[1:5] == b"NEW_X"
            if isave[30] ≥ maxiter
                task[1:43] = b"STOP: TOTAL NO. of ITERATIONS REACHED LIMIT"
            elseif isave[34] ≥ maxfun
                task[1:52] = b"STOP: TOTAL NO. of f AND g EVALUATIONS EXCEEDS LIMIT"
            end
        else
        	break
        end
	end
	return (x, fRef[])
end # LBFGSBw

# function rosenbrock!(g, x)
	# n = length(x)

	# # "Compute function value f for the sample problem."
	# f = 0.25 * (x[1] - 1)^2
	# for i = 2:n
		# f = f + (x[i] - x[i-1]^2)^2
	# end
	# f = 4 * f
	
	# # "Compute gradient g for the sample problem."
	# t₁ = x[2] - x[1]^2
	# g[1] = 2 * (x[1] - 1) - 1.6e1 * x[1] * t₁
	# for i = 2:n-1
		# t₂ = t₁
		# t₁ = x[i+1] - x[i]^2
		# g[i] = 8 * t₂ - 1.6e1 * x[i] * t₁
	# end
	# g[n] = 8 * t₁
	# return f
# end

# n = 2
# x0 = zeros(Cdouble, n)
# for i = 1:n
    # x0[i] = 3e0
# end
# l = zeros(Float64, n)    # the lower bounds
# u = zeros(Float64, n)    # the upper bounds
# # "First set bounds on the odd-numbered variables."
# for i = 1:2:n
    # l[i] = 1e0
    # u[i] = 1e2
# end
# # "Next set bounds on the even-numbered variables."
# for i = 2:2:n
    # l[i] = -1e2
    # u[i] = 1e2
# end

# (xf, res) = LBFGSBw( rosenbrock!, x0, l, u )
# print("final value: ",res," in: ",xf," \n")

