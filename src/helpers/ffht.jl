# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

"""
Fast Walsh-Hadamard on each column of X. size(X,1) must be a power of 2.
"""
function ffht!(X)
	d = size(X,1)
	l = log2(d)
	n = size(X,2)
	res = ccall(
		# (:fht_double_batch, libfht),
		(:fht_double_columns, libfht),
		Cint,
		(Ref{Cdouble}, Cint, Cint),
		X, l, n
	)
	X .= X./d
end
