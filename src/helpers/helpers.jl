# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

#######################################################################
#                           Linear Algebra                            #
#######################################################################

bv(i, d) = [i==j for j in 1:d]
sqnorm(v::Vector) = real(v'*v)
colsqnorms(A) = sum(A.^2, dims=1)
rowsqnorms(A) = sum(A.^2, dims=2)
colnorms(A) = sqrt.(sum(A.^2, dims=1))
rownorms(A) = sqrt.(sum(A.^2, dims=2))
function sorteigen(evals::Vector{T}, evecs::Matrix{T}; rev=true) where {T<:Real}
    p = sortperm(evals, rev=rev)
    evals[p], evecs[:, p]
end
# TODO replace by efficient implem
"""
	trunc_eigen(M, r)
First eigenvalues/eigenvecs of a symmetric matrix by decreasing eigenvalue magnitude. Should be replaced by efficient truncated algorithms in the future, but these are sometimes slower…
If M is not symmetric, only the symmetric part is considered.
"""
function trunc_eigen(M, r)
	Σ, U = eigen(Symmetric(M))
	sΣ, sU = sorteigen(Σ, U)
	return sΣ[1:r], sU[:, 1:r]
end

#######################################################################
#                          Security checks                            #
#######################################################################

"""
$(SIGNATURES)
Returns a boolean indicating whether `x` contains a `NaN`.
"""
contains_no_nan(x) = all(.~isnan.(x))

#######################################################################
#               Trigo (can be overwritten if using VML)               #
#######################################################################

vcis(x) = cis.(x)
vcis!(o, x) = o .= cis.(x)
cis_provider() = :std

function reimviews(m)
    mfl = reinterpret(Float64, m)
    mre = @view mfl[1:2:end-1,:]
    mim = @view mfl[2:2:end,:]
    mre, mim
end

#######################################################################
#                            Interpolation                            #
#######################################################################

"""
Draw nsamples samples w.r.t. the distribution density pdf.
Works by interpolating the inverse cumulative density.
"""
function drawfrompdf(pdf, x, nsamples::Int)
	pdx = pdf.(x)
	cdx = cumsum(pdx)./sum(pdx)
	@info "interp $nsamples pts"
	df = DataFrame(cdx=cdx, x=x)
	cdf = combine(groupby(df, :cdx), :x => first => :x)
	# Inverse transform sampling
	itp = linear_interpolation(cdf.cdx, cdf.x) 
	itp(rand(nsamples))
end

#######################################################################
#                             Versionning                             #
#######################################################################

"""
githash()
Returns the short hash of the last commit, or an empty string.
"""
function githash(pkgname)
	gitdir = joinpath(pkgdir(pkgname), ".git")
	pdir = ["--git-dir", gitdir]
	safetycheck = replace(read(`git $pdir rev-parse --is-inside-work-tree`, String), "\n"=>"")=="true"
	hash = if safetycheck
		replace(read(`git $pdir rev-parse --short HEAD`, String), "\n"=>"")
	else 
		@warn "Called gitcommit() outside of a git repository."
		""
	end
end

pkgdir(pkgname) = dirname(dirname(Base.find_package(pkgname)))

#######################################################################
#                       Pre-allocated supports                        #
#######################################################################

"""
Type representing a collection of atoms. 
Can deal with growing number of columns by preallocating memory.
"""
mutable struct PreallocatedSupport{T}
	S::Matrix{T}
	k 			# Current size
	function PreallocatedSupport{T}(d1, d2) where T
		new(Matrix{T}(undef, d1, d2), 0)
	end
end
currentview(s) = (@view s.S[:,1:s.k])
function addatom!(s::PreallocatedSupport, atom)
	s.k += 1
	s.S[:,s.k] = atom
	currentview(s)
end
function removeatom!(s::PreallocatedSupport, idx)
	if (idx != s.k) s.S[:, idx] = s.S[:, s.k] end 	# Fill the gap with the k-th column
	s.k -= 1
	currentview(s)
end

#######################################################################
#                             Projections                             #
#######################################################################

function proj_simplex(y)
	# Adapted from https://arxiv.org/pdf/1101.6081.pdf (Projection Onto A Simplex, Y. Chen & X. Ye)
	n = length(y)
	s = sort(y)
	for i in n-1:-1:1
		tᵢ = (sum(s[i+1:n])-1)/(n-i)
		if tᵢ ≥ s[i]
			return max.(y .- tᵢ, 0) end
	end
	return max.(y .- (sum(y)-1)/n, 0)
end

#######################################################################
#                                Types                                #
#######################################################################

function size_eq_reals(t)
	if t <: Real 
		1.0
	elseif t <: Complex 
		2.0
	else 
		@error "Sketching with element type $eltype whose size is unknown. Try using 'm_factor' rather than 'm_factor_eq_real'."
	end
end
