# This file is part of CompressiveLearning.jl, a Julia package for 
# machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

#################
#  Square Wave  #
#################

"""
$(TYPEDEF)
# Fields
$(TYPEDFIELDS)
"""
struct QuantizedFourierSkOp{T<:AbstractArray{Float64,2}, U<:Union{Nothing, Vector{Float64}}} <: LinearSkOp
	m::Int		# Sketch size
	d::Int		# Dimension
	Ω::T		# Frequencies
	ξ::U	# Dithering
end
sketch_eltype(::QuantizedFourierSkOp) = Complex{Float64}

# Constructors
QuantizedFourierSkOp(Ω, ξ) = QuantizedFourierSkOp(size(Ω, 2), size(Ω, 1), Ω, ξ)
QuantizedFourierSkOp(A::BaseSkOp) = QuantizedFourierSkOp(A.Ω, A.ξ)
QuantizedFourierSkOp(Ω) = QuantizedFourierSkOp(Ω, gen_dithering(size(Ω,2)))
QuantizedFourierSkOp(m, d, k::Kernel; dithering = :unif, kwargs...) = 
	QuantizedFourierSkOp(drawfrequencies(m, d, k; kwargs...), gen_dithering(dithering, m))

# Sketching
# TODO Not sure about the numerical stability, is it better to use mod? (slower)
qnsin(x) = 1.0 - 2.0*(floor(x)-2.0*floor(x/2))	# using mod(x,a) = x - a*floor(x/a) and floor(floor(x)/2)=floor(x/2)
qncos(x) = qnsin(x+0.5)
qcis_el(x) = qncos(x) + im*qnsin(x)
@doc doc"""
	One-bit quantized equivalent of `cis`, i.e. sgn(cos(·))+im*sgn(sin(·)).
"""
function cis_approx!(out, X, fun_re, fun_im)
	for i = 1:length(X)
	# @threads for i = 1:length(X)  # TODO maybe slower for small matrices?
		@inbounds Xo = X[i]/π
		@inbounds out[i] = fun_re(Xo) + im*fun_im(Xo)
	end
	out
end
cis_approx(X, fun_re, fun_im) =	cis_approx!(similar(X, Complex{eltype(X)}), X, fun_re, fun_im)
qcis!(out, X) = cis_approx!(out, X, qncos, qnsin)
qcis(X) = cis_approx(X, qncos, qnsin)
sketchcolumns_square!(outsk, A, X, mask = nothing) = qcis!(outsk, linop(A.Ω, X, A.ξ, mask))
sketchcolumns_square(A, X, mask = nothing) = qcis(linop(A.Ω, X, A.ξ, mask))
sketchcolumns!(outsk, A::QuantizedFourierSkOp, X, mask = nothing) = sketchcolumns_square!(outsk, A, X, mask)


###################
#  Triangle Wave  #
###################

struct TriangleFourierSkOp{T<:AbstractArray{Float64,2}, U<:Union{Nothing, Vector{Float64}}} <: LinearSkOp
	m::Int		# Sketch size
	d::Int		# Dimension
	Ω::T		# Frequencies
	ξ::U 		# Dithering
end
sketch_eltype(::TriangleFourierSkOp)  = Complex{Float64}

# Constructors
TriangleFourierSkOp(Ω, ξ) = TriangleFourierSkOp(size(Ω,2), size(Ω,1), Ω, ξ)
TriangleFourierSkOp(A::BaseSkOp) = TriangleFourierSkOp(A.Ω, A.ξ)
TriangleFourierSkOp(Ω) = TriangleFourierSkOp(Ω, gen_dithering(size(Ω,2)))
TriangleFourierSkOp(m, d, k::Kernel; dithering = :unif, kwargs...) = 
	TriangleFourierSkOp(drawfrequencies(m, d, k; kwargs...), 
						gen_dithering(dithering, m))

# Sketching
trncos(x) = 2.0*abs(x-2*floor(x/2)-1) - 1.0
trnsin(x) = trncos(x-0.5)
trcis!(out, X) = cis_approx!(out, X, trncos, trnsin)
sketchcolumns!(outsk, A::TriangleFourierSkOp, X, mask = nothing) = trcis!(outsk, linop(A.Ω, X, A.ξ, mask))


#######################################################################
#       Triangle with fake grad (triangle instead of quantized)       #
#######################################################################

struct TriangleTweakedFourierSkOp{T<:AbstractArray{Float64,2}, U<:Union{Nothing, Vector{Float64}}} <: LinearSkOp
	m::Int		# Sketch size
	d::Int		# Dimension
	Ω::T		# Frequencies
	ξ::U 		# Dithering
end
sketch_eltype(::TriangleTweakedFourierSkOp) = Complex{Float64}

# Constructors
TriangleTweakedFourierSkOp(Ω, ξ) = TriangleTweakedFourierSkOp(size(Ω,2), size(Ω,1), Ω, ξ)
TriangleTweakedFourierSkOp(Ω) = TriangleTweakedFourierSkOp(Ω, gen_dithering(size(Ω,2)))
TriangleTweakedFourierSkOp(A::BaseSkOp) = TriangleTweakedFourierSkOp(A.Ω, A.ξ)
TriangleTweakedFourierSkOp(m, d, k::Kernel; dithering = :unif, kwargs...) = 
	TriangleTweakedFourierSkOp(drawfrequencies(m, d, k::Kernel; kwargs...), 
							   gen_dithering(dithering, m))

# Sketching
sketchcolumns!(outsk, A::TriangleTweakedFourierSkOp, X, mask = nothing) = trcis!(outsk, linop(A.Ω, X, A.ξ, mask))
