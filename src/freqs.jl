# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

"""
Builds a d×f dense matrix whose columns are on the unit sphere.
"""
function dense_isotrope(d, f)
	Ω = randn(d, f)		# Generate directions
	Ω = Ω./sqrt.(sum(Ω.^2, dims=1))
end

@doc doc"""
	drawradiuses(m, d; dist = :AdaptedRadius)

Draws a vector of `m` i.i.d. radiuses.

The distribution `dist` can be any of:
- `:AdaptedRadius` (default): close to a folded Gaussian, with reduced sampling around zero (see “Sketching for large-scale learning of mixture models”, Keriven et. al, 2017).
- `:Chi`: Chi distribution with `d` degrees of freedom.

See also: [`drawfrequencies`](@ref).
"""
function drawradiuses(m::Int, d::Int;	dist = :AdaptedRadius,
					  					σ = 1.0)
	if dist == :AdaptedRadius
		pdf = x -> sqrt(x.^2+x.^4/4) .* exp(-0.5*x.^2)
		r = drawfrompdf(pdf, 0:0.01:10, m)
		# r./σ   # Changes on 08/06/2020. This lines correspond to sketchmlbox's distribution
		# This is reweighted to have comparable kernel_var with a Gausian kernel
		# Here 3.85714 = E[r^2] when r follows the AdaptedRadius distribution of Nicolas.
		# σ_scaled is such that drawing frequency vectors as r/σ_scaled * φ with φ uniform on the sphere results in a kernel variance of 1/σ², where the σ passed here in parameter is the "desired spatial variance of the kernel"
		σ_scaled = σ * sqrt(3.85714 / d)
		r ./ σ_scaled
	elseif dist == :Chi
		rand(Chi(d), m)./σ
	elseif dist == :Unit
		ones(m)
	elseif dist == :SqrtOfd
		sqrt(d)*ones(m)
	else
		error("Unknown radius distribution.")
	end
end


"""
$(SIGNATURES)

Draws a `d`-by-`f` matrix (or equivalent fast transform) of frequencies.
- `linop_type` can take values `:dense`, `HDHDHD`, `HGHDHD` or `fastfood`.
- `radialdist` will be passed to `drawradiuses`.

See also: [`drawradiuses`](@ref).
"""
function drawfrequencies(f::Int, d::Int, σ = 1.0;
						 radial_correction = :dpad_radial_distribution, 
						 linop_type = :dense, 
						 radialdist = :Chi)

	if linop_type == :dense
		r = drawradiuses(f, d, σ = σ, dist = radialdist)	# Draw radiuses
		Ω = dense_isotrope(d, f).*r'
	else
		if radial_correction == :rescale
			# Only correct in expectation.
			r = drawradiuses(f, d, σ = sqrt(d/nextpow(2,d))*σ, dist = radialdist)	# Draw radiuses
		elseif radial_correction == :dpad_radial_distribution
			dpad = nextpow(2, d)
			r = drawradiuses(f, dpad, σ = σ, dist = radialdist)	# Draw radiuses
		else
			error("Unknown radial correction $(radial_correction)")
		end
		if linop_type == :HDHDHD
			Ω = WHOrthoRandom(d, f, r)
		elseif linop_type == :HGHDHD
			Ω = WHOrthoRandom(d, f, r; gaussian_first_block = true)
		elseif linop_type == :fastfood
			Ω = Fastfood(d, f, r)
		else
			error("Unknown type '$linop_type' for the linear operator. Try 'fastfood', 'HDHDHD', 'HGHDHD' or 'dense'.")
		end
	end
end

#######################################################################
#                               Kernels                               #
#######################################################################

abstract type Kernel end

# ===== Gaussian =====

"""
$(TYPEDEF)
k(x,y) = exp(-‖x-y‖₂²/(2σ²))
Implies p(ω)∝N(0, 1/σ²I).
"""
struct GaussianKernel <: Kernel 
	σ::Float64
end
GaussianKernel(; var = 1.0) = GaussianKernel(sqrt(var))
drawfrequencies(f::Int, d::Int, k::GaussianKernel; kwargs...) =
	drawfrequencies(f, d, k.σ; radialdist = :Chi, kwargs...)
(k::GaussianKernel)(x, y) = exp(-norm(x-y)^2/(2*k.σ^2))

# ===== Laplacian =====

"""
$(TYPEDEF)
k(x,y) = exp(-λ‖x-y‖₁)
Implies p(ω)∝ Π Cauchy(0, λ)
"""
struct LaplacianKernel <: Kernel 
	λ::Float64
end
LaplacianKernel(; var = 1.0) = LaplacianKernel(sqrt(2/var))

function drawfrequencies(f::Int, d::Int, k::LaplacianKernel; )
	rand(Cauchy(0, k.λ), d, f)
end

# ===== Adapted Radius =====

"""
$(TYPEDEF)
"""
struct AdaptedRadiusKernel <: Kernel 
	σ::Float64
end
AdaptedRadiusKernel(; var = 1.0) = AdaptedRadiusKernel(sqrt(var))
drawfrequencies(f::Int, d::Int, k::AdaptedRadiusKernel; kwargs...) =
	drawfrequencies(f, d, k.σ; radialdist = :AdaptedRadius, kwargs...)

# TODO avoid having a unique function as some kernels don't depend on σ
function build_kernel(ktype, σ)
	if ktype == :Gaussian
		GaussianKernel(σ)
	elseif ktype == :Laplacian
		LaplacianKernel(var = σ^2)
	elseif ktype == :AdaptedRadius
		AdaptedRadiusKernel(σ)
	elseif ktype == :UnitPCAKernel
		UnitPCAKernel()
	elseif ktype == :ChiPCAKernel
		ChiPCAKernel()
	else
		@error "Unknown kernel type $ktype."
	end
end
function build_kernel_kf(ktype, σ)
	if ktype == :Gaussian
		KernelFunctions.GaussianKernel() ∘ KernelFunctions.ScaleTransform(1/σ)
	else
		@error "Unknown kernel type '$ktype'. Conversion to KernelFunctions kernels is for now only supported for Gaussian kernels."
	end
end

# ===== Adapted Radius =====
struct UnitPCAKernel <: Kernel end
"""
$(TYPEDEF)
Kernel associated to random features φ(x)=(ωᵀx)² with ω~N(0,I). 
The evaluation is this kernel is approximated to κ(x,y)=(xᵀy)² (which would
be strictly speaking the mean kernel associated to the random measurements
<xxᵀ,A> with Aᵢⱼ~N(0,1) i.i.d.).
"""
struct ChiPCAKernel <: Kernel end
(k::ChiPCAKernel)(x, y) = (x'*y)^2 # This is an approximation!


