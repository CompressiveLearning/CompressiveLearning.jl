# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

using JuMP, Ipopt
# using Plots

@doc doc"""
Estimates the mean intra-clusters variance.
"""
function mean_intra_var_Keriven2017(D; 
									n = 5000,
									m = 500,
									nb_blocks = 30,
									nb_its = 3)
	n = min(n, size(D,2))
	X = D[:, randperm(size(D,2))[1:n]] # Random subset of columns
	d = size(X, 1)
	σ² = 10.0
	for it = 1:nb_its
		σ²_tmp = 10*σ²
		# Sketch
		A = FourierSkOp(m, d, CompressiveLearning.GaussianKernel(var=σ²_tmp))
		# A = FourierSkOp(m, d, GaussianKernel(var=1/σ²))
		s = abs.(sketch(A, X))
		# sort
		sq_rads_all = sum(A.Ω.^2, dims=1)[:]
		p = sortperm(sq_rads_all)
		s = s[p]
		sq_rads_all_sorted = sq_rads_all[p]
		# Ωs = Ω[:, p]

		# Find upper enveloppe
		block_size = Int(floor(m/nb_blocks))
		block_argmax(i) = (i-1)block_size + argmax(s[(i-1)block_size+1:min(i*block_size, m)])
		blocks_argmaxs = [block_argmax(i) for i in 1:nb_blocks]
		blocks_maxs = s[blocks_argmaxs]
		sq_rads = sq_rads_all_sorted[blocks_argmaxs]

		# Update σ²
		model = Model(optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0))
		# model = Model(with_optimizer(Ipopt.Optimizer, print_level = 0))
		@variable(model, opt_σ² >= 0.0, start = σ²)
		# @NLobjective(model, Min, sum(norm(blocks_maxs - exp.(-0.5.*sq_rads.*σ²))))
		@NLobjective(model, Min, sum((blocks_maxs[i] - exp(-0.5*sq_rads[i]*opt_σ²))^2 for i in 1:nb_blocks))
		JuMP.optimize!(model)
		σ² = JuMP.value(opt_σ²)
		# println("It $it, σ²= $(σ²)")
		# sq_rads_all = sum(Ωs.^2, dims=1)
		# scatter(sq_rads_all_sorted[:], s; markersize=1)
		# p = scatter!(sq_rads[:], blocks_maxs; markersize=3)
		# display(p)
		# sleep(1)
	end
	return σ²
end

function dataset_stats(X, out_dic::Dict = Dict{Symbol,Any}(); 
						compute_sepk2 = false,
						compute_pairwise_dists = false,
						compute_intra_var = false, )
	d, n = size(X)
	out_dic[:second_moment] = sum(sum(b.^2) for b in centered_batch_iterator(X))/length(X)
	if compute_pairwise_dists
		sqdists = Distances.pairwise(SqEuclidean(), X, dims=2)
		sqdists[sqdists .< 0] .= 0.0
		out_dic[:mean_sqeuclidean_dists] = sum(sqdists)/(n*(n-1))
		out_dic[:mean_euclidean_dists] = sum(sqrt.(sqdists))/(n*(n-1))
	end
	if compute_intra_var
		out_dic[:mean_intra_var_keriven2017] = mean_intra_var_Keriven2017(X)
	end
	if compute_sepk2
		out_dic[:estimated_sepk2] = simple_separation_estimation_k2(X; deconvolve = true)
		out_dic[:estimated_sepk2_raw_sketch] = simple_separation_estimation_k2(X; deconvolve = false)
	end
	return out_dic
end

function simple_separation_estimation_k2(X; m = 1000, deconvolve = false)
	# TODO not robust, just to test
	d = size(X,1)
	m = 1000
	b = maximum(abs.(X))
	σ_ω = 1/(10*b*sqrt(d))
	@assert isreal(σ_ω)
	@assert σ_ω < Inf
	Ω = randn(d, m)*σ_ω 
	φ(Ω) = mean(exp.(im*Ω'*X), dims=2)
	if deconvolve
		φ_dec(Ω, σ²) = φ(Ω) ./ exp.(-0.5*sum((Ω').^2, dims=2)*σ²)
		σ²_est = mean_intra_var_Keriven2017(X)
		@assert isreal(σ²_est)
		@assert σ²_est < Inf
		φ_to_use(Ω) = φ_dec(Ω, σ²_est)
	else
		φ_to_use = φ
	end
	approx_sin²(Ω) = 1 .- abs.(φ_to_use(Ω)).^2
	est_sqnorm_a = mean(approx_sin²(Ω))/σ_ω.^2
	ε_est = 2*sqrt(est_sqnorm_a) # estimation of ε
	@assert isreal(ε_est)
	@assert ε_est < Inf
	return ε_est
end

function separation_from_centroids(C)
	sqdists = pairwise(SqEuclidean(), C, dims=2)
	sqdists[sqdists .< 0] .= 0.0
	display(sqdists)
	return minimum(sqdists[sqdists .> 0]), mean(sqdists[sqdists .> 0])
end
