# This file is part of CompressiveLearning.jl, a Julia package for 
# machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

# Related paper:
#
# [1] Differentially Private Compressive k-means 
#	  V. Schellekens, A. Chatalic, F. Houssiau, Y.-A. de Montjoye, L. Jacques, R. Gribonval
#	  ICASSP 2019

# using JuMP
# using Ipopt

# ===== Utils =====

"""
	analytic_gaussian_mechanism_Balle18(Δ₂, ε, δ)
Returns the parameter `σ` from Algorithm 1 of [Balle and Wang, ICML'18].
"""
function analytic_gaussian_mechanism_Balle18(Δ₂, ε, δ)
	Φ(t) = 0.5*(1 + erf(t/sqrt(2)))
	Bp(v) = Φ( sqrt(ε*v)) - exp(ε)*Φ(-sqrt(ε*(v+2)))
	Bm(v) = Φ(-sqrt(ε*v)) - exp(ε)*Φ(-sqrt(ε*(v+2)))
	δ₀ = Bp(0.0)
	α = if δ ≈ δ₀
		1.0
	elseif δ ≥ δ₀
		v = find_zero((v->Bp(v)-δ), (0, Inf))
		sqrt(1+v/2) - sqrt(v/2)
	else # δ < δ₀
		u = find_zero((v->Bm(v)-δ), (0, Inf))
		sqrt(1+u/2) + sqrt(u/2)
	end
	return α*Δ₂/sqrt(2ε)
end

# ===== Noise  =====

"""
	ComplexLaplace(b)
A complex r.v. whose real and imaginary parts are Laplace r.v. with parameter b.
Its variance σ_z² satisfies σ_z²=4b² (and σ_r²=σ_i²=2b²).
"""
struct ComplexLaplace
	d::Laplace{Float64}
	ComplexLaplace(b::Float64) = new(Laplace(0.0, b))
	ComplexLaplace(; σ::Float64=1.0) = new(Laplace(0.0, σ/2))
end
function addnoise!(Y::AbstractArray{Complex{T}}, X, ν::ComplexLaplace)  where T
	Y .= X .+ (rand(ν.d, size(X)...) .+ im*rand(ν.d, size(X)...))
end
"""
	ComplexNormal(σ_z)
Equivalent to N(0,σ_z²/2)+i·N(0,σ_z²/2).
"""
struct ComplexNormal
	d::Normal{Float64}
	ComplexNormal(σ_z::Float64=1.0) = new(Normal(0.0, σ_z/sqrt(2)))
end
function addnoise!(Y::AbstractArray{Complex{T}}, X, ν::ComplexNormal) where T
	Y .= X .+ (rand(ν.d, size(X)...) .+ im*rand(ν.d, size(X)...))
end

addnoise!(Y, X, ν::Nothing) = X
addnoise!(Y, X, ν::UnivariateDistribution) = Y .= X + rand(ν, size(X)...)
addnoise!(X, ν) = addnoise!(X, X, ν)

# ===== UDP averager =====

"""
$(SIGNATURES)
Releases (n*(sketch of `p`) + ξ) / (n + ζ).
"""
struct UDPAverageSkOp{T<:SketchingOperator,Tdξ<:Union{UnivariateDistribution,ComplexLaplace,ComplexNormal},Tdζ<:Union{Nothing,UnivariateDistribution}} <: ComposedLinearSkOp
	"Parent sketching operator"
	p::T
	"Noise distribution for the sum of features"
	d_ξ::Tdξ 
	"Noise distribution for the dataset size"
	d_ζ::Tdζ	
end
sketch_onebatch(A::UDPAverageSkOp, X) = sketch_onebatch(A.p, X)
function postprocess!(s::LinearSketch, A::UDPAverageSkOp, n)
	s.s .*= s.n		# We want only the sum (divided by α_f if subsampling is on), not the mean. 
	tmp_s = copy(s.s)
	addnoise!(s.s, A.d_ξ)		# (Σᵢ ϕ(xᵢ)bᵢ)/α + ξ
	ξ = s.s - tmp_s
	if A.d_ζ ≠ nothing
		# s.s ./= (s.n + rand(A.d_ζ))	# n + ζ
		s.s ./= max(1, s.n + rand(A.d_ζ))
	else
		s.s ./= s.n
	end
	return s
end

# """
	# heuristic_γ(nf, εf, α, m; δ = 0.35)
# Computes the optimum splitting γ∊]0,1[ between numerator and dataset size for UDP, RFF and Laplace noise.
# This computations relies uses the SNR as a proxy for utility.
# """
# function heuristic_γ(nf, εf, αf, mf; δ = 0.35)
	# # α = BigFloat(αf)
	# # m = BigFloat(mf)
	# n = BigFloat(nf)
	# ε = BigFloat(εf)
	# A = 1/(n^2*ε^2)
	# C = cbrt(0.5*(27*A + 3*sqrt(81*A^2 + 3*32*A^3)))
	# γ_star_bf = 1 - C/3 + 2*A/C
	# γ_star = convert(Float64, γ_star_bf)
	# @assert (γ_star > 0.0 && γ_star < 1.0) "heuristic_γ computed the value '$γ_star' for γ from n=$n, ε=$ε, which is not a valid value in ]0,1[."
	# return γ_star
# end

abstract type NeighborhoodRelation end
struct ReplaceOneSample <: NeighborhoodRelation end
struct AddOrRemoveOneSample <: NeighborhoodRelation end

#######################################################################
#              Private Sketching Operators Constructors               #
#######################################################################

# ε-DP
c_sim_rff(::ReplaceOneSample) = 2*sqrt(2)
c_sim_rff(::AddOrRemoveOneSample) = sqrt(2) 
c_ρ(A::FourierSkOp) = 1.0
c_ρ(A::QuantizedFourierSkOp) = sqrt(2.0) 	# because our quantized features are not normalized

Δ₁_Σ(A::Union{FourierSkOp,QuantizedFourierSkOp}, sim::NeighborhoodRelation; out_dic = Dict()) = sketchsize(A) * c_sim_rff(sim) * c_ρ(A)
# We use the same bound for BDP, UDP, cf. paper
Δ₁_Σ(A::SqRandomSkOp, sim::NeighborhoodRelation; out_dic = Dict()) = pca_Δ₁(A.Ω; out_dic = out_dic) 
ξ_distribution_Laplace(A::Union{FourierSkOp,QuantizedFourierSkOp}, Δ₁_Σ, ε) = ComplexLaplace(Δ₁_Σ / ε)
ξ_distribution_Laplace(A::SqRandomSkOp, Δ₁_Σ, ε) = Laplace(0.0, Δ₁_Σ / ε)

c_sim_RFF_ADP(::ReplaceOneSample) = 2.0
c_sim_RFF_ADP(::AddOrRemoveOneSample) = 1.0
# (ε,δ)-DP
function Δ₂_Σ(A::Union{FourierSkOp,QuantizedFourierSkOp}, sim::NeighborhoodRelation; 
			  DP_RQF_Δ₂_computation_method = nothing,
			  out_dic = Dict())
	@assert (DP_RQF_Δ₂_computation_method == nothing) # This is ugly, should reorganize the code
	sqrt(sketchsize(A)) * c_ρ(A) * c_sim_RFF_ADP(sim)
end
Δ₂_Σ(A::SqRandomSkOp, sim::NeighborhoodRelation; DP_RQF_Δ₂_computation_method = nothing, out_dic = Dict()) = 
	pca_Δ₂(A.Ω; DP_RQF_Δ₂_computation_method = DP_RQF_Δ₂_computation_method, out_dic = out_dic)
ξ_distribution_Normal(A::Union{FourierSkOp,QuantizedFourierSkOp},  σ) = ComplexNormal(sqrt(2)*σ)
ξ_distribution_Normal(A::SqRandomSkOp, σ) = Normal(0.0, σ)


function choose_γ(γ, out_dic)
	γn = (γ == :auto) ? heuristic_γ() : γ
	if !isa(out_dic, Nothing)
		out_dic[:DP_γ] = (γ == :auto) ? :heuristic_using_n0 : :userprovided
		out_dic[:DP_γ_num] = γn
	end
	return γn
end
function heuristic_γ()
	error("No heuristic exists in this setting. Try manually providing γ (or DP_γ for the wrappers).") # There was an error in the old mechanism. 
	# We need an estimate n₀ here to follow the idea in the paper.
end


"""
$(SIGNATURES)
Returns a wrapper operator for `A` wich satisfies (`ε`,`δ`)-differential privacy for the neighborhood relation `sim`.
Here `r` is the number of observations per sample (for subsampling, `1≤r≤m`), `α_n` the subsampling rate (for samples), `use_ζ` can be used to manually enable/disable noise on the dataset size (but should be `true` for UDP privacy to hold).
"""
function DP_skop(A, sim, use_ζ, ε::Real, δ, r::Int, α_n, γ; 
				 DP_RQF_Δ₂_computation_method = nothing, 
				 out_dic = Dict())
	@assert (0 < ε < Inf)
	@assert (γ isa Nothing) || (γ == :auto) || (γ isa Float64 && γ>0.0 && γ<1.0)
	@assert (0 ≤ α_n ≤ 1.0)
	m = sketchsize(A)
	@assert (1 ≤ r ≤ m)
	α_f = r/m
	bA = parentskop(A)
	use_ζ_in_practice = use_ζ && ~(sim isa ReplaceOneSample) # For BDP, we never use ζ

	Δ₁_den = 1.0	# sensitivity for releasing n
	γn = (use_ζ_in_practice ? choose_γ(γ, out_dic) : 1.0)
	ε_Σ = subsampling_ε(γn*ε, α_n)	# sum of features
	ε_n = (1-γn)ε

	d_ξ = if δ == 0.0
		Δ₁Σ = Δ₁_Σ(bA, sim; out_dic = out_dic)
		if (out_dic ≠ nothing)
			out_dic[:noise_type] = "Laplacian"
			out_dic[:l1_sensitivity] = Δ₁Σ
		end
		ξ_distribution_Laplace(bA, Δ₁Σ, ε_Σ)
	else
		Δ₂Σ = Δ₂_Σ(bA, sim;
				   DP_RQF_Δ₂_computation_method = DP_RQF_Δ₂_computation_method,
				   out_dic = out_dic)
		δ_ss = δ / α_n
		σ = analytic_gaussian_mechanism_Balle18(Δ₂Σ, ε_Σ, δ_ss)
		if (out_dic ≠ nothing)
			out_dic[:noise_type] = "Gaussian"
			out_dic[:gaussian_mechanism] = "analyticBalle18"
			out_dic[:num_noise_std] = σ
			out_dic[:computed_l2_sensitivity] = Δ₂Σ
		end
		ξ_distribution_Normal(bA, σ)
	end
	if (out_dic ≠ nothing)
		out_dic[:using_denominator_noise] = use_ζ_in_practice
	end
	d_ζ = (use_ζ_in_practice ? Laplace(0.0, Δ₁_den/((1-γn)ε)) : nothing)
	UDPAverageSkOp(A, d_ξ, d_ζ)
end


"""
$(SIGNATURES)
Returns the (tight) privacy lever ε' such that for any ε'-DP mechanism M, the mechanism M(S(·)) is ε-DP [1], where S subsamples the dataset samples with probability `α_n`.

[1]	Privacy Amplification by Subsampling: Tight Analyses via Couplings and Divergences
	http://arxiv.org/abs/1807.01647
	Borja Balle, Gilles Barthe and Marco Gaboardi
	2018
"""
function subsampling_ε(ε, α_n)
	ε_BF = BigFloat(ε)
	α_n_BF = BigFloat(α_n)
	return convert(Float64, log(1+(exp(ε_BF)-1)/α_n_BF))
end



@doc doc"""
	pca_sup_lbfgs(Ω; p = 2)
Computes ``sup_{x:‖x‖₂≤1} f(x)`` where ``f(x)=(1/m)*Σ_{1≤j≤m} (ωⱼᵀx)^p`` and ``ωⱼ`` denotes the j-th column of `Ω`.
Optimizes in practice ``f(x)/‖x‖^p`` using L-BFGS-B, and support fast transforms.
"""
function pca_sup_lbfgs(Ω; p=2, init = :random)
	d, m = size(Ω)
	obj = if p == 2
		function(g, cx)
			sx = sum(cx.^2)
			e = Ω'*cx
			fx = sum(e.^2/m) # 1/m * sum(‖Ωᵀx‖₂²)
			∇fx = 2*vec(Ω*e/m)
			g .= -(∇fx/sx - 2*fx*cx/sx^2)
			return -(fx/sx)
		end
	elseif p == 4
		function(g, cx)
			e = Ω'*cx  
			sx = sum(cx.^2) # ‖Ωᵀx‖₂²
			fx = sum(e.^4)/m
			∇fx = 4*vec(Ω*e.^3/m)
			g .= -(∇fx/sx^2 - 4*fx*cx/sx^3)
			return -fx/(sx^2)
		end
	else @error "Only p=2 and p=4 are supported."
	end
	if init == :random
		x = (0.9/sqrt(d))*randn(d)	# normalization is useless, just to check
	elseif init == :fixed
		x = (0.9/sqrt(d))*ones(d)
	else
		error("Unknown initialization method \"$(init)\".")
	end
	# x0_obj = -obj(x,randn(d))
	# println("p=$p, $(init), init obj = $(x0_obj)")
	# println(x / norm(x))
	# (xsol, objval) = LBFGSBw(obj, x, -1.0*ones(d), 1.0*ones(d); maxiter = 10000, verbose=1)
	(xsol, objval) = @suppress_out LBFGSBw(obj, x, -1.0*ones(d), 1.0*ones(d); verbose=1, factr=1e3)
	# println(xsol / norm(xsol))
	objval = -objval
	# @assert objval>1e-3
	return objval
end

function sup_op_2_4_power_4_iter(Ω, iterations = 2000, tol=1e-8)
	d, m = size(Ω)
	x = randn(d)
	x .= x / norm(x)
	x_old = zeros(d)
	obj(x) = sum((Ω'*x).^4)
	for i = 1:iterations
		x_old .= x
		p3 = (Ω'*x).^3
		x_tmp = vec(sum((Ω*[(p3[j]*(i==j)) for i in 1:m]) for j in 1:m))
		x .= x_tmp / norm(x_tmp)
		if (obj(x) - obj(x_old))/obj(x) ≤ tol
			println(" stopping at $i")
			break
		end
	end
	return obj(x)
end

# All these functions compute
# sup_{x:‖x‖₂≤1} Σ_{1≤j≤m} (ωⱼᵀx)^p in different settings
function pca_Δ₁_num(Ω, c_Φ, out_dic)
	out_dic[:sensitivity_estimation_method] = :l1_numopt
	c_Φ*size(Ω,2)*pca_sup_lbfgs(Ω; p = 2)
end
function pca_Δ₁_tightframe(d, m, rad, c_Φ, out_dic)
	out_dic[:sensitivity_estimation_method] = :l1_tightframe
	# Special case Tight frame, ΩΩᵀ=(m/d)*rad²*I
	c_Φ*(m/d)*rad^2
end
# function pca_Δ₂_udp_tightframe(d, m, rad, c_Φ, out_dic)
	# error("TODO")
# end
_pca_Δ₁(Ω::AbstractArray, c_Φ, out_dic) = pca_Δ₁_num(Ω, c_Φ, out_dic)

istightframe(Ω::AbstractArray) = false 	# No by default
istightframe(Ω::WHOrthoRandom) = all(Ω.R[1:Ω.d] .≈ Ω.R[1]) # TODO check that it is one for privacy?
function _pca_Δ₁(Ω::WHOrthoRandom, c_Φ, out_dic)
	if istightframe(Ω)	# Check when d is not a power of 2
		pca_Δ₁_tightframe(Ω.d, Ω.m, Ω.R[1], c_Φ, out_dic)
	else
		pca_Δ₁_num(Ω, c_Φ, out_dic)
	end
end
pca_Δ₁(Ω; c_Φ = 1.0, out_dic = Dict()) = _pca_Δ₁(Ω, c_Φ, out_dic)

# default_Δ₂_method(Ω) = istightframe(Ω) ? :closed_form : :lbfgs_rayleigh_quotient
default_Δ₂_method(Ω) = :lbfgs_rayleigh_quotient
function pca_Δ₂(Ω; c_Φ = 1.0, DP_RQF_Δ₂_computation_method = default_Δ₂_method(Ω), out_dic = Dict())
	Δ₂ = if istightframe(Ω) && (DP_RQF_Δ₂_computation_method == :closed_form)
		out_dic[:sensitivity_estimation_method] = :closed_form_tightframe
		error("Not defined yet")
	elseif DP_RQF_Δ₂_computation_method == :riesz_thorin_lbfgs
		out_dic[:sensitivity_estimation_method] = :riesz_thorin_lbfgs
		pca_Δ₂_riesz_thorin_lbfgs(Ω)
	elseif DP_RQF_Δ₂_computation_method == :lbfgs_rayleigh_quotient
		out_dic[:sensitivity_estimation_method] = :lbfgs_rayleigh_quotient
		pca_Δ₂_lbfgs_rayleigh_quotient(Ω)
	elseif DP_RQF_Δ₂_computation_method == :power_iteration
		out_dic[:sensitivity_estimation_method] = :power_iteration
		pca_Δ₂_power_iteration(Ω)
	else
		error("Unknown method $(DP_RQF_Δ₂_computation_method), or cannot be applied.")
	end
	return c_Φ*Δ₂
end
pca_Δ₂_lbfgs_rayleigh_quotient(Ω; inits = 10) = 
	maximum([sqrt(size(Ω,2)*pca_sup_lbfgs(Ω; p = 4)) for i = 1:inits]) # Estimated ‖Ωᵀ‖_{2→4}²
# pca_Δ₂_riesz_thorin_lbfgs_old(Ω) = sqrt(size(Ω,2)*pca_sup_lbfgs(Ω; p = 2)) # ‖Ω‖_{2→2}
pca_Δ₂_riesz_thorin_lbfgs(Ω::WHOrthoRandom) = maximum(Ω.R) * sqrt(size(Ω,2)*pca_sup_lbfgs(Ω; p = 2)) # ‖Ω‖_{2→2}
pca_Δ₂_riesz_thorin_lbfgs(Ω::Matrix) = maximum(colnorms(Ω)) * sqrt(size(Ω,2)*pca_sup_lbfgs(Ω; p = 2)) # ‖Ω‖_{2→2}
pca_Δ₂_power_iteration(Ω) = sqrt(sup_op_2_4_power_4_iter(Ω))
