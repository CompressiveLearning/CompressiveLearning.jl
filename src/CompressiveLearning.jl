# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

module CompressiveLearning
CL = CompressiveLearning

# Sketching & Learning
export CLOMPR, CLAMP, CKM, weighted_CKM, CPCA, CGMM		# wrappers
export MinMaxSkOp, FourierSkOp, QuantizedFourierSkOp, TriangleFourierSkOp, SqRandomSkOp, NyströmSkOp
export drawfrequencies, GaussianKernel, LaplacianKernel, AdaptedRadiusKernel
export sketch, sketchsize
export kmeans_inverse_problem, pca_inverse_problem, GMM_inverse_problem
export build_skop
export dataset_stats
export evaluate_kmeans, evaluate_GMM, evaluate_pca
export FastTransform, WHOrthoRandom, Fastfood, StructuredLandmarks

using FastHadamardStructuredTransforms_jll
const libfht = FastHadamardStructuredTransforms_jll.libfasttransforms

# using Base.Threads, LinearAlgebra, Random, Distributed, Requires, VML
using Base.Threads, LinearAlgebra, Random, Distributed, Requires
using Markdown			# for documentation
using StatsBase			# to sample without replacement
using Distributions
using Interpolations	# to sample from pdfs
using Clustering		# to compare with kmeans
using Suppressor 		# to silence LBFGSB output
using Distances
using BatchIterators, ProgressMeter 	# ProgressMeter for sketching
using SpecialFunctions, Roots	 # for Balle mechanism in privacy.jl (erf and find_root)
using DocStringExtensions # for SIGNATURES and other extensions
using KernelFunctions # for better integration in the ecosystem + efficient kernel matrix
using DataFrames

# Also requires LBFGS (cf. src/helpers/LBFGSB_wrapper.jl)
# Also requires some optimization libraries (cf src/learning_tasks/PCA.jl)

# Third party wrappers
include("helpers/LBFGSB_wrapper.jl")
include("helpers/helpers.jl")

# Code of the module
include("freqs.jl")	
include("sketching.jl")	
include("kernel_learning.jl")	
include("fasttransforms.jl")	
include("quantization.jl")	

# Applications
include("learning.jl")
include("learning_tasks/clustering.jl")
include("learning_tasks/GMM.jl")
include("learning_tasks/PCA.jl")

include("Nyström/Nyström.jl")	

# Differential Privacy
include("privacy.jl")	

# Decoders
include("decoders/clompr.jl")		# CL-OMPR decoder
include("decoders/gamp.jl")			# CL-AMP decoder

function __init__()
	@require IntelVectorMath="c8ce9da6-5d36-5c03-b118-5a70151be7bc" begin
		using .IntelVectorMath
		include("helpers/vml.jl")
		@require Revise="295af30f-e4ad-537b-8983-00126c2a3abe" begin
			using .Revise
			Revise.track("helpers/vml.jl")
		end
	end
	@require PyCall="438e738f-606a-5dbb-bf0a-cddfbfd45ab0" begin
		using .PyCall
		pushfirst!(PyVector(pyimport("sys")."path"), joinpath(@__DIR__, "Nyström"))
		include("Nyström/Nyström_python.jl")
	end
end

end
