export CKM_BLESS, CGMM_BLESS

function python_kernel(ker::UGaussianKernel; 
						use_falkon = true,
						falkon_use_cpu = true, )
	if use_falkon
		kfalkon = pyimport("falkon.kernels")
		gkt = pyimport("GK_to_torch")
		ofalkon = pyimport("falkon.options")
		f_opts = ofalkon.FalkonOptions(use_cpu=falkon_use_cpu)
		k_np = kfalkon.GaussianKernel(sigma=kernel_std(ker), opt=f_opts)
		return gkt.GK_to_torch(k_np)
	else
		klib = pyimport("sklearn.gaussian_process.kernels")
		return klib.RBF(kernel_std(ker)) # Before using Falkon kernels
	end
end

function dppy_ls(X, ker::UGaussianKernel, m;
			use_falkon = true, 
			falkon_use_cpu = true, )
	@debug "Calling python DPPY library for the leverage scores (m=$m)…"
	lib = pyimport("dppy.finite_dpps")
	py_ker = python_kernel(ker; use_falkon, falkon_use_cpu)
	dpp = lib.FiniteDPP("likelihood", L_eval_X_data=(py_ker, PyReverseDims(X))) 
	centers_idx = dpp.sample_exact_k_dpp(m, mode="alpha",
						params = Dict(:early_stop => true),  # Return right after finding a sample, as we will sample only once anyway.
						verbose=true) 
	return centers_idx .+ 1 # Python starts numerotation at 0
end

"""
$(SIGNATURES)
Returns the indexes of columns of `X` sampling with bless.
Requires bless and sklearn to be available in the python instllation used by PyCall.
"""
function sample_bless(X, ker, λ)
	bless = pyimport("bless")
	klib = pyimport("sklearn.gaussian_process.kernels")
	res = bless.bless(PyReverseDims(X), klib.RBF(kernel_std(ker)), lam_final = λ)
	return res[1] .+ 1 # Python starts at 0
end

bless_mean_size(X, ker, λ; trials = 4) = mean(length(sample_bless(X, ker, λ)) for i in 1:trials)

"""
$(SIGNATURES)
Creates a `NyströmSkOp` object using the kernel `k` and points randomly sampled from the dataset `X` (samples = columns) using BLESS. The sketch size `m` depends in this setting on the intrinsic dimension of the problem.
"""
function BlessNyströmSkOp(X::AbstractArray, ker::GaussianKernel;
					 	  stability_shifting = 1e-6,
					 	  λ = 1e-2)
	(stability_shifting > λ) && (@warn "stability_shifting=$stability_shifting > λ=$λ")
	S = sample_bless(X, ker, λ)
	m = length(S)
	Y = X[:, S]
	KS = Distances.pairwise(SqEuclidean(), Y; dims=2)
	KS = Symmetric(exp.(-KS./(2*kernel_std(ker)^2)))
	if stability_shifting > 0
		KS += stability_shifting*I
	end
	K_sqrt_inv = sqrt(inv(KS))::Symmetric{Float64,Matrix{Float64}} # Ensure it's not complex
	return (NyströmSkOp(m, size(X,1), Y, KS, K_sqrt_inv, ker), m)
end

# TODO: factorize parts of code with CKM. This is a separate function simply because m is not fixed here, but the middle part of the code can be shared.

"""
$(SIGNATURES)
Compressive `k`-means of the columns of `X` using Nyström approximation with leverage scores computed using BLESS. Supported keyword arguments include:
- `kernel_var`: kernel variance
- `decoder` (`:CLOMPR` or `:CLAMP`)
- `decoder_kwargs`: dictionary of keyword arguments, passed to the decoder function
- `out_dic`: output dictionary, useful parameters will be stored inside when provided
"""
function CKM_BLESS(X, k, λ;
			 		kernel_var = nothing,	# Chooses σ²=kernel_var
			 		kernel_var_inter_var_ratio = nothing, # Chooses σ²=kernel_var_inter_var_ratio*inter_var
			 		kernel_var_choice = nothing, 
			 		decoder = :CLOMPR,
			 		decoder_kwargs = Dict(),
			 		out_dic = Dict(),
					batch_size = nothing,
					Nyström_stability_shifting = 1e-8, )
	
	σX = sqrt(sum(sum(b.^2) for b in centered_batch_iterator(X))/(size(X,1)*size(X,2)))
	σ = choose_std(X; kernel_var = kernel_var, 
				   kernel_var_inter_var_ratio = kernel_var_inter_var_ratio, 
				   kernel_var_choice = kernel_var_choice, 
				   out_dic = out_dic)
	d = size(X, 1)

	@info "Building BLESS SkOp with λ=$λ, σ=$σ."
	Ask, m = BlessNyströmSkOp(X, GaussianKernel(σ); λ)

	# If a dictionary out_dic is provided, store additional infos
	if !isa(out_dic, Nothing)
		out_dic[:m] = m
		out_dic[:m_factor] = m/(k*d)
		out_dic[:m_factor_eq_reals] = out_dic[:m_factor] # same same
		out_dic[:approximation_type] = :Nyström
		out_dic[:kernel_type] = :Gaussian
		out_dic[:Nyström_sampling_type] = :BLeSS_leverage_scores
		out_dic[:Nyström_bless_lambda] = λ
		out_dic[:Nyström_stability_shifting] = Nyström_stability_shifting
	end
	return CKM(X, k, Ask; decoder, decoder_kwargs, out_dic, batch_size)
end

function CGMM_BLESS(X, k, λ;
			 		kernel_var = nothing,
			 		kernel_var_inter_var_ratio = nothing,
			 		kernel_var_choice = nothing, 
			 		decoder = :CLOMPR,
			 		decoder_kwargs = Dict(),
			 		out_dic = Dict(),
					Nyström_stability_shifting = 1e-8)
	
	σX = sqrt(sum(sum(b.^2) for b in centered_batch_iterator(X))/(size(X,1)*size(X,2)))
	σ = choose_std(X; kernel_var, kernel_var_inter_var_ratio, kernel_var_choice, out_dic)
	d = size(X, 1)

	@info "Building BLESS SkOp with λ=$λ, σ=$σ."
	Ask, m = BlessNyströmSkOp(X, GaussianKernel(σ); λ)

	if !isa(out_dic, Nothing)
		out_dic[:m] = m
		out_dic[:m_factor] = m/(2*k*d)
		out_dic[:m_factor_eq_reals] = out_dic[:m_factor] # same same
		out_dic[:approximation_type] = :Nyström
		out_dic[:kernel_type] = :Gaussian
		out_dic[:Nyström_sampling_type] = :BLeSS_leverage_scores
		out_dic[:Nyström_bless_lambda] = λ
		out_dic[:Nyström_stability_shifting] = Nyström_stability_shifting
	end
	return CGMM(X, k, Ask; decoder, decoder_kwargs, out_dic)
end
