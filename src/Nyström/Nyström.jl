# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2021 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################
KF = KernelFunctions
const UKernel = Union{CL.Kernel, KF.Kernel}
const UGaussianKernel = Union{CL.GaussianKernel, KF.GaussianKernel, KF.TransformedKernel{<:KF.GaussianKernel, <:ScaleTransform}}

"""
$(TYPEDEF)
An operator computing the mean embedding associated to a Nyström feature map.
# Fields
$(TYPEDFIELDS)
"""
struct NyströmSkOp{DT, ET, OT<:LinearOperator{DT}, KT<:UKernel} <: LinearSkOp
	"Number of landmark points used for the approximation"
	m::Int
	"Dimension"
	d::Int	
	"Linear operator whose columns are landmarks points"
	L::OT  
	"Reduced kernel matrix"
	K::Symmetric{ET, Matrix{ET}}
	"Square root of the inverse of the reduced kernel matrix"
	K_sqrt_inv::Symmetric{ET, Matrix{ET}}
	"Kernel function"
	k::KT
end

dppy_ls(X, ker, m) = error("Please manually load PyCall (by running `using PyCall`) in order to use DPPY.")

"""
$(SIGNATURES)
Returns the indexes of `l` samples (columns of `X`) picked by greedily maximizing the log-determinant of the corresponding subkernel matrix for kernel `ker`.

[1] Fast Greedy MAP Inference for Determinantal Point Process to Improve Recommendation Diversity
	Chen, Zhan, Zhou
	NIPS18
"""
function fast_greedy_map(X, ker, l)
	@assert l≤size(X,2) "Trying to pick l=$l samples, but the dataset only contains $(size(X,2)) samples."
	n = size(X, 2)
	cᵢs = zeros(l, n) # transposed
	dᵢ² = ones(n)
	j = rand(1:n) # Diagonal is 1 for a Gaussian Kernel
	idx = [j]
	while length(idx) < l
		k = length(idx)
		cᵢ_opt = cᵢs[1:k, j]
		Lji = kernelmatrix(ker, X[:,j:j], X[:,:]; obsdim=2) # 1×n
		eᵢs = (Lji - cᵢ_opt'*cᵢs[1:k,:])  / sqrt(dᵢ²[j]) # 1×n
		cᵢs[k,:] = eᵢs
		dᵢ² .-= vec(eᵢs.^2)
		dᵢ²[j] = -Inf # So that it will never be chosen again
		# j = argmax(dᵢ²)
		# In case of badly chosen kernel, randomize a bit otherwise 
		# we'll always take the first samples.
		j = rand(findall(dᵢ².≈maximum(dᵢ²))) 
		push!(idx, j)
	end
	return idx
end

"""
$(SIGNATURES)
Creates a `NyströmSkOp` object using the (symmetric) kernel `k` and `m` points randomly sampled from the dataset `X` (samples = columns).
It is strongly advised to use a kernel from the KernelFunctions package if efficiency is required.
"""
function NyströmSkOp(m::Int, X, ker::Union{CL.Kernel, KF.Kernel};
					 linop_type = :dense,  
					 sampling_type = :uniform,
					 ls_regularization = 1e-2,  # TODO choice??
					 stability_shifting = 1e-6,
					 use_falkon = true, 
					 out_dic = nothing)

	n = size(X, 2)
	use_structure = (linop_type != :dense)
	dpad = nextpow(2, size(X,1))
	l = use_structure ? ceil(Int, m/dpad) : m
	if out_dic ≠ nothing 
		out_dic[:ft_seed_landmarks] = l
		out_dic[:ft_dpad] = dpad
	end
	@debug "Sampling $m landmarks to build a Nyström sketching operator."
	S = if sampling_type == :uniform
		sort(sample(1:n, l; replace=false))
	elseif sampling_type == :kernel_leverage_scores
		K = kernelmatrix(ker, X; obsdim=2) # n² space
		scores = diag(K*inv(K+ls_regularization*n*I))
		scores ./= sum(scores)
		sort(sample(1:n, Weights(scores), l; replace=false))
	elseif sampling_type == :DPPY_leverage_scores
		dppy_ls(X, ker, l)
	elseif sampling_type == :greedy_leverage_scores
		fast_greedy_map(X, ker, l)
	else
		throw(ArgumentError("Value '$sampling_type' not recognized for the keyword argument 'sampling_type'. Supported values are :uniform, :kernel_leverage_scores, :DPPY_leverage_scores, :greedy_leverage_scores."))
	end
	Y = X[:,S]
	L = use_structure ? StructuredLandmarks(Y; m, type = linop_type) : Y
	@debug "Selected a matrix of landmarks of size $(size(L))"
	return NyströmSkOp(ker, L; stability_shifting)
end

# TODO: inefficient, just to test
KF.kernelmatrix(ker::CL.Kernel, X, Y=X; obsdim=2) = 
	exp.(-Distances.pairwise(SqEuclidean(),X,Y; dims=obsdim) ./ (2*kernel_std(ker)^2))

# Inefficient but cannot do much better if we need to build the kernel matrix anyway.
KF.kernelmatrix(ker::CL.Kernel, X::StructuredLandmarks; kwargs...) = 
	KF.kernelmatrix(ker, Matrix(X); kwargs...) 
KF.kernelmatrix(ker::KF.Kernel, X::StructuredLandmarks; kwargs...) = 
	KF.kernelmatrix(ker, Matrix(X); kwargs...) 

# Due to the way KernelFunctions deal with transformations, 
# it's actually easier to recode everything.
function KF.kernelmatrix(ker::KF.Kernel, X::StructuredLandmarks, Y::AbstractMatrix; obsdim=2)
	exp.(-Distances.pairwise(SqEuclidean(),X,Y; dims=obsdim) ./ (2*kernel_std(ker)^2))
end

"""
$(SIGNATURES)
Creates a `NyströmSkOp` object using the fixed set of landmark points `L`.
"""
function NyströmSkOp(ker, L::AbstractMatrix; stability_shifting = 1e-6)
	d = size(L,1)
	m = size(L,2)
	@debug "Building and inverting the kernel matrix of size $(m)×$(m)…"
	KS = Symmetric(kernelmatrix(ker, L; obsdim=2))
	if stability_shifting > 0
		KS += stability_shifting*I
	end
	K_sqrt_inv = sqrt(inv(KS))::Symmetric{Float64,Matrix{Float64}} # Ensure it's not complex
	@debug "Inverse computed."
	return NyströmSkOp(m, d, L, KS, K_sqrt_inv, ker)
end

sketch_eltype(::NyströmSkOp) = Float64
sketchcolumns!(outsk, A::NyströmSkOp, X, any::Nothing) = sketchcolumns!(outsk, A::NyströmSkOp, X) # TODO clean
function sketchcolumns!(outsk, A::NyströmSkOp, X) 
	outsk[:] = A.K_sqrt_inv*kernelmatrix(A.k, A.L, X; obsdim=2)
end

#######################################################################
#                              Learning                               #
#######################################################################

kernel_std(k::CL.GaussianKernel) = k.σ
kernel_std(k::KF.GaussianKernel) = 1.0
kernel_std(k::KF.TransformedKernel{<:KF.GaussianKernel, <:KF.ScaleTransform}) = 1/k.transform.s[1] # Not sure why this is a vector??
function sketchparams!(out, A::NyströmSkOp{DT, T, <:LinearOperator, <:UGaussianKernel} where {DT,T}, θ, prmsType::DiracsMixture)
	KLθ = kernelmatrix(A.k, A.L, θ; obsdim=2) # m×k
	out[:] = A.K_sqrt_inv*KLθ 
	function ∇skθ!(g, y)
		# The Jacobian J(θ₁) w.r.t. θ₁ is for the Gausssian kernel with variance σ²:
		# K^(-1/2) [(landmark₁ - θ₁)/σ² k(θ,landmark ₁) ; … ; same for landmark_m] ∊ℝ^m×d
		# And what we want is [J(θ₁)ᵀy, … ,J(θ_k)ᵀy]
		z = A.K_sqrt_inv*y
		w = (KLθ .* z)  # [k(L, θ₁), … k(L, θ_k)] .* (K^(-½) y)  (size m×k)
		sw = sum(w, dims=1) # size 1 × k
		res = ((A.L * w) .- (θ .* sw)) / kernel_std(A.k)^2 
		g[:] = res
	end
	return ∇skθ!
end

function sketchparams!(out, A::NyströmSkOp{DT, T, <:LinearOperator, <:UGaussianKernel} where {DT,T}, θ, prmsType::GaussianMixtureDiagCov)
	μ = θ[1:A.d,:]		# Centers (d×k)
	Σv = θ[A.d+1:end,:] # Vectorized covariances (d×k)
	L = A.L 	 # Landmark points (size d×m)
	@assert size(μ,1) == size(L,1)
	@assert size(μ,1) == size(Σv,1)
	ker = A.k	 # (Gaussian) Kernel function
	σ = kernel_std(ker)
	σ² = σ^2 # Kernel variance
	a = sqrt.(prod(Σv .+ σ², dims=1)) # 1 × k
	iΣvr = (Σv .+ σ²).^(-1)  # d × k
	b = exp.(-0.5*(sum(iΣvr.*μ.^2,dims=1)' .+ (iΣvr' * L.^2) .- 2*(iΣvr .* μ)'*L)) # k×m
	out_wo_K = σ^A.d ./ a .* b'
	out[:] = A.K_sqrt_inv * out_wo_K # m×k

	function ∇skθ!(g, y)
		∇μ = @view g[1:A.d,:]
		∇Σ = @view g[A.d+1:end,:]
		z = A.K_sqrt_inv*y 	# m×1
		fθKy = out_wo_K .* z 	# f(θ)⊙(K^{-1/2}y)  (m×k)
		sfθKy = sum(fθKy, dims=1) # 1×k
		# cf. Separated note if needed
		∇μ[:,:] .= iΣvr .* (L*fθKy - μ .* sfθKy) # d×k
		∇Σ[:,:] .= 0.5iΣvr.*((-1 .+ iΣvr.*μ.^2).*sfθKy + iΣvr.*(L.^2*fθKy - 2μ.*(L*fθKy)))
	end
	return ∇skθ!
end

# Learning

learning_skop(Ask::NyströmSkOp; kwargs...) = Ask
