import torch
import numpy as np

class GK_to_torch(object):
    def __init__(self, kernel):
        self.kernel = kernel
    def __call__(self, *args, **kwargs):
        torch_args = list()
        for a in args:
            if isinstance(a, np.ndarray):
                a = torch.from_numpy(a)
            torch_args.append(a)
        torch_kwards = dict()
        for kwa_key, kwa_val in kwargs.items():
            if isinstance(kwa_val, np.ndarray):
                kwa_val = torch.from_numpy(kwa_val)
            torch_kwards[kwa_key] = kwa_val
        out = self.kernel(*torch_args, **torch_kwards)
        if isinstance(out, torch.Tensor):
            out = out.numpy()
        return out
    def diag(self, X):
        if isinstance(X, np.ndarray):
            out = self.kernel.diag(torch.from_numpy(X))
        else:
            out = self.kernel.diag(X)
        if isinstance(out, torch.Tensor):
            out = out.numpy()
        return out
