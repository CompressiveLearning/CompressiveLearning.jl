# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

"""
$(TYPEDEF)
Abstract type for the parameters of a mixture. Note that the subtypes of this type are not intended to contain any parameters, but simply to describe what represent the parameters (which are passed separately, e.g. as a vector, to make it easier to directly modify them and use them in optimization procedures).
"""
abstract type MixtureType end
"""
$(TYPEDEF)
Mixture of diracs.
$(TYPEDFIELDS)
"""
mutable struct DiracsMixture <: MixtureType
	"Number of Diracs in the mixture"
	p::Int 		
end

"""
$(SIGNATURES)
Returns a tuple (`out`, `∇skθ!`) where
- `out` is the `m`×`k` matrix `[sketch(P_{θ_1}), …, sketch(P_{θ_k})]` (sketch of the atomic distributions associated to the parameters `θ₁,…,θ_k`).
- `∇skθ!` is the function (K^m -> K^{p×k}): y |-> [J_sketch(θ_1)^T y, …, (J_sketch(θ_k))^T y] where J_sketch(θ) denotes the Jacobian at θ of the function which sketches the atomic probability distribution associated to parameter θ.
"""
function sketchparams(A::SketchingOperator, θ, prmsType)
	out    = Array{sketch_eltype(A)}(undef, A.m, size(θ, 2))
	∇skθ! = sketchparams!(out, A, θ, prmsType)
	return (out, ∇skθ!)
end

"""
sketchparams!(out, A::SketchingOperator, θ, prmsType)
Writes to `out` the `m`×`k` matrix `[sketch(P_{θ_1}), …, sketch(P_{θ_k})]` (sketch of the atomic distributions associated to the parameters `θ₁,…,θ_k`).
Returns a function `∇skθ!` (K^m -> K^{p×k}): y |-> [J_sketch(θ_1)^T y, …, (J_sketch(θ_k))^T y] where J_sketch(θ) denotes the Jacobian at θ of the function which sketches the atomic probability distribution associated to parameter θ.
"""
function sketchparams! end

################################################
#  Generic constructor of sketching operators  #
################################################

"""
$(SIGNATURES)
Return the standard deviation of the kernel.
"""
function choose_std(X;
			 kernel_var = nothing,	# Chooses σ²=kernel_var
			 kernel_var_inter_var_ratio = nothing, # Chooses σ²=kernel_var_inter_var_ratio*inter_var
			 kernel_var_choice = nothing,
			 out_dic = nothing )

	@assert kernel_var isa Union{Real, Nothing}
	@assert kernel_var_choice isa Union{Symbol, Nothing}
	@assert kernel_var_inter_var_ratio isa Union{Real, Nothing}
	@assert (kernel_var_inter_var_ratio isa Nothing || kernel_var isa Nothing)
	if (kernel_var isa Real && kernel_var > 0) && (kernel_var_choice isa Nothing || kernel_var_choice == :user_provided)
		σ = sqrt(kernel_var)
		kernel_var_choice = :UserProvided
	elseif kernel_var_inter_var_ratio isa Real && kernel_var_inter_var_ratio > 0
		@assert ~(out_dic == nothing) && haskey(out_dic, :dataset_gmm_inter_var)
		σ = sqrt(kernel_var_inter_var_ratio*out_dic[:dataset_gmm_inter_var])
		kernel_var_choice = :FromInterVar
	elseif (kernel_var isa Nothing && kernel_var_inter_var_ratio isa Nothing) || (kernel_var_choice = :second_moment)
		σ = sqrt(sum(sum(b.^2) for b in centered_batch_iterator(X))/(size(X,1)*size(X,2)))	# Choose an adapted kernel
		kernel_var_choice = :SecondMoment
	else
		error("kernel_var must be a positive real or nothing, got $(kernel_var).")
	end
	if !isa(out_dic, Nothing)
		out_dic[:kernel_var_choice] = kernel_var_choice
		out_dic[:kernel_var] = σ^2
		out_dic[:kernel_std] = σ
		out_dic[:kernel_var_inter_var_ratio] = (kernel_var_inter_var_ratio == nothing) ? NaN : kernel_var_inter_var_ratio
	end
	return σ
end

"""
$(SIGNATURES)
Builds one sketching operator (without noise or subsampling).
Keyword arguments include:
- `activation`: `:sinusoid` (default), `:onebit`, `:square`, `:triangle`
- `linop_type`: `:dense` (default), `HDHDHD`, `HGHDHD`, `:fastfood`. See [1] for a description of `:HDHDHD` behavior, [Le et. al, 2013] for a description of `:fastfood`. 
- `kernel_type`: `:Gaussian` (default, recommended), `:Laplacian` (not recommended) or `:AdaptedRadius` (see [2]). Note that when using fast transforms, the kernel will differ slightly.
- `dithering`: :unif, :none, or :default (default, will use :unif for quantized operators and :none otherwise)
- `Nyström_sampling_type`: :uniform (default), :DPPY_leverage_scores, :greedy_leverage_scores. Sampling strategy used for the Nyström method (only used if `approximation_type` is set to `:Nyström`).

[1] Large-Scale High-Dimensional Clustering with Fast Sketching
	ICASSP 2018
	Antoine Chatalic, Rémi Gribonval and Nicolas Keriven

[2] Sketching for large-scale learning of mixture models
	Information and Inference (2017)
	Nicolas Keriven, Anthony Bourrier, Rémi Gribonval and Patrick Pérez
"""
function base_skop(m::Int, d::Int, σ; 
					dataset = nothing, # Required for Nyström
					approximation_type = :random_features, 
					activation = :sinusoid, 
					linop_type = :dense, 
					kernel_type = :Gaussian,
					dithering = :default,
					radial_correction = :dpad_radial_distribution,
					Nyström_sampling_type = :uniform, 
					Nyström_ls_regularization = 1e-2, 
					out_dic = nothing)

	dithering = (dithering == :default) ? default_dithering(activation) : dithering
	dithDic = (dithering == :none) ? Dict() : Dict(:dithering => dithering)

	A = if approximation_type == :random_features
		args = Any[m, d, build_kernel(kernel_type, σ; )]
		skop_constructor(activation)(args... ; 
									linop_type = linop_type, 
									radial_correction = radial_correction, 
									dithDic...)
	elseif approximation_type == :Nyström
		@assert dithering == :none "Dithering not supported with Nyström feature map."
		@assert dataset != nothing "Kwarg `dataset` must be provided for Nyström."
		if !isa(out_dic, Nothing)
			out_dic[:Nyström_sampling_type] = Nyström_sampling_type
			if Nyström_sampling_type != :DPPY_leverage_scores
				out_dic[:Nyström_ls_regularization] = Nyström_ls_regularization
			end
		end
		NyströmSkOp(m, dataset, build_kernel_kf(kernel_type, σ); 
					linop_type = linop_type, 
					ls_regularization = Nyström_ls_regularization,
					sampling_type = Nyström_sampling_type,
					out_dic = out_dic)
	else
		throw(ArgumentError("Unrecognized value '$approximation_type' for parameter 'approximation_type' in base_skop."))
	end
	if !isa(out_dic, Nothing)
		out_dic[:usefasttransforms] = (linop_type ≠ :dense)
		out_dic[:radial_correction] = radial_correction
		out_dic[:linop_type] = linop_type
		out_dic[:activation] = activation
		out_dic[:dithering] = dithering
		out_dic[:kernel_type] = kernel_type
		out_dic[:approximation_type] = approximation_type
	end
	return A
end

"""
$(SIGNATURES)
Returns a sketching operator built on `Ask` with additional features (e.g. subsampling, privacy).

Extra kwargs include:
- `DP_ε`: positive Float (default: `0.0`). Ensures `(DP_ε, DP_δ)`-differential privacy when ``ε>0``.
- `DP_δ`: only used when ``DP_ε>0``. Should be much smaller than the number of samples for concrete guarantees.
- `DP_γ`: float in ]0,1[ or :auto (will pick default value 0.98 or use the heuristic if possible)
- `DP_use_ζ`: boolean (default: `true`). If set to true, noise will only be added on the sum of features, not on the number of samples.
- `observations`: integer ``r≤m``, or :all (default, which is equivalent to ``r=m``).
- `samples_subsampling_rate`: float between 0.0 (strict) and 1.0 (default, i.e. no subsampling). 
- `samples_subsampling_method`: sampling method, can be `:WOR` or `:Bernoulli`.
"""
function composed_skop(Ask::SketchingOperator;
					DP_ε = 0.0,
					DP_δ = 0.0,
					DP_γ = :auto,
					DP_use_ζ = true,
					DP_relation = AddOrRemoveOneSample(),
					DP_RQF_Δ₂_computation_method = nothing,
					observations = :all,
					samples_subsampling_rate = 1.0,
					samples_subsampling_method = :WOR,
					out_dic = nothing)

	m = sketchsize(Ask)
	AskSub, n_obs = if observations != :all
		@assert (observations isa Int) && observations ≥ 1 && observations ≤ m
		FeaturesSubsamplingSkOp(Ask, observations), observations
	else 
		Ask, m 
	end
	AskSubSamples = if samples_subsampling_rate ≠ 1.0
		SamplesSubsamplingSkOp(AskSub, samples_subsampling_rate, samples_subsampling_method)
	else
		AskSub
	end
	AskFinal = if (DP_ε > 0.0 && DP_ε < Inf)
		DP_skop(AskSubSamples, DP_relation, DP_use_ζ, DP_ε, DP_δ, n_obs, samples_subsampling_rate, DP_γ; 
				DP_RQF_Δ₂_computation_method = DP_RQF_Δ₂_computation_method,
				out_dic = out_dic)
	else 
		AskSubSamples
	end

	if !isa(out_dic, Nothing)
		out_dic[:observations] = observations
		out_dic[:observations_num] = n_obs
		out_dic[:freqs_subsampling_rate] = n_obs/m
		out_dic[:samples_subsampling_rate] = samples_subsampling_rate
		out_dic[:features_subsampling_rate] = (observations == :all) ? 1.0 : observations/m
		out_dic[:DP_ε] = DP_ε
		if (DP_ε < Inf) 
			out_dic[:DP_δ] = DP_δ
		end
	end
	return AskFinal
end

"""
$(SIGNATURES)
Returns a tuple (`Ask`, `Aln`) for sketching and learning, using the same random operator.

Supported parameters include:
- `approximation_type` (default: :random_features)
- `sketching_activation` (default: :sinusoid)
- `learning_activation` (default: :sinusoid)
- `linop_type` (default: :dense)
- `kernel_type` (default: :Gaussian)
- `radial_correction` (default: :dpad_radial_distribution)
- `Nyström_sampling_type` (default: :uniform)
- `Nyström_ls_regularization` (default: 1e-2) 

See also [`base_skop`](@ref).
"""
function skops_pair(m, d, σ;
					dataset = nothing, 
					approximation_type = :random_features, 
					sketching_activation = :sinusoid,
					learning_activation = :sinusoid,
					linop_type = :dense,
					kernel_type = :Gaussian,
					radial_correction = :dpad_radial_distribution,
					Nyström_sampling_type = :uniform, 
					Nyström_ls_regularization = 1e-2, 
					out_dic = out_dic,
					kwargs...)

	Ask = base_skop(m, d, σ; 
					dataset = dataset, 
					approximation_type = approximation_type, 
					activation = sketching_activation,
					linop_type = linop_type,
					kernel_type = kernel_type,
					radial_correction = radial_correction, 
					Nyström_sampling_type = Nyström_sampling_type, 
					Nyström_ls_regularization = Nyström_ls_regularization, 
					out_dic = out_dic)
	Ask_composed = composed_skop(Ask; 
								 out_dic = out_dic,
								 kwargs...)
	Aln = learning_skop(parentskop(Ask_composed); 
						out_dic = out_dic,
						learning_activation = learning_activation)
	return (Ask_composed, Aln)
end

"""
$(SIGNATURES)
Returns a sketching operator for learning (similar to `Ask` but with unquantized features, no subsampling etc.).
"""
function learning_skop(Ask; learning_activation = :sinusoid,
					out_dic = nothing)

	if !isa(out_dic, Nothing)
		out_dic[:learning_activation] = learning_activation
	end
	return skop_constructor(learning_activation)(Ask)
end


function skop_constructor(q)
	if 	   (q == :onebit) 			QuantizedFourierSkOp
	elseif (q == :triangle) 		TriangleFourierSkOp
	elseif (q == :tweakedtriangle) 	TweakedTriangleFourierSkOp
	elseif (q == :sinusoid) 		FourierSkOp
	elseif (q == :square) 			SqRandomSkOp
	else @error "Unrecognized activation function (provided '$(q)', but should be ':onebit', ':triangle' or ':sinusoid')." end
end

function default_dithering(q)
	if	   (q == :onebit) || (q == :triangle) || (q == :tweakedtriangle) 		return :unif
	elseif (q == :sinusoid) || (q == :square)									return :none
	else @error "Unrecognized activation function '$(q)'." end
end

function quantization_renormalization(qsk, qln)
	if qsk == qln 1.0
	elseif qsk == :onebit && qln == :sinusoid π/4
	else @error "Unexpected renormalization from $qsk to $qln." end
end
