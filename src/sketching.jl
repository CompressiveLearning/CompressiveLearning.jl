# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

"""
$(TYPEDEF)
Base type for non-composite sketching operators.
"""
abstract type BaseSkOp end		# base = not composite

"""
$(TYPEDEF)
Abstract type for operators computing linear sketches of the data distribution.  
"""
abstract type LinearSkOp <: BaseSkOp end			# linear w.r.t. distributions
sketchsizeB(A::BaseSkOp) = sketchsize(A)*sizeof(sketch_eltype(A))
"""
$(SIGNATURES)
Return the size (number of elements) of a sketch produced by `A`.
"""
sketchsize(A::BaseSkOp) = A.m
parentskop(A::BaseSkOp) = A

"""
Returns the type of the elements of a sketch computed by the sketching operator `A`.
"""
sketch_eltype(A)

"""
$(TYPEDEF)
Base type for linear sketching operators which directly depend on another operator. The “parent” operator is by convention always stored in a field called `p`, and the top operator in the chain can be obtained with `parentskop`.
"""
abstract type ComposedLinearSkOp <: LinearSkOp end	# composed = has another parent LinearSkOp (field p)
sketch_eltype(A::ComposedLinearSkOp) = sketch_eltype(A.p)
sketchsize(A::ComposedLinearSkOp) = sketchsize(A.p)
parentskop(A::ComposedLinearSkOp) = parentskop(A.p)

"""
Abstract internal type for sketches. A `BaseSketch` is basically an object containing a field `s`, and whose type can be used for dispatch.
"""
abstract type BaseSketch end		# Abstract wrapper type for sketches
unwrap(s::BaseSketch) = s.s
postprocess!(s, A::BaseSkOp, n) = s		# Unless specified, there is no postprocessing

############################################################################
#  Composite Sketches (compute different sketches from same data at once)  #
############################################################################

"""
$(TYPEDEF)
Alias for an array of "base" sketching operator, which can then all be evaluated at once.
"""
const CompositeSkOp = Array{BaseSkOp}
"""
Abstract type for composite sketches, i.e. sketches computed using multiple sketching operators at once.
"""
const CompositeSketch = Array{BaseSketch}

sketchsizeB(A::CompositeSkOp) = reduce(+, [sketchsizeB(skop) for skop in A])
sketchsize(A::CompositeSkOp) = reduce(+, [sketchsize(skop) for skop in A])
sketch_onebatch(cpSkOp::CompositeSkOp, X) = [sketch_onebatch(A, X) for A in cpSkOp]
# TODO update in place??
Base.merge(s1::CompositeSketch, s2::CompositeSketch) = [merge(es1, es2) for (es1, es2) in zip(s1, s2)]
unwrap(s::CompositeSketch) = [unwrap(t) for t in s]
function postprocess!(s::CompositeSketch, A::CompositeSkOp, n)
	for (A_base, s_base) in zip(A, s)
		postprocess!(s_base, A_base, n)
	end
end

# ===== Union types =====

"""
$(TYPEDEF)
Base type for sketching operators. Any new sketching operator should subtype (possibly undirectly) this type.
"""
const SketchingOperator = Union{CompositeSkOp, BaseSkOp}
"""
$(TYPEDEF)
Base type for sketches.
"""
const Sketch = Union{CompositeSketch, BaseSketch}
Base.merge(::Nothing, s::Sketch) = s

#####################
#  Batch sketching  #
#####################

"""
$(SIGNATURES)
Returns the sketch of `X` for the sketching operator `A`. 
Unless `force_single_sketch` is set to `true`, the sketch is computed by splitting `X` into smaller batches and sketching each batch individually for memory efficiency.
The batch size is selected using `BatchIterators.choose_batchsize`, but in particular the following constraints can be used:
- `maxmemGB`: maximum memory of a sketched batch;
- `maxbatchsize`: maximum number of samples of a batch.
- `batch_size`: exact batch size to use.
- `force_single_batch`: will sketch all the data as one single batch.
"""
function sketch(A::SketchingOperator, X; 
				force_single_batch = false,
				batch_size = nothing,
				out_dic = nothing,
				kwargs...)
	d, n = size(X)
	bs = force_single_batch ? n : (batch_size ≠ nothing ? batch_size : choose_batchsize(d, n; sizeoneB = sketchsizeB(A), kwargs...))
	if out_dic ≠ nothing
		out_dic[:batch_size] = bs
	end
	sketch(A, X, bs)
end
"""
$(SIGNATURES)
Returns the sketch of `X` for the sketching operator `A`, computed by splitting `X` into batches of `batchsize` data samples. 
"""
function sketch(A::SketchingOperator, X, batchsize)
	s = nothing
	n = size(X, 2)
	bi = BatchIterator(X, batchsize=batchsize)
	@debug "Sketching the dataset…"
	p = Progress(bi.length, 1, "Sketching (d=$(size(X,1)), m=$(sketchsize(A)), $(bi.length) batches)…  ")
	for batch in bi
		s = merge(s, sketch_onebatch(A, batch))	# TODO in place?
		next!(p)
	end
	postprocess!(s, A, n)
	@info "Sketching finished."
	return unwrap(s)	# Extract sketch (e.g. vector) from Sketch wraper
end

"""
$(SIGNATURES)
Applies the sketching operator `A` on one batch of data batch `X`. 
"""
sketch_onebatch(A::SketchingOperator, X)

######################
#  MinMax sketching  #
######################

"""
$(TYPEDEF)
A sketching operators which computes the bounds of the dataset for each dimension.

# Fields
$(TYPEDFIELDS)
"""
struct MinMaxSkOp <: BaseSkOp 
	" Dimension of the data "
	d # Dimension
end
sketchsize(A::MinMaxSkOp) = 2*A.d
sketch_eltype(A::MinMaxSkOp) = Float64
struct MinMaxSketch{T} <: BaseSketch
	s::Tuple{Array{T},Array{T}}
end

sketch_onebatch(s::MinMaxSkOp, X) = MinMaxSketch((minimum(X; dims=2), maximum(X; dims=2)))
Base.merge(s1::MinMaxSketch, s2::MinMaxSketch) = MinMaxSketch((min.(s1.s[1], s2.s[1]), max.(s1.s[2], s2.s[2])))

"""
$(SIGNATURES)
Returns a tuple `(s, bounds)`, were s is the sketch of the dataset `X` w.r.t. to the sketching operator `A`, and bounds is a tuple (lb, ub) of the lower and upper bounds of X. The bounds are computed together with the sketch in one single pass over the data.
"""
function sketch_and_bounds(A::LinearSkOp, X; kwargs...)
	# Create a composite sketching operator calculating both at same time
	boundsSkOp = MinMaxSkOp(size(X,1))
	compositeSkOp = [A boundsSkOp]
	compositeSketch = sketch(compositeSkOp, X; kwargs...)
	Tuple(compositeSketch)
end

################################
#  Linear Sketching Operators  #
################################

"""
$(TYPEDEF)
Wrapper type to memorize the (mean) sketch and the number of samples on which it has been computed (for further pooling).
$(TYPEDFIELDS)
"""
mutable struct LinearSketch{T} <: BaseSketch
	"The numerical sketch (typically a vector)"
	s::T	
	"Number of samples from which the sketch has been computed"
	n::Int	
end
Base.merge(s1::LinearSketch, s2::LinearSketch) = LinearSketch((s1.n*s1.s + s2.n*s2.s)/(s1.n + s2.n), s1.n + s2.n)
sketch_onebatch(A::LinearSkOp, X) = LinearSketch(vec(mean(sketchcolumns(A, X), dims=2)), size(X,2))

"""
$(SIGNATURES)
Sketches the columns of X without averaging them. A new array is created to store the result.
"""
function sketchcolumns(A, X, mask = nothing)
	outsk = Array{sketch_eltype(A)}(undef, (mask isa Nothing ? sketchsize(A) : length(mask)), size(X,2))
	sketchcolumns!(outsk, A, X, mask)
end

linop_aux(Ω, X, ξ)	  	 = (ξ isa Nothing) ? Ω'*X 	  			   : Ω'*X 	 	  .+ ξ
linop_aux(Ω, X, ξ, mask) = (ξ isa Nothing) ? Ω[:,mask]'*X 		   : Ω[:,mask]'*X .+ ξ[mask]
linop(Ω, X, ξ, mask) 	 = (mask isa Nothing) ? linop_aux(Ω, X, ξ) : linop_aux(Ω, X, ξ, mask)

gen_dithering(n) = 2π*rand(n)
function gen_dithering(dithering_type::Symbol, m)
	@assert dithering_type == :unif || dithering_type == :none
	(dithering_type == :unif) ? gen_dithering(m) : nothing
end

##########################################################
#  Generic operator: linear random proj + non-linearity  #
##########################################################

"""
$(TYPEDEF)
# Fields
$(TYPEDFIELDS)
"""
struct FourierSkOp{T<:AbstractArray{Float64,2}, U<:Union{Nothing, Vector{Float64}}} <: LinearSkOp
	"Sketch size"
	m::Int
	"Dimension"
	d::Int	
	"Matrix of frequency vectors (columns)"
	Ω::T 
	"Dithering Vector. The sketch is computed as ``exp.(i(Ωᵀx + ξ))`` when ``ξ≠0``."
	ξ::U	
end
FourierSkOp(Ω, ξ = nothing) = FourierSkOp(size(Ω, 2), size(Ω, 1), Ω, ξ)
FourierSkOp(A::BaseSkOp) = FourierSkOp(A.Ω, A.ξ)

"""
$(SIGNATURES)
A sketching operator which computes a linear sketch of random Fourier features, i.e. ``s(X) = mean(Φ(xᵢ)) `` with ``Φ(x) = exp.(i Ωᵀx)``. Here ``Ω`` is a (randomly drawn) matrix whose columns can be interpreted as frequency vectors, whose distribution depends on the kernel `k`.
"""
function FourierSkOp(m::Int, d::Int, k::Kernel; 
					 dithering = :none,
					 kwargs...)
	Ω = drawfrequencies(m, d, k; kwargs...)
	ξ = gen_dithering(dithering, m)
	FourierSkOp(Ω, ξ)
end
sketch_eltype(::FourierSkOp) = Complex{Float64}
sketchcolumns!(outsk, A::FourierSkOp, X, mask = nothing) = vcis!(outsk, linop(A.Ω, X, A.ξ, mask))

###################################################
#  Operator using subsampling of the frequencies  #
###################################################

"""
$(SIGNATURES)
An operator which computes only a subsample of the features of the parent sketching operator `P`, using `nb_obs` random observations (without replacement) per input sample. 
"""
struct FeaturesSubsamplingSkOp <: ComposedLinearSkOp
	p	# Parent operator
	r
	function FeaturesSubsamplingSkOp(p, observations::Int)
		@assert typeof(p.Ω) <: Array 	# TODO support fast transforms
		new(p, observations)
	end
end
function sketch_onebatch(A::FeaturesSubsamplingSkOp, X) 
	n = size(X, 2)
	s = zeros(sketch_eltype(A.p), A.p.m)
	for i = 1:n
		mask = sort(sample(1:A.p.m, A.r; replace = false))
		s[mask] .+= vec(sketchcolumns(A.p, X[:,i], mask))
	end
	α = A.r/sketchsize(A)
	LinearSketch(s./(α*n), n)
end


abstract type SamplingScheme end
struct WORSampling <: SamplingScheme
	"Subsampling rate"
	α::Float64
end
struct BernouilliSampling <: SamplingScheme
	"Subsampling rate"
	α::Float64
end

@doc doc"""
	SamplesSubsamplingSkOp(p, α::Float64, sampling)
This operator uses the parent sketching operator `p` after subsampling the dataset samples. Parameter `sampling` can be either `Bernouilli` for i.i.d. Bernouilli sampling, or `:WOR` for a sampling without replacement.
"""
struct SamplesSubsamplingSkOp{T <: SamplingScheme} <: ComposedLinearSkOp
	"Parent sketching operator"
	p::SketchingOperator
	"Subsampling scheme"
	sampling::T
	function SamplesSubsamplingSkOp(p, α::Float64, sampling_type)
		@assert α > 0 && α ≤ 1.0
		@assert sampling_type in [:WOR, :Bernouilli]
		sampling = (sampling_type == :WOR ? WORSampling(α) : BernouilliSampling(α))
		new{typeof(sampling)}(p, sampling)
	end
end
sample_indexes(A::SamplesSubsamplingSkOp{WORSampling}, n) = sort(sample(1:n, round(Int, A.sampling.α*n); replace = false))
sample_indexes(A::SamplesSubsamplingSkOp{BernouilliSampling}, n) = convert(Array{Bool}, rand(Distributions.Bernoulli(A.sampling.α), n))
function sketch_onebatch(A::SamplesSubsamplingSkOp, X)
	idx = sample_indexes(A, size(X,2))
	αX = X[:,idx]
	sketch_onebatch(A.p, αX)
end
