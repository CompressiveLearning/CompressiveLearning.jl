# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

# Related paper:
#
# [1] Sketching for large-scale learning of mixture models
# 	  Information and Inference: A Journal of the IMA, 7:3 (2017), pp. 447--508
# 	  N. Keriven, A. Bourrier, R. Gribonval, P. Pérez

using GaussianMixtures

"""
$(TYPEDEF)
Type for mixtures of Gaussians with diagonal covariances.
"""
mutable struct GaussianMixtureDiagCov <: MixtureType
	p::Int 		# Parameters dimension
	d::Int		# Input space dimension
end

#######################################################################
#                CL-OMPR internals for GaussianMixture                #
#######################################################################

"""
$(SIGNATURES)
- `mean_σ²` should here be an indicator of the "mean" intra-variance of the clusters.
- `bounds` are initialization bounds, which will typically be user-provided or estimated from the data when `CLOMPR` is called without `init_bounds`.
"""
function init_param(θ, M::GaussianMixtureDiagCov; 
					init_method = :uniform, 
					variance_init_method = :random_around_mean_var, 
					mean_σ² = nothing,
					bounds=(-Inf,Inf))

	lb, ub = bounds
	# TODO it would be reasonable to clean CLOMP so that we always have a vector
	bounds_μ = if (length(lb) > 1) 
		(lb[1:M.d], ub[1:M.d])
	else
		bounds 
	end
	init_μ = init_param(θ[1:M.d,:], DiracsMixture(M.d), 
						init_method = init_method, 
						bounds = bounds_μ)
	if variance_init_method	== :random_around_mean_var
		@assert mean_σ² ≠ nothing
		init_σ² = mean_σ² * (0.5 .+ rand()*ones(M.d))
		# init_σ² = mean_σ² * (1.0 .+ 0.1*randn()*ones(M.d))
	else 
		error("Unsupported value '$variance_init_method' for the keyword argument 'variance_init_method'.")
	end
	return [init_μ; init_σ²]
end

function sketchparams!(out, A::FourierSkOp, θ, M::GaussianMixtureDiagCov)
	Ω = A.Ω
	# Compute sketchs
	sketchcolumns!(out, A, θ[1:M.d,:])	 # exp.(i Ωᵀμ)
	out .*= exp.(-0.5 * (Ω.^2)' * θ[M.d+1:end,:])
	# Express the gradients
	function ∇skθ!(g, y)
		G = reshape((@view g[:]), M.p, :)	# 2d × k matrix corresponding to [∇μ; ∇Σ]
		# There is a sign error in Nicolas' notes for that:
		G[1:M.d, :] .= Ω*(real(out).*imag(y) - imag(out).*real(y))					# ∇μ
		G[M.d+1:end, :] .= -0.5 * Ω.^2 *(real(out).*real(y) + imag(out).*imag(y))	# ∇Σ
	end
	return ∇skθ!
end

#######################################################################
#                              Wrappers                               #
#######################################################################

"""
$(SIGNATURES)
Main wrapper for compressive Gaussian mixture modeling with diagonal covariances.
The parameters `m_factor` and `m_factor_eq_reals` are computed with 
respect to the total number of parameters `2kd`.
See also: [CKM](@CKM)
"""
CGMM

function CGMM(X, k::Int; 
			  m_factor = 10, 
			  m_factor_eq_reals = nothing, 
			  approximation_type = :random_features, 
			  kernel_var = nothing,	
			  kernel_var_inter_var_ratio = nothing, 
			  kernel_var_choice = nothing, 
			  sketching_activation = :sinusoid, 
			  learning_activation = :sinusoid,
			  decoder = :CLOMPR,
			  decoder_kwargs = Dict(),
			  out_dic = nothing,
			  kwargs...)

	σ = choose_std(X; kernel_var, kernel_var_inter_var_ratio, kernel_var_choice, out_dic)
	d = size(X, 1)
	nb_params = (2d)*k # Total number of parameters for a mixture
	m = if m_factor_eq_reals != nothing
		A_tmp, _ = skops_pair(10, d, σ; dataset = X, approximation_type, sketching_activation, learning_activation, out_dic, kwargs...)
		size_eqr = size_eq_reals(sketch_eltype(A_tmp)) # typically 1.0 or 2.0
		m_used = Int(round(m_factor_eq_reals*nb_params/size_eqr))	# 
		if !isa(out_dic, Nothing)
			m_eq_reals = Int(m_used * size_eqr)
			out_dic[:m_factor_eq_reals] = m_factor_eq_reals
			out_dic[:m_eq_reals] = m_eq_reals
		end
		m_used
	else
		Int(round(m_factor*nb_params))		 
	end # Number of frequencies
	@debug "CGMM with m=$(m) frequencies (d=$(d), k=$(k), m_factor=$(m/nb_params), m_factor_eq_reals=$(m_factor_eq_reals), approximation_type=$(approximation_type), p=$(nb_params) parameters."

	# Generate sketching operators
	(Ask, Aln) = skops_pair(m, d, σ; dataset = X, 
							approximation_type, sketching_activation, learning_activation, out_dic = out_dic,
							kwargs...)
	@debug "Sketching/Learning operators generated."

	if !isa(out_dic, Nothing)
		out_dic[:m_factor] = m_factor
		out_dic[:m] = m
	end
	CGMM(X, k, Ask, Aln; decoder, decoder_kwargs, out_dic)
end

function CGMM(X, k::Int, Ask::SketchingOperator, Aln::SketchingOperator=Ask;
					sketching_activation = :sinusoid, learning_activation = :sinusoid, 
			 		decoder = :CLOMPR,
			 		decoder_kwargs = Dict(),
			 		out_dic = nothing,
			 		kwargs...)

	d = size(X,1)
	radX = maximum(maximum(sqrt.(sum(b.^2,dims=1))) for b in BatchIterator(X))
	mean_σ² = mean_intra_var_Keriven2017(X) # For CLOMP initialization

	# 1. Compute the sketch
	((s, bds), sketching_time) = @timed sketch_and_bounds(Ask, X)
	if (sketching_activation != learning_activation)
		s .*= quantization_renormalization(sketching_activation, learning_activation) end

 	# 2. Recover centroids and weights by solving the inverse problem
	((θ, α), learning_time) = @timed GMM_inverse_problem(Aln, s, k; 
										data_bounds = bds, 
										decoder, decoder_kwargs, radX, mean_σ², out_dic)

	# If a dictionary out_dic is provided, store additional infos
	if !isa(out_dic, Nothing)
		# out_dic[:m_factor] = m_factor
		# out_dic[:m] = m
		out_dic[:decoder] = decoder
		out_dic[:sketching_time] = sketching_time
		out_dic[:learning_time] = learning_time
		for (k, v) in decoder_kwargs
			@assert typeof(k) <: Symbol
			out_dic[k] = decoder_kwargs[k]
		end
	end

	α ./= sum(α)
	GMM = MixtureModel(map(i -> MvNormal(θ[1:d, i], LinearAlgebra.Diagonal(θ[d+1:end, i])), 1:k), α)
	return GMM
end


"""
$(SIGNATURES)
Returns a pair (θ,α) of GMM parameters and weights matching the sketch `s`.
Here θ is a d×k matrix whose columns are diracs, and α is a k-dimensional vector of weights.
"""
function GMM_inverse_problem(A, s, k ; 
								decoder = :CLOMPR, 
								data_bounds=(-Inf,Inf),
								decoder_kwargs = Dict(),
								rel_min_σ² = 1e-4,
								radX = Inf,
								mean_σ² = nothing, 
								out_dic = Dict())
	p = 2A.d
	t = GaussianMixtureDiagCov(p, A.d)
	# Define bounds for parameters (stacking mean and diagonal covariance)
	(lb, ub) = clomp_bounds(data_bounds, t; rel_min_σ², radX)

	# Solve the inverse problem
	if decoder == :CLOMPR
		CLOMPR(s, k, t, A; 
			   bounds = (lb, ub),
			   out_dic, 
			   init_extra_params = Dict(:mean_σ² => mean_σ²), 
			   decoder_kwargs...)
	else
		error("Decoder $(algorithm) not supported for GMM fitting.")
	end
end

"""
$(SIGNATURES)
Returns parameter bounds for clompr using some input bounds on the data and additional information.
- `input_bounds` should be a tuple of bounds on the data (scalar or vectorial).
"""
function clomp_bounds(input_bounds, t::GaussianMixtureDiagCov; 
						rel_min_σ², radX = Inf)
	lb, ub = input_bounds
	min_σ² = if (all(isfinite.(lb)) && all(isfinite.(ub)))
		(ub .- lb).^2 * rel_min_σ²
	else
		@warn "No bounds have been provided on the data. The lower-bound $(min_σ²) on the variances is used for stability but might not be adapted to the data."
		rel_min_σ²
	end
	if (length(lb)==t.d) lb = [lb; min_σ²] end
	if (length(ub)==t.d) ub = [ub; radX^2*ones(t.d)] end
	if (lb isa Number) lb = repeat([lb, min_σ²], outer=t.d) end
	if (ub isa Number) ub = repeat([ub, radX^2], outer=t.d) end
	@assert length(lb) == length(ub) == t.p
	return (lb, ub)
end

#######################################################################
#                             Evaluation                              #
#######################################################################

"""
$(SIGNATURES)
Computes an approximation of the standard Kullback-Leibler divergence KL(P|Q) using a sample of `n` points drawn i.i.d. from `P`.
"""
function KL(P, Q; n = 2e5, blocksize = 10000)
	kl = 0.0
	nblocks = ceil(Int, n/blocksize)
	for iblock = 1:nblocks
		XP = rand(P, blocksize)
		kl += sum(logpdf(P, XP) - logpdf(Q, XP))
	end
	kl/n
end
"""
$(SIGNATURES)
Computes an approximation of the symmetric Kullback-Leibler divergence between `P` and `Q`.
"""
symKL(P, Q; kwargs...) = KL(P, Q; kwargs...) + KL(Q, P; kwargs...)

# loglhd(GMM, X) = sum(logpdf(GMM, X))/size(X,2)
loglhd(GMM, X) = sum(sum(logpdf(GMM, B)) for B in BatchIterator(X))/size(X,2)

"""
$(SIGNATURES)
Computes log-likelihood of `GMM` for the data `X`, and the symmetric KL divergence w.r.t. to the generative model if this one is passed as `dic[:non_numerical][:dsGMM]` (TODO: maki this cleaner).
If `compare_EM`, then the same quantities are computed for a GMM fitted to the data `X` using the EM algorithm from the `GaussianMixtures` package.
"""
function evaluate_GMM(X, GMM, k, dic = Dict{Symbol, Any}(); 
					  compute_symKL = false,
					  compare_EM = false)
	dic[:loglhd] = loglhd(GMM, X)
	if compute_symKL
		@assert haskey(dic, :non_numerical) && haskey(dic[:non_numerical], :dsGMM)
		trueGMM = dic[:non_numerical][:dsGMM]
		dic[:symKL] = symKL(trueGMM, GMM)
	end
	if compare_EM
		GMM_EM = EM_GMM(X, k)
		dic[:loglhd_EM] = loglhd(GMM_EM, X)
		if compute_symKL
			@assert haskey(dic, :non_numerical) && haskey(dic[:non_numerical], :dsGMM)
			trueGMM = dic[:non_numerical][:dsGMM]
			dic[:symKL_EM] = symKL(trueGMM, GMM_EM)
			dic[:rsymKL] = dic[:symKL]/dic[:symKL_EM]
		end
	end
end

#######################################################################
#                            Other methods                            #
#######################################################################


"""
$(SIGNATURES)
Fits a mixture of `k` Gaussians (with diagonal covariances) to the dataset `X` using the EM algorithm of the GaussianMixtures package.
"""
function EM_GMM(X::AbstractMatrix, k; its=10, out_dic=Dict(), kwargs...)
	Xt = copy(transpose(X))		# Sadly I don't see any way to avoid that given the API
	m = GaussianMixtures.GMM(k, Xt; 
							 method = :kmeans, 
							 kind = :diag, 
							 nInit = 50,  # for kmeans
							 nIter = its, # for EM
							 nFinal = its)
	MixtureModel(m) # TODO Not sure why it doesn't work anymore
end

EM_GMM(X, k) = EM_GMM(X[:,:], k) # Force load in memory

