# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

# Related paper:
#
# [1] Compressive statistical learning with random feature moments
#	  arXiv:1706.07180
#	  Rémi Gribonval, Gilles Blanchard, Nicolas Keriven and Yann Traonmilin
#	  2017

"""
$(TYPEDEF)
Mixture type for a low-rank representation of a symmetric matrix. 
"""
mutable struct LowRankSymFactor <: MixtureType
	p
end

#######################################################################
#                         Sketching Operator                          #
#######################################################################

"""
$(TYPEDEF)
Base type for non-composite sketching operators.

# Fields
$(TYPEDFIELDS)
"""
struct SqRandomSkOp{T<:AbstractArray{Float64,2}} <: LinearSkOp
	m::Int	# Sketch size
	d::Int	# Dimension
	Ω::T	# Matrix of frequencies
end

SqRandomSkOp(Ω) = SqRandomSkOp(size(Ω, 2), size(Ω, 1), Ω)
SqRandomSkOp(A::BaseSkOp) = SqRandomSkOp(A.Ω)
SqRandomSkOp(m, d; kwargs...) = SqRandomSkOp(m, d, UnitPCAKernel(); kwargs...)
SqRandomSkOp(m, d, ::UnitPCAKernel; kwargs...) = SqRandomSkOp(drawfrequencies(m, d, 1.0; kwargs..., radialdist = :Unit))
SqRandomSkOp(m, d, ::ChiPCAKernel; kwargs...) = SqRandomSkOp(drawfrequencies(m, d, 1.0; kwargs..., radialdist = :Chi))

sketch_eltype(::SqRandomSkOp) = Float64
function sketchcolumns!(outsk, A::SqRandomSkOp, X, mask = nothing)
	outsk .= linop(A.Ω, X, nothing, mask).^2
end


#######################################################################
#                           Inverse problem                           #
#######################################################################

# ===== CL-OMPR (ADMiRA?) =====

function sketchparams!(out, A::SqRandomSkOp, θ, prmsType::LowRankSymFactor)
	sketchcolumns!(out, A, θ)		# [(ωᵢᵀθ)²]_{1≤j≤m}	, is m × k 
	function ∇skθ!(g, y)
		# 2 Ω (Ωᵀθ ⊙ y), but Ωᵀθ = sqrt.(sketch)
		g[:] = 2A.Ω * (sqrt.(out) .* y)
	end
	return ∇skθ!
end

# ===== Nuclear norm relaxation =====

@doc doc"""
Computes $ A(UUᵀ)=\[ωⱼᵀUUᵀωⱼ\]_{j=1}^{m} $ for a `d`-by-`r` matrix `U` and a `d`-by-`m` operator Ω.
"""
sqrandom(Ω, U) = sum((Ω'*U).^2; dims=2)

using JuMP
# using ProxSDP
using SCS
# using CSDP # > stuck at edge of primal feasibility
# using SDPA # > solver terminated with status SLOW_PROGRESS
# SDPLR > installation problem
# using DSDP # problem installation

@doc doc"""
Solves $\min Tr(X)$ s.t. $X$ is SDP and $A(X)=s$.
"""
function lr_recovery_SCS(A, s, d, k)
	m = Model(with_optimizer(SCS.Optimizer))
	@variable(m, X[1:d, 1:d], PSD)
	for k in 1:length(s)
	    @constraint(m, A.Ω[:, k]'*X*A.Ω[:, k] == s[k])
	end
	@objective(m, Min, tr(X))
	optimize!(m)
	status = termination_status(m)
	if status ≠ MOI.OPTIMAL  error("Solver terminated with status $status.") end
	return covPCA(value.(X), k)
end


function rankproj_factor(U, k)
	@assert all(U .!= NaN)
	@assert all(U .!= Inf)
	U₁, S, U₂ = svd(U)
	U₁[:, 1:k] .* S[1:k]'
end

@doc doc"""
	RFRM_lbfgs(Ω, U, s)
Robust Factorized Rank Minimization: min_U ‖A(UUᵀ)-s‖².
"""
function RFRM_lbfgs(A, s, d, k; 
					out_dic = nothing, 
					optimrankfactor = 1.0,
					init = :random)
	@debug "Entering RFRM_lbfgs"
	r = min(max(k, Int(optimrankfactor*k)), d)
	if out_dic ≠ nothing 
		out_dic[:OptimRank] = r 
	end
	function aux!(g, Ul)
		U = reshape(Ul, d, r)
		∇U = reshape(g, d, r)
		# Compute Objective
		residual = sqrandom(A.Ω, U) - s
		obj = sum(residual.^2)
		# Compute Gradient
		R = A.Ω'*U		# m × k
		∇U .= 4*(A.Ω*(residual.*R))
		return obj			# objective = ‖A(UUᵀ)-s‖²
	end
	
	x₀ = if init == :spectral
		vec(spectral_init(A, s, k))
	elseif init == :random
		randn(d*r)
	else
		error("Unknown init $init for RFRM_lbfgs.") 
	end
	# x, _ = @suppress_out LBFGSBw(aux!, x₀; maxiter = 300)
	x, _ = LBFGSBw(aux!, x₀; maxiter = 300)
	U = reshape(x, d, r)
	@assert all(U .!= NaN) && all(U .!= Inf)
	rankproj_factor(U, k)
end

@doc doc"""
	affine_proj_Chol(cholΩ, X)
Computes the vector $[ω₁ᵀΩω₁, …]$ using a cholesky factorization of `X`.
"""
affine_proj_Chol(Ω, X) = sum((Ω'*(cholesky(X).U)').^2; dims=2)
affine_proj(Ω, X) = [Ω[:,i]'*X*Ω[:,i] for i in 1:size(Ω,2)]

"""
$(SIGNATURES)
Singular value projection (experimental)
"""
function SVP(A, s, d, k; 
			 ε = 1e-2, 
			 η₀ = 0.1,
			 η_incr = 1.1,
			 η_decr = 0.5,
			 η_max = 10e3,
			 η_min = 10e-5)

	residual(X) = affine_proj(A.Ω, X) - s
	function ∇f(X, r)
		∇U = zeros(d, d)
		for i = 1:sketchsize(A)
			∇U .+= 2*r[i]*A.Ω[:,i]*A.Ω[:,i]'
		end
		return ∇U
	end

	# Init
	η = η₀
	X = randn(d,d)
	X .= X*X'	# Ensure PSD init
	Xnew = copy(X)
	r = residual(X)
	rnew = copy(r)
	obj_old = Inf
	global obj = sum(r.^2)
	global cnt = 0
	for i = 1:1000
		global obj, cnt
		cnt = cnt + 1
		Xnew .= X - η .* ∇f(X, r)
		Xnew .= proj_PSD(Xnew)
		rankproj!(Xnew, k)
		rnew .= residual(Xnew)
		obj = sum(rnew.^2)
		if obj < obj_old
			η = min(η_incr * η, η_max)
			X .= Xnew
			r .= rnew
			obj_old = obj
		else
			η = max(η_decr * η, η_min)
		end
	end
	# Make Symmetric, then PSD
	return covPCA(proj_PSD(X), k)
end

function spectral_init(A, y, r; 
					   use_lambda = false)
	Ω = A.Ω
	m = length(y)
	sq_y = sqrt.(y)	# This is a problem with noise (can have negative values)
	# Modified Spectral Initialization
	Ty = 1/sqrt(3) .- exp.(-y./mean(y))
	# Ty = y
	D = Ω*(Ty .* transpose(Ω))/(2*m)
	if use_lambda
		λ = mean(sq_y)
		Σ, Z = trunc_eigen(D, r)
		Λ = (Σ .- λ)/2
		U = Z .* transpose(sqrt.(Λ))	
	else
		Σ, Z = trunc_eigen(D, r+1)
		Λ = (Σ[1:r] .- Σ[r+1])/2
		U = Z[:,1:r] .* transpose(sqrt.(Λ))	
	end
	return U
end

"""
$(SIGNATURES)

Experimental (does not work well, not sure why).

# Refence

Solving Systems of Quadratic Equations via Exponential-Type Gradient Descent Algorithm
Meng Huang and Zhiquang Xu
June 2018 (arXiv 1806.00904v1 [math.NA])
"""
function Huang2018(A, y, d, r; 
				   α = 20.0,
				   tol_ngrad = 1e-3,
				   max_its = 3000)
	@debug "Entering Huang2018"
	Ω = A.Ω
	m = length(y)
	my = mean(y)
	U = Huang2018_init(Ω, m, r, y)
	μ = 0.1 / my

	# Gradient Descent
	it = 0
	∇f = Matrix(undef, d, r)
	loss = Inf
	w = exp.(-y./(α*my))
	while (it == 0 || norm(∇f)/(d*r) > tol_ngrad) && it <= max_its 
		it += 1
		# Compute gradient ∇f(U)
		p = Ω'*U 						# m * r, rowⱼ = ωⱼᵀU 
		sk_U = rowsqnorms(p)			# vector of ‖ωⱼᵀU‖²
		weights = (sk_U - y) .* w		# with exponential weights
		∇f .= Ω * (p .* weights)		# (without the 1/m factor)
		# Update
		U .-= μ/m * ∇f
	end
	@assert all(U .!= Inf)
	return U
end
function Huang2018_init(Ω, m, r, y; α_y = 9)
	y_ss = y .* convert(Array{Float64}, y .≤ α_y*mean(y))
	Y = Ω*(y_ss .* transpose(Ω))/m
	Λ, Z = trunc_eigen(Y, r+1)
	Σ = (Λ[1:r] .- Λ[r+1])/2
	U₀ = Z[:,1:r] .* transpose(sqrt.(Σ))	
end

"""
$(SIGNATURES)
Recovery from a sketch of quadratic measurements `y`.

Experimental.
Problem: with large noise y can have negative values.

# Reference

Structured Signal Recovery from Quadratic Measurements
SPARS 2019
Kaihui Liu, Feiyu Wang and Liangtian Wan
"""
function Liu2019_AGD(A, y, d, r, μ = 1.0; 
					 max_its = 3000,
					 out_dic = nothing, 
					 use_lambda = false,
					 monotone_restart = true)

	Ω = A.Ω
	m = length(y)
	sq_y = sqrt.(y)

	# Modified Spectral Initialization
	Ty = 1/sqrt(3) .- exp.(-y./mean(y))
	# Ty = y
	D = Ω*(Ty .* transpose(Ω))/(2*m)
	if use_lambda
		λ = mean(sq_y)
		Σ, Z = trunc_eigen(D, r)
		Λ = (Σ .- λ)/2
		U = Z .* transpose(sqrt.(Λ))	# U₀ from Algorithm 1
	else
		Σ, Z = trunc_eigen(D, r+1)
		Λ = (Σ[1:r] .- Σ[r+1])/2
		U = Z[:,1:r] .* transpose(sqrt.(Λ))	# U₀ from Algorithm 1
	end
	V = copy(U)
	V_old = copy(U)

	# Gradient Descent
	η = 1.0
	it = 0
	∇L = Matrix(undef, d, r)
	loss = Inf
	while (it == 0 || norm(∇L)/(d*r) > 1e-2) && it <= max_its #TODO
		it += 1
		# Compute gradient ∇L(U) = 1/m * Σⱼ (‖ωⱼᵀU‖₂-sqrt(yᵢ))/‖ωⱼᵀU‖₂ * ωⱼωⱼᵀU
		p = Ω'*U 						# m * r, rowⱼ = ωⱼᵀU 
		rp = rownorms(p)				# vector of ‖ωⱼᵀU‖₂	
		weights = (rp - sq_y) ./ rp		# vector of (‖ωⱼᵀU‖₂-sqrt(yᵢ))/‖ωⱼᵀU‖₂
		∇L .= Ω * (p .* weights)
		V_old .= V
		# V .= U - μ/m * ∇L
		U .= U - μ/m * ∇L

		new_loss = sum((rp - sq_y).^2)
		loss = new_loss

		η_old = η
		η = if it < max_its 
			(sqrt(4η^2+1)+1)/2
		else
			(sqrt(8η^2+1)+1)/2
		end
	end
	if it == max_its
		println("Maximum number of iterations reached ($it), norm(∇L)/(dr)=$(norm(∇L)/(d*r)).")
	end
	return U
end

function proj_PSD(X::Matrix)
	@assert size(X,1) == size(X,2)
	Xs = 0.5*(X + X')

	U, Σ, V = svd(Xs)
	H = V*Diagonal(Σ)*V'
	Xf = 0.5*(Xs + H)
	Xf .= 0.5*(Xf + Xf')

	# Correct slightly if needed
	k = 1.0
	while ~isposdef(Xf)
		ν = eigmin(Xf)
		Xf .= Xf + Matrix((eps(ν) - ν * k^2)I, size(Xf)...)
		k += 1.0
	end
	return Xf
end

function rankproj!(C, k)
	# TODO use e.g. PROPACK.jl
	U, Σ, V = svd(C)
	C .= (U[:,1:k].*sqrt.(Σ[1:k])')*V[1:k, :]
end

#######################################################################
#                             Evaluation                              #
#######################################################################

"""
$(SIGNATURES)
Computes the quality of the column-space of `U` for PCA.
"""
function evaluate_pca(X, U, df = Dict(); 
					  normalize_utility = true)
	# println(size(U))
	@assert all(U .!= NaN) && all(isfinite.(U))
	d, n = size(X)
	k = size(U, 2)
	Uon, _, _ = svd(U)

	if d<10000
		evaluate_pca_dd(X, Uon, k, n, df, normalize_utility)
	elseif k*n < 2e8	# if storing kn floats is ok, but d^2 isn't
		evaluate_pca_kn(X, Uon, k, n, df, normalize_utility)
	else
		@error "cannot evaluate, d and n too large"
	end
	return df
end

# O(d^2)
function evaluate_pca_dd(X, U, k, n, df, normalize_utility)
	# If n is large and d reasonable, we should compute the d × d covariance
	C = cov(X, dims=2, corrected=false)
	df[:utility] = tr(U'*C*U)
	if normalize_utility
		Σ = svdvals(C)
		df[:utility_wrt_bestk] = df[:utility] / sum(Σ[1:k])	# best k-dim subspace
		df[:utility_wrt_totalenergy] = df[:utility] / sum(Σ)	
	end
	return df
end
# O(kn)
function evaluate_pca_kn(X, U, k, n, df, normalize_utility)
	# F = U'*X/sqrt(n)	# k × n
	F = Matrix{eltype(X)}(undef, k, n)
	pos = 1
	for b in BatchIterator(X)
		bs = size(b,2)
		F[:,pos:(pos+bs-1)] = U'*b/sqrt(n)
		pos = pos+bs
	end
	println(typeof(F))
	println(tr(F*F'))
	quality = tr(F*F')	# k × k
	df[:utility] = quality
	if normalize_utility
		sqrtΣ = svdvals(X)
		Σ = (sqrtΣ/sqrt(n)).^2
		df[:utility_wrt_bestk] = df[:utility] / sum(Σ[1:k])	# best k-dim subspace
		df[:utility_wrt_totalenergy] = df[:utility] / sum(Σ)	
	end
	return df
end

#######################################################################
#                              Wrappers                               #
#######################################################################

"""
	CPCA(X, k; m_factor = 2.0, decoder = :RobustFactorized, decoder_kwargs = Dict(), kwargs...)
Compressive PCA. Extra kwargs will be passed to `skops_pair`.
"""
function CPCA(X, k; m_factor = 2.0,
			 		decoder = :RobustFactorized,
					decoder_kwargs = Dict(),
			 		out_dic = nothing,
					kernel_type = :UnitPCAKernel, 
					approximation_type = :random_features, 
					kwargs...)

	d = size(X, 1)
	m = Int(round(m_factor*k*d))	# Number of frequencies
	Ask, Aln = skops_pair(m, d, nothing;
					dataset = X, 
					sketching_activation = :square,
					learning_activation = :square,
					approximation_type = approximation_type, 
					kernel_type = kernel_type,
					out_dic = out_dic,
					kwargs...)
	@debug "CPCA with m=$(m) frequencies (d=$(d), k=$(k), m_factor=$(m_factor))."

	# Sketch
	s, sketching_time = @timed sketch(Ask, X)
	# Learn
	U, learning_time = @timed pca_inverse_problem(Aln, s, k;
												  decoder = decoder,
												  decoder_kwargs = decoder_kwargs,
												  out_dic = out_dic)
	# Store infos
	if !isa(out_dic, Nothing)
		out_dic[:m_factor] = m_factor
		out_dic[:m] = m
		out_dic[:k] = k
		out_dic[:decoder] = decoder
		out_dic[:kernel_type] = kernel_type
		out_dic[:sketching_time] = sketching_time
		out_dic[:learning_time] = learning_time
		for (k, v) in decoder_kwargs
			out_dic[k] = decoder_kwargs[k]
		end
	end
	return U
end

"""
$(SIGNATURES)
Generic decoding function for pca. Allows to select a decoding method via the keyword argument `decoder`.
"""
function pca_inverse_problem(A, s, k;
							 decoder = :RobustFactorized,
							 decoder_kwargs = Dict(),
							 out_dic = nothing)
	return if decoder == :CLOMPR
		t = LowRankSymFactor(A.d)
		# CLOMPR(s, k, t, A; bounds = bounds, decoder_kwargs...)
		θ, α = CLOMPR(s, k, t, A; init_method = :randn, decoder_kwargs...)
		idx = .~(α .≈ 0.0)
		if sum(idx) < length(α) 
			@warn "Using only $(sum(idx)) nnz values out of $(length(α))." 
		end
		# θ[:,idx] .* sqrt.(α[idx])'
		θ
	elseif decoder == :RobustFactorized
		RFRM_lbfgs(A, s, A.d, k;
					out_dic = out_dic, 
					decoder_kwargs...)
	elseif decoder == :Huang2018
		Huang2018(A, s, A.d, k;
					decoder_kwargs...)
	elseif decoder == :AGD
		Liu2019_AGD(A, s, A.d, k;
					out_dic = out_dic, 
					decoder_kwargs...)
	elseif decoder == :NuclearNorm_SCS
		lr_recovery_SCS(A, s, A.d, k)
	else
		error("Decoder $decoder not supported.")
	end
end

function covPCA(C, k)
	U, Σ, _ = svd(C)
	return U[:,1:k].*sqrt.(Σ[1:k])'
end
