# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

# Related paper:
#
# [1] Compressive K-means
# 	  N. Keriven, N. Tremblay, Y. Traonmilin, R. Gribonval
# 	  ICASSP 2017

#######################################################################
#                Specialization for the learning task                 #
#######################################################################

function sketchparams!(out, A::FourierSkOp, θ, prmsType::DiracsMixture)
	sketchcolumns!(out, A, θ)		
	return ∇sketchparams_Fourier(out, A.Ω)
end

function ∇sketchparams_Fourier(skθ, Ω)
	# skθ_re, skθ_im = real(skθ), imag(skθ)
	# skθ_re, skθ_im = reimviews(skθ)
	function ∇skθ!(g, y)
		g[:] = Ω*(real(skθ).*imag(y) - imag(skθ).*real(y))
		# g[:] = Ω*(skθ_re.*imag(y) - skθ_im.*real(y))
		# g[:] = Ω*(skθ_re.*y_re - skθ_im.*y_im)
	end
end

function sketchparams!(out, A::TriangleFourierSkOp, θ, prmsType::DiracsMixture)
	sketchcolumns!(out, A, θ)		
	return ∇sketchparams_Fourier_sign(out, A.Ω)
end

# Grad of triangle wave should be a square wave, 
# but we use triangle instead for efficiency (i.e. quadratic approx of sin/cos).
function sketchparams!(out, A::TriangleTweakedFourierSkOp, θ, prmsType::DiracsMixture)
	sketchcolumns!(out, A, θ)		
	return ∇sketchparams_Fourier(out, A.Ω)
end
function ∇sketchparams_Fourier_sign(skθ, Ω)
	function ∇skθ!(g, y)
		g[:] = Ω*(sign.(real(skθ)).*imag(y) - sign.(imag(skθ)).*real(y))
	end
end

#######################################################################
#                             Evaluation                              #
#######################################################################

function evaluate_kmeans(X, C, dic = Dict(); 
				 		 compute_RSSE = false,
				 		 compute_ARI = false,
				 		 compute_kmedians_score = false,
				 		 trueclasses = nothing)

	dic[:NSSE] = NSSE(X, C)
	if compute_kmedians_score
		dic[:MEAN_KMEDIANS_ERR] = NSSE(X, C, 1)
	end
	if compute_RSSE
		k = size(C,2) 
		res = kmeans_multitrials(X, k; init = :kmpp)
		dic[:RSSE] = dic[:NSSE]/NSSE(X, res)
		if compute_kmedians_score
			dic[:REL_KMEDIANS_ERR] = dic[:MEAN_KMEDIANS_ERR]/NSSE(X, res, 1)
		end
	end
	if compute_ARI
		if isa(trueclasses, Nothing) error("Cannot compute ARI without `trueclasses`.") end
		dic[:ARI] = ARI(X, C, trueclasses)
	end
	return dic
end

function classes(X, C)
	n = size(X,2)
	c = Vector{Int}(undef, n)
	id = 1
	for bX in BatchIterator(X)
		dsts = Distances.pairwise(Euclidean(), bX, C; dims=2)
		v = vec([idx[2] for idx in argmin(dsts, dims=2)])
		c[id:(id+length(v)-1)] .= v
		id = id + length(v)
	end
	return c
end
function ARI(X, C, truth; out_dic = Dict())
	c = classes(X, C)
	idx = Clustering.randindex(c, truth)
	if out_dic ≠ nothing
		out_dic[:ARI] = idx[1]
	end
	return idx[1] # Adjusted Rand Index
end

@doc doc"""
	NSSE(X, C, p=2)

Computes the normalized sum of errors at the power p:
```math
	NSSE(X, C) = 1/n * Σ_{i=1}^n \min_{1≤j≤k} ‖xᵢ-cⱼ‖^p
```
where $C=[c₁,…,c_k]$ and $X=[x₁,…,x_n]$.
"""
function NSSE(X, C, p=2)
	n = size(X,2)
	d = size(X,1)
	k = size(C,2)
	sqNormC = sum(C.^2, dims=1) 

	NSSE = 0.
	for B in BatchIterator(X)
		sqNormB = sum(B.^2, dims=1)'
		distmat = repeat(sqNormB, 1, k) + repeat(sqNormC, size(B,2), 1) - 2*B'*C
		distmat[distmat.<0] .= 0.
		if p≠2; distmat = distmat.^(p/2); end
		NSSE = NSSE + sum(minimum(distmat, dims=2))
	end
	NSSE = NSSE/n
end

function RSSE(X, C)
	k = size(C,2) 
	res = kmeans_multitrials(X, k; init = :kmpp)
	NSSE(X, C, 2)/NSSE(X, res, 2)
end

#######################################################################
#                        Higher-level wrappers                        #
#######################################################################

"""
$(SIGNATURES)

Compressive `k`-means of the columns of `X`. Returns a matrix `C` whose columns are the centroids.

Supported keyword arguments include:
- `m_factor`: the sketch size will be `m = m_factor × k × d` (independently of the type).
- `kernel_var`: kernel variance
- `decoder` (`:CLOMPR` or `:CLAMP`)
- `decoder_kwargs`: dictionary of keyword arguments, passed to the decoder function
- `out_dic`: output dictionary, useful parameters will be stored inside when provided

Supports also all the keyword arguments of [`skops_pair`](@ref).

See also [`weighted_CKM`](@ref).
"""
CKM(X, k; kwargs...)

"""
Performs compressive k-means, but returns a tuple `(C, α)`, where:
- the columns of `C` are the centroid locations;
- `α` is a vector of weights used in the reconstruction.

This function accepts the same arguments as [`CKM`](@ref).

See also: [`CKM`](@ref), which returns only the centroids.
"""
function weighted_CKM end

function weighted_CKM(X, k; 	m_factor = 10, 
					m_factor_eq_reals = nothing, 
					approximation_type = :random_features, 
			 		kernel_var = nothing,	# Chooses σ²=kernel_var
			 		kernel_var_inter_var_ratio = nothing, # Chooses σ²=kernel_var_inter_var_ratio*inter_var
			 		kernel_var_choice = nothing, 
					sketching_activation = :sinusoid, 
					learning_activation = :sinusoid,
					sketch_scaling_factor = 1.0,
					samples_subsampling_rate = 1.0,
					samples_subsampling_method = :WOR,
					observations = :all,
					compute_NSR = false,
					simulated_NSR = 0.0,
					simulated_NSR_method = :additivenoise,
			 		decoder = :CLOMPR,
			 		decoder_kwargs = Dict(),
			 		out_dic = Dict(),
					batch_size = nothing,
			 		kwargs...)

	σ = choose_std(X; 
				   kernel_var = kernel_var, 
				   kernel_var_inter_var_ratio = kernel_var_inter_var_ratio, 
				   kernel_var_choice = kernel_var_choice, 
				   out_dic = out_dic)

	d = size(X, 1)
	# m is the number of features used (which can be real or complex)
	m = if m_factor_eq_reals != nothing
		A_tmp, _ = skops_pair(10, d, σ; dataset = X, 
							  approximation_type, sketching_activation, learning_activation, out_dic, kwargs...)
		size_eqr = size_eq_reals(sketch_eltype(A_tmp)) # typically 1.0 or 2.0
		m_used = Int(round(m_factor_eq_reals*k*d/size_eqr))	# 
		if !isa(out_dic, Nothing)
			m_eq_reals = Int(m_used * size_eqr)
			out_dic[:m_factor_eq_reals] = m_factor_eq_reals
			out_dic[:m_eq_reals] = m_eq_reals
		end
		m_used
	else
		Int(round(m_factor*k*d))		 
	end # Number of frequencies
	@debug "CKM with m=$(m) frequencies (d=$(d), k=$(k))."

	if simulated_NSR > 0.0 && simulated_NSR_method ≠ :additivenoise
		@assert sketching_activation == :sinusoid
		η = 0.4
		if simulated_NSR_method == :subsample_samples
			@assert (observations == :all || observations == m)
			samples_subsampling_rate = 1/(1+ size(X,2)*simulated_NSR*η)
			@assert samples_subsampling_rate > 0.0
			samples_subsampling_method = :Bernouilli
		elseif simulated_NSR_method == :subsample_features
			@assert samples_subsampling_rate == 1.0 
			observations = round(Int, m/(1+ size(X,2)*simulated_NSR*η))
			@assert (1 ≤ observations ≤ m)
		elseif simulated_NSR_method == :additivenoise
			@assert (samples_subsampling_rate == 1.0 && (observations == :all || observations == m))
		else
			error("Unknown method $(simulated_NSR_method) to simulate NSR.")
		end
	end
	Ask, Aln = skops_pair(m, d, σ;
							dataset = X, 
							approximation_type = approximation_type, 
						  	samples_subsampling_rate = samples_subsampling_rate,
						  	samples_subsampling_method = samples_subsampling_method,
							observations = observations,
							sketching_activation = sketching_activation,
							learning_activation = learning_activation,
							out_dic = out_dic,
							kwargs...)

	force_single_batch = (samples_subsampling_rate < 1.0 && samples_subsampling_method == :WOR)

	if out_dic ≠ nothing
		out_dic[:m_factor] = m_factor
		out_dic[:m] = m
	end

	weighted_CKM(X, k, Ask, Aln; 
				sketching_activation, learning_activation, 
				sketch_scaling_factor,
				force_single_batch, compute_NSR,
				simulated_NSR, simulated_NSR_method, )
end


"""
$(SIGNATURES)
Compressive `k`-means of `X` using sketching operators `Ask` for sketching and `Aln` for learning.

See also: [`weighted_CKM`](@ref)
"""
CKM(X, k, Ask::SketchingOperator, Aln::SketchingOperator=Ask; kwargs...)

function weighted_CKM(X, k, Ask::SketchingOperator, Aln::SketchingOperator=Ask;
					sketching_activation = :sinusoid, learning_activation = :sinusoid,
					sketch_scaling_factor = 1.0,
					force_single_batch = false, 
					compute_NSR = false,
					simulated_NSR = 0.0,
					simulated_NSR_method = :additivenoise,
			 		decoder = :CLOMPR,
			 		decoder_kwargs = Dict(),
			 		out_dic = Dict(),
					batch_size = nothing,
			 		kwargs...)

	σX = sqrt(sum(sum(b.^2) for b in centered_batch_iterator(X))/(size(X,1)*size(X,2)))
	m = sketchsize(Ask)

	# 1. Compute the sketch
	((s, bds), sketching_time) = @timed sketch_and_bounds(Ask, X; 
											force_single_batch = force_single_batch,
											batch_size = batch_size,
											out_dic = out_dic)
	# 1-bis. eventual sketch transformations
	simulate_NSR!(s, simulated_NSR, simulated_NSR_method, m)
	if (sketching_activation != learning_activation)
		s .*= quantization_renormalization(sketching_activation, learning_activation) 
	end
	raw_s = copy(s)
	if sketch_scaling_factor ≠ 1.0 # just to test
		s .*= sketch_scaling_factor
	end

 	# 2. Recover centroids and weights by solving the inverse problem
	((θ, α), learning_time) = @timed kmeans_inverse_problem(Aln, s, k; 
										bounds = bds, 
										debug_X = X,
										decoder, out_dic, decoder_kwargs, σX)

	# If a dictionary out_dic is provided, store additional infos
	if !isa(out_dic, Nothing)
		out_dic[:k] = k
		out_dic[:decoder] = decoder
		out_dic[:sketching_time] = sketching_time
		out_dic[:learning_time] = learning_time
		out_dic[:sketch_scaling_factor] = sketch_scaling_factor
		for (k, v) in decoder_kwargs
			@assert typeof(k) <: Symbol
			out_dic[k] = decoder_kwargs[k]
		end
		out_dic[:sketch_sqnorm] = sqnorm(s)
		out_dic[:simulated_NSR] = simulated_NSR
		out_dic[:simulated_NSR_method] = simulated_NSR_method
		if compute_NSR
			z_X = sketch(parentskop(Ask), X)
			out_dic[:clean_sketch_sqnorm] = sqnorm(z_X) 
			NSR_wrt_zX = sqnorm(raw_s - z_X)/sqnorm(z_X)
			out_dic[:NSR_wrt_zX] = NSR_wrt_zX
		end
		out_dic[:BLAS_num_threads] = LinearAlgebra.BLAS.get_num_threads() 
	end

	return (θ, α)
end

function CKM(args...; kwargs...)
	C, α = weighted_CKM(args...; kwargs...)
	return C
end

function simulate_NSR!(s, simulated_NSR, simulated_NSR_method, m)
	if simulated_NSR > 0.0 && simulated_NSR_method == :additivenoise
		b = randn(m) + im .* randn(m)
		b ./= norm(b)
		b .*= norm(s) * sqrt(simulated_NSR)
		s .= s + b		# Add noise
	end
end


"""
Returns a pair (θ,α) of diracs and weights matching the sketch s.

Here θ is a d×k matrix whose columns are diracs, and α is a k-dimensional vector of weights.
"""
function kmeans_inverse_problem(A, s, k ; 
								debug_X = nothing,
								decoder = :CLOMPR, 
								bounds=(-Inf,Inf),
								decoder_kwargs = Dict(),
								out_dic = Dict(),
								σX = nothing)
	t = DiracsMixture(A.d)
	if decoder == :CLOMPR
		CLOMPR(s, k, t, A;
			   bounds = bounds,
			   out_dic = out_dic,
			   decoder_kwargs...)
	elseif decoder == :CLAMP
		@debug "Calling CLAMP"
		CLAMP(s, k, A, σX; decoder_kwargs...,
			  debug_dic = Dict())
					# :truecentroids => out_dic[:non_numerical][:truecentroids],
					# :X=>debug_X))
	else
		error("Decoder $(algorithm) not supported.")
	end
end

"""
	kmeans_multitrials(X, k; trials = 3, kwargs...)
Calls multiple times kmeans and returns the best solution. Extra `kwargs` are passed to kmeans.
"""
function kmeans_multitrials(X::AbstractMatrix, k;
							trials = 3, 
							init = :kmpp,
							out_dic = nothing,
							kwargs...)
	bestC = [] 
	bestNSSE = Inf
	for i = 1:trials
		res = kmeans(X, k; init = init, kwargs...)
		itC = res.centers
		itNSSE = NSSE(X, itC)
		if itNSSE < bestNSSE
			bestC = itC
			bestNSSE = itNSSE
		end
	end
	if out_dic ≠ nothing
		out_dic[:method] = "kmeans"
		out_dic[:kmeans_trials] = trials
		out_dic[:kmeans_init] = init
		out_dic[:k] = k
		out_dic[:d] = size(X,1)
		out_dic[:n] = size(X,2)
	end
	return bestC
end
kmeans_multitrials(X, k; kwargs...) = kmeans_multitrials(X[:,:], k; kwargs...)
