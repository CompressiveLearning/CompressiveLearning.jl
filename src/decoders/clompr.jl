# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################


# Related paper:
#
# [1] Sketching for large-scale learning of mixture models
# 	  Information and Inference: A Journal of the IMA, 7:3 (2017), pp. 447--508
# 	  N. Keriven, A. Bourrier, R. Gribonval, P. Pérez
#

function tmparrays(k, A)
	outskθ = Array{sketch_eltype(A)}(undef, A.m, k)
end 

"""
$(SIGNATURES)

CL-OMP(R) algorithm. Returns a pair `(θ, α)` of parameters and weights ideally minimizing `‖Sketch(θ,α)-s‖₂`, where the sketch is computed according to `A`. where `θ` is a `prmtype.p` × `k` matrix of parameters, and `α` the length-`k` vector of associated weights.

Optional parameters:
- `replacement`: the number of iterations is set to `2k` instade of `k`. After the `k`th iteration, optimization is performed over `k`+1 atoms and the best `k` are keeped.
- `init_method`: method for picking a new atom (default: :uniform, :randn, :atzero). Option :uniform requires bounds to be provided. Supported arguments might change with the type of the parameters.
- `maxits_inloop`: maximum number of iterations for the global optimization inside the main loop (Int, default: `300`).
- `maxits_final`: maximum number of iterations for the last global optimization (Int, default: `3000`).
- `bounds`: tuple of lower bound, upper bound (default: `(-Inf,Inf)`) for the optimization parameters. Each bound can be a scalar or a vector of a dimension coherent with the `prmtype` provided. Note that when the optimization parameters correspond to multiple variables (e.g. μ and σ for GMMs), it is advised to use vector bounds as automatic vectorization might be meaningless.
- `init_bounds`: bounds used when looking for a new atom. If none are provided, `bounds` will be used as well for initialization.
- `bestatom_inits`: Number of initialization trials (best one is keeped) when picking a new atom in the first `k` steps (Int, default: `3`).
- `bestatom_inits_replacement`: Number of initialization trials (best one is keeped) when picking a new atom in the replacement steps (Int, default: `10`).
- `save_history`: Bool (default: `false`), requires `out_dic` to be provided.
- `out_dic`: Dict (default: nothing). When provided, informations about the algorithm will be stored in this dictionnary.
- `weights_update`: should be any of `:always, :onceperit, :attheend (default), :never`
- `weights_update_method`: should be any of `:normalize, :project (default)`, where `:project` means projection on the simplex.
"""
function CLOMPR(s, k::Int, prmType, A::SketchingOperator; 
				replacement = true,
				weights_update_method = :project, 
				weights_update = :attheend, 
				bound_alpha = false,
				init_method = :uniform,
				init_bounds = (-Inf,Inf),
				init_extra_params = Dict(),
				maxits_inloop = 300,
				maxits_final = 3000,
				bounds = nothing,
				bestatom_inits = 3,
				bestatom_inits_replacement = 10,
				save_history = false,
				out_dic = nothing)

	@assert weights_update in [:always, :onceperit, :attheend, :never]
	@assert weights_update_method in [:normalize, :project]

	nbIts = replacement ? 2k : k
	memforθ = PreallocatedSupport{Float64}(prmType.p, replacement ? k+1 : k)
	θ = currentview(memforθ) 	# Work with a view. Each column is a parameter.
	α = [] 

	save_history && init_history(out_dic, k, nbIts)

	residual = copy(s)	# Initialize residual
	@info "Running CL-OMPR with $(nbIts) iterations, replacement=$(replacement)."
	outskθ1 = tmparrays(1, A) # To avoid reallocation at each iteration
	for it = 1:nbIts	# main loop

		@debug "CL-OMPR: Iteration $it."
		# 1. Pick new atom
		bestatom_init_bounds = if ((init_bounds ≠ nothing) && all(isfinite.(init_bounds)))
			if isa(out_dic, Dict)
				out_dic[:clomp_bestatom_init] = :user_provided_bounds
				out_dic[:clomp_bestatom_bounds] = maximum(abs.(init_bounds))
			end
			# TODO put a warning if vectorizing for a task other than k-means?
			# TODO More generally, maybe force vector bounds also for `bounds`?
			vectorize_bounds(init_bounds, prmType.p, 1)
		else
			if isa(out_dic, Dict)
				out_dic[:clomp_bestatom_init] = :data_bounds
			end
			bounds # If there is no specific need to use different bounds for bestatom
		end

		newatom = findbestatom!(outskθ1, A, residual, θ, prmType, bounds; 
								init_method = init_method,
								init_bounds = bestatom_init_bounds, 
								save_history = save_history,
								nb_inits = (it>k) ? bestatom_inits : bestatom_inits_replacement,
								init_extra_params = init_extra_params,
								out_dic = out_dic)
		θ = addatom!(memforθ, newatom)
		
		# 2. Replacement
		if it > k 
			removelast, θ = keep_k!(θ, memforθ, α, prmType, s, k, A) 
			if removelast break end  # The atom we just added is the worst, no need to continue.
		end
		outskθ = tmparrays(size(θ,2), A) 	# To avoid allocations during optim

		# 3. Recompute the weights α
		α = mincostfun_α(θ, α, A, s, prmType; bound_alpha = bound_alpha)  	# New allocation
		if weights_update == :always
			scale_α!(α, weights_update_method) end

		# 4. Global Optimization
		mincostfun_θα!(outskθ, θ, α, A, s, prmType, bounds; 
					   bound_alpha = bound_alpha,
					   maxits = maxits_inloop)
		# TODO bounds in general case are not the same (OK for kmeans)
		# println("max outskθ = $(maximum(outskθ))")

		if weights_update == :always || weights_update == :onceperit
			scale_α!(α, weights_update_method) end
		sketchparams!(outskθ, A, θ, prmType) # Just sketch, but ignore the gradient

		# 5. Update residual
		residual .= s - outskθ*α
	end

	# Optimize one last time, with more iterations.
	outskθ = tmparrays(size(θ,2), A)
	mincostfun_θα!(outskθ, θ, α, A, s, prmType, bounds; 
				   bound_alpha = bound_alpha,
				   maxits = maxits_final) 
	if weights_update ≠ :never
		scale_α!(α, weights_update_method) 
	end
	return (θ, α)
end

function scale_α!(α, method)
	if method == :normalize
		α .= α./sum(α)
	elseif method == :project
		α .= proj_simplex(α)
	else @error "Unknown projection method '$method'. Should be :normalize or :project." end
end

struct NullSketchException <: Exception end
"""
$(SIGNATURES)
Find a new atom having largest correlation with the residual
"""
function findbestatom!(outskθ, A, residual, currentPrms, prmType, bounds; 
					   init_method = :randn,
					   init_bounds = nothing,
					   save_history = false,
					   nb_inits = 1,
					   max_failed_inits = 50, 
					   init_extra_params = Dict(),
					   out_dic = nothing)
	# bestatom_obj: Computes the objective f(θ)=-Re<sketch(θ)/norm(sketch(θ)),residual>
	# 	and store its gradient (evaluated in θ) in g.
	function bestatom_obj!(g, θ)
		θmat = reshape(θ,:,1) # Really needed?
		# Sketch and Jacobian function in θ
		∇skθ! = sketchparams!(outskθ, A, θmat, prmType)
		# @debug "Typef of A: $(typeof(A))"
		@assert contains_no_nan(outskθ)
		norm_skθ = norm(outskθ)
		# @assert norm_skθ > 0
		if norm_skθ == 0
			throw(NullSketchException())
		end
		normalized_skθ = outskθ./= norm_skθ
		obj = real(-dot(normalized_skθ, residual)) 
		∇skθ!(g, obj.*normalized_skθ - residual)	
		g .= g./norm_skθ						 # Gradient
		return obj
	end
	newatom, objval = Vector(undef, prmType.p), Inf
	j = 0
	failed_inits = 0
	while (j <= nb_inits) 
		# Returns a param of same type than current (but with only 1 atom)
		x0 = init_param(currentPrms, prmType; 
						bounds = init_bounds, 
						init_method = init_method, 
						init_extra_params...)
		x0s = copy(x0) # just to save
		(lb, ub) = bounds
		# Call optimization routine
		# TODO add flag to switch silent/not
		# (newatomᵢ, objvalᵢ) = @suppress_out LBFGSBw(bestatom_obj!, x0, lb, ub; maxiter = 100)
		try
			(newatomᵢ, objvalᵢ) = LBFGSBw(bestatom_obj!, x0, lb, ub; maxiter = 100)
			if objvalᵢ < objval
				newatom .= newatomᵢ
				objval = objvalᵢ
			end
			failed_inits = 0
			j += 1
		catch e
			if isa(e, NullSketchException)
				if failed_inits > max_failed_inits
					@error "Init failed, and failed_inits reached the maximum number of trials ($(failed_inits))"
					break
				else
					failed_inits += 1
					continue
				end
			else 
				rethrow(e)
			end
		end
		# @debug "out of try/catch"
	end
	save_history && save_bestatom_history(A, residual, prmType, bounds, 
										  out_dic, currentPrms, newatom)
	return newatom
end

"""
$(SIGNATURES)
Returns a new atom.
- `bounds` should be a tuple (of scalars or d-dimensional vectors) of bounds for the data.
- `init_method` should be any of `:uniform` (default), `:atzero`, `:randn`.
"""
function init_param(currentPrms, prmType::DiracsMixture; 
					init_method = :uniform, 
					bounds=(-Inf,Inf))

	if init_method == :atzero
		return zeros(prmType.p, 1)
	elseif init_method == :uniform
		lb, ub = bounds
		@assert all(isfinite.(lb)) && all(isfinite.(ub))
		return lb + (ub - lb).*rand(prmType.p,1)
	elseif init_method == :randn
		return randn(prmType.p, 1)
	else
		error("Invalid initialization strategy '$init_method'.")
	end
end

"""
$(SIGNATURES)
Optimizes the cost function (θ,α) ↦ ‖Sketch(θ,α)-s‖.
boundsθ can be a bound for one parameter, it will be expanded
"""
function mincostfun_θα!(outskθ, θ, α, A, s, prmType, boundsθ; 
						bound_alpha = false,
						maxits = 300)
	(p, k) = size(θ)
	r = similar(s)
	szθ = k*p
	function getviews(g, x)
		opt_θ = reshape((@view x[1:szθ]), p, k)
		opt_α = @view x[szθ+1:end]
		∇θ = reshape((@view g[1:szθ]), p, k)
		∇α = @view g[szθ+1:end]
		return (opt_θ, opt_α, ∇θ, ∇α)
	end
	# Function computing the objective to minimize and associated gradient
	function global_obj!(g, x)
		(opt_θ, opt_α, ∇θ, ∇α) = getviews(g, x)
		# Sketch current parameters
		∇skθ! = sketchparams!(outskθ, A, opt_θ, prmType)
		r .= s - outskθ*opt_α
		obj = real(dot(r,r))			# objective = ‖s-Sketch(θ,α)‖²
		# Compute gradients
		∇skθ!(∇θ, r) 
		∇θ[:] = -2*opt_α'.*∇θ 
		∇α[:] = -2*real(outskθ'*r)		# The sign here depends on the residual
		return obj
	end
	# Compute bounds
	(lb, ub) = vectorize_bounds(boundsθ, p, k)
	min_α = 1.0/(50*k)
	lb = [vec(lb); repeat([(bound_alpha ? min_α : 0.0)], outer=(k,1))] # Add bounds for α
	ub = [vec(ub); repeat([(bound_alpha ? 1. : Inf)], outer=(k,1))]
	# Optimization is performed jointly over θ and α
	x0 = [vec(θ); α]
	# TODO stacktraces disappear when using suppress_out!?
	# (xf, _) = @suppress_out LBFGSBw(global_obj!, x0, lb, ub; maxiter = maxits)
	(xf, _) = LBFGSBw(global_obj!, x0, lb, ub; maxiter = maxits)
	θ .= reshape(xf[1:szθ], p, k)
	α .= xf[szθ+1:end]
	return
end

function vectorize_bounds((lb, ub)::Tuple, repeat_dim1, repeat_dim2)
	if (lb isa Number) lb = repeat([lb], outer=repeat_dim1) end # Expand if only 1 given
	if (ub isa Number) ub = repeat([ub], outer=repeat_dim1) end
	lb = repeat(lb, outer=(1,repeat_dim2)) # Same bounds for every param. of the mixture
	ub = repeat(ub, outer=(1,repeat_dim2))
	@assert size(lb) == (repeat_dim1, repeat_dim2)
	(lb, ub)
end

"""
$(SIGNATURES)
Optimizes the cost function α ↦ ‖Sketch(θ,α)-s‖, for fixed θ.
This is a (convex) nonnegative least squares problem.
See also mincostfun_θα which optimizes the same cost function, but jointly over θ and α.
"""
function mincostfun_α(θ, α, A, s, prmType;
					  bound_alpha = false,
					  maxits = 300)
	(p, it) = size(θ)
	(skθ, _) = sketchparams(A, θ, prmType)
	# Function computing the objective and gradient
	function global_obj!(g, α)
		r = vec(skθ*α - s)	# Residual
		obj = sqnorm(r)
		g .= 2*real(skθ'*r)
		return obj
	end
	min_α = 1.0/(100*it)
	(lb, ub) = ((bound_alpha ? min_α : 0.0)*ones(it,1), (bound_alpha ? 1.0 : Inf)*ones(it,1))	# ∀i, 0≤α_i≤1
	α0 = 1/it*ones(it)
	# (αf, _) = @suppress_out LBFGSBw(global_obj!, α0, lb, ub; maxiter = maxits)
	(αf, _) = LBFGSBw(global_obj!, α0, lb, ub; maxiter = maxits)
	return αf
end

"""
$(SIGNATURES)
Reduces θ to its k best atoms.
"""
function keep_k!(θ, memforθ, α, prmType, s, k, A)
	(skθ, _) = sketchparams(A, θ, prmType)
	skθn = skθ./sum(real(skθ).^2+imag(skθ).^2, dims=1)
	# β = vec(skθn\s) 			# Correlations
	β = vec([real(skθn); imag(skθn)]\[real(s);imag(s)])		# Correlations
	# TODO NNLSQ
	idxsmallest = sortperm(β)[1]	# Index of least correlated atom
	(idxsmallest == memforθ.k, removeatom!(memforθ, idxsmallest))
end

#######################################################################
#                           Plots for debug                           #
#######################################################################

function init_history(out_dic, k, nbIts)
	@debug "Saving history."
	@assert ~(out_dic == nothing)
	out_dic[:history] = Dict()
	out_dic[:history][:k] = k
	out_dic[:history][:nbIts] = nbIts
	out_dic[:history][:bestatom_history] = []
	out_dic[:history][:prms_beforebestatom] = []
	out_dic[:history][:newatoms] = []
end

"""
Computes (x,y,z) for heatmaps of the "bestatom".
"""
function heatmap_bestatom_2d(A, residual, prmType::DiracsMixture, bounds)
	function bestatom_obj(θ)
		(skθ, _) = sketchparams(A, θ, prmType)
		norm_skθ = norm(skθ)
		normalized_skθ = skθ./norm_skθ
		obj = real(-dot(normalized_skθ, residual))
		return obj
	end
	(lb, ub) = bounds
	nstep = 20
	ar1 = range(lb[1], stop=ub[1], length=nstep)
	ar2 = range(lb[2], stop=ub[2], length=nstep)
	z = [bestatom_obj([e1, e2]) for e2 in ar2, e1 in ar1]
	(ar1, ar2, z)
end

"""
Computes (x,y,z) for heatmaps of the "bestatom".
"""
function heatmap_bestatom_2d(A, residual, prmType::GaussianMixtureDiagCov, bounds;
		newatom)
	function bestatom_obj(θ)
		(skθ, _) = sketchparams(A, θ, prmType)
		norm_skθ = norm(skθ)
		normalized_skθ = skθ./norm_skθ
		obj = real(-dot(normalized_skθ, residual))
		return obj
	end
	(lb, ub) = bounds
	nstep = 20
	ar1 = range(lb[1], stop=ub[1], length=nstep)
	ar2 = range(lb[2], stop=ub[2], length=nstep)
	@assert length(newatom) == 4
	z = [bestatom_obj([e1, e2, newatom[end-1], newatom[end]]) for e2 in ar2, e1 in ar1]
	(ar1, ar2, z)
end

function save_bestatom_history(A, residual, prmType, bounds, out_dic, currentPrms, newatom)
	@assert ~(out_dic == nothing)
	push!(out_dic[:history][:bestatom_history], heatmap_bestatom_2d(A, residual, prmType, bounds; newatom))
	push!(out_dic[:history][:prms_beforebestatom], currentPrms)
	push!(out_dic[:history][:newatoms], newatom)
end

