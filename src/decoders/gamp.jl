# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

# Relevant reference:
#
# [1] Sketched Clustering via Hybrid Approximate Message Passing
#     Evan Byrne, Antoine Chatalic, Rémi Gribonval and Philip Schniter
#     2019

# using Plots
# using JLD

####################
#  Output Channel  #
####################


mutable struct ExpSketchOut{V <: AbstractVector, T <: AbstractFloat}
	s::V
	k::Int
	g::Vector{T}		# m
	g²::Vector{T}		# m
	α::Vector{T}		# k
	τ::Vector{T}		# k
	num_std::T
	min_rep::Int
	max_rep::Int
	n₁::Int
	n₂::Int
	tuning_stepsize::Float64
	tuning_damping::Float64
end

function ExpSketchOut(s, k, g², α, τ; 
					  num_std = 4.0,
					  min_rep = 1,
					  max_rep = 10,
					  n₁ = 7,
					  n₂ = 15,
					  tuning_stepsize = 1.0,
					  tuning_damping = 0.8) # was 1.0
	g = sqrt.(g²)
	ExpSketchOut(s, k, g, g², α, τ, num_std, min_rep, max_rep, 
				 n₁, n₂, tuning_stepsize, tuning_damping)
end

#######################################################################
#                           Main Algorithm                            #
#######################################################################

eᵢ(d, i) = 1.0*[(j==i) for j in 1:d]
# Computes ‖Rφ₁‖ where the dpad-dimensional vector is R[φ₁,φ₂]
partial_radiuses(Ω, dmax) = vec(sqrt.(sum((Ω'*eᵢ(size(Ω,1), i)).^2 for i in 1:dmax)))

function normalize_freqs(Ω::FastTransform)
	L = deepcopy(Ω) # d × m
	d, m = size(L)
	pr = partial_radiuses(Ω, d)		# R ‖φ₁‖
	rads = pr ./ L.R[1:size(Ω,2)] 	# ‖φ₁‖
	g² = pr.^2
	set_radiuses!(L, 1 ./ rads)

	@assert all(isfinite.(L.R))
	@assert all(isfinite.(g²))
	return L, g²
end
function normalize_freqs(Ω::AbstractMatrix)
	L = copy(Ω) # d × m
	d, m = size(L)
	g² = vec(sum(L.^2, dims = 1))
	L = L ./ sqrt.(g²)'
	return L, g²
end

"""
$(SIGNATURES)

“Compressive Learning Approximate Message Passing” algorithm.
Recover `k` centroids from the sketch `s`.
"""
function CLAMP(s, k, skop, σX; 
			   nb_inits = 1,
			   tuning = true,
			   tuning_max_its = 10,
			   SHyGAMP_max_its = 200,
			   sketch_tol = 5e-4,
			   uniform_integration_nrep = false,
			   uniform_vars = false,
			   check_finite = true,
			   clip_bad_Qs = true,
			   τ_init = :AtZero,
			   debug = false,
			   debug_dic = Dict())

	truecentroids = (:truecentroids in keys(debug_dic)) ? debug_dic[:truecentroids] : nothing 
	X = (:X in keys(debug_dic)) ? debug_dic[:X] : nothing 
	function print_debug(C)
		println("DEBUG: mean true C=$(mean(truecentroids)) Var true C = $(var(truecentroids))")
		vC = var(C)
		println("Var est C = $(vC)")
	end
	debug && println("sketching op is a ",typeof(skop))
	debug && println("σ=",σX)

	# ===== Initialize misc. matrices & hyperparameters =====
	debug && println("std(Ω) (FREQ) =",std(skop.Ω))
	debug && println("std(Ω) (SPATIAL) =",1/std(skop.Ω))
	# L = copy(skop.Ω) # d × m
	d, m = size(skop.Ω)
	# @assert typeof(L) <: Matrix 	# TODO ft
	# g² = vec(sum(L.^2, dims = 1)) # TODO FT
	# L = L ./ sqrt.(g²)'
	L, g² = normalize_freqs(skop.Ω)
	α =	ones(k) ./ k
	τ = if (τ_init == :AtZero)
		zeros(k)
	else
		σX^2 / k * ones(k) 	
	end
	out_est = ExpSketchOut(s, k, g², α, τ)

	Qc = 4 * σX^2 .* ones(k, d) 	# factor 4 from matlab code
	
	# ===== Try multiple inits for C, keep the one minimizing ‖sketch(C)-s‖  =====
	Z 	= Matrix{Float64}(undef, k, m)
	Qz 	= Matrix{Float64}(undef, k, m)
	C = nothing
	smallest_skdist = Inf
	for init = 1:min(1, nb_inits)
		C₀ = rand(Normal(0.0, σX), k, d)	
		# SHyGAMP!(C₀, Qc, s, L, out_est, Z, Qz ; max_its = SHyGAMP_max_its)
		if skdist_secondorder(C₀, s, skop, out_est) < smallest_skdist
			C = C₀ 	
		end
	end
	# C₀ = load("fixedmat.jld", "C0")

	# ===== Run SHyGAMP and tune α,τ iteratively =====
	debug && println("INIT ok, starting iterations")
	debug && print_debug(C)
	it = 1
	sketch_reldist = Inf
	while it ≤ tuning_max_its && sketch_reldist > sketch_tol # TODO
		Qc .= 4 * σX^2 .* ones(k, d) 	# Re-init
		# Run SHyGAMP with fixed α and τ
		SHyGAMP!(C, Qc, s, L, out_est, Z, Qz ; 
				 debug_skop = skop,
				 max_its = SHyGAMP_max_its, 
				 uniform_integration_nrep = uniform_integration_nrep,
				 uniform_vars = uniform_vars,
				 clip_bad_Qs = clip_bad_Qs,
				 check_finite = check_finite,
				 debug = debug && (it==1),
				 debug_truecentroids = truecentroids,
				 debug_X = X)
		debug && print_debug(C)
		tuning && tune_hyperprms!(out_est, Z, Qz)
		sketch_reldist = skdist_secondorder(C, s, skop, out_est) 
		it += 1
	end
	# println("FINAL α: $α")
	return C', α	# C is d × k Matrix, α is a Vector of length k
end

@doc doc"""
	SHyGAMP!(C, Qc, sketch, A, out_est[, Z, Qz])

`A` must be of size d×m, `C` and `Qc` of size k×d, and `Z` and `Qz`, if provided, of size k×m.
This function modifies in place `C`, `Qc`, `Z` and `Qz`.
"""
function SHyGAMP!(C, Qc, s, A, out,
				  	Z = Matrix{Float64}(undef, size(C,1), size(A,2)),
				  	Qz = Matrix{Float64}(undef, size(C,1), size(A,2)) ; 

				 	tol = 1e-3,
					check_finite = true,
					debug = false,
				 	debug_skop = nothing,
					debug_truecentroids = nothing,
					debug_X = nothing,
					damping = 0.05, 	
				 	damping_incr = 1.0,
				 	damping_min = 0.05,
				 	damping_max = 1.0,
					uniform_vars = false,
					clip_bad_Qs = false,
					max_its = 200,
					qpmin = 1e-12, # should be small when SNR is high
					qcmin = 1e-12,
					uniform_integration_nrep = false)

	d, m = size(A)
	k, d2 = size(C)
	@assert d == d2
	d_over_m = d/m
	@assert m == length(s)

	@debug "SHyGAMP with m=$m, d=$d, k=$k."
	C_prior = ConstantPrior()

	qrmin = qcmin

	# Notations:
	# - “Q” for (co)variances
	# - “R” at the end for "Robust" (cannot get too small)
	
	# Variable Sizes :
	# - A					d×m
	# - C, Qc, R:			k×d
	# - Z, Qz, S, P: 		k×m
	# - Qp, Qs, Qr: 		k×1
	
	# ===== Initialization =====
	S = zeros(k, m)
	P 	= Matrix{Float64}(undef, k, m)
	R 	= Matrix{Float64}(undef, k, d)
	Qs 	= Vector{Float64}(undef, k) 
	Qr 	= Vector{Float64}(undef, k)
	QrR	= Vector{Float64}(undef, k) 
	Qp 	= Vector{Float64}(undef, k)
	QpR	= Vector{Float64}(undef, k)

	# ===== Damping =====
	prev_Qp = copy(Qp)
	prev_Qs = copy(Qs)
	prev_S = copy(S)
	Cd = copy(C)			# damped C
	prev_Cd = copy(Cd)

	# ===== History =====
	C_hist = copy(C)

	###########################
	debug && println("///////////////////////////////////////////")
	debug && println("INIT: NSSE of C = ",NSSE(debug_X, C'))
	debug && println("INIT: mean(C) = ",mean(C))
	debug && println("INIT: var(C) = ",var(C))
	debug && println("INIT: mean(QC) = ",mean(Qc))
	debug && println("INIT: var(QC) = ",var(Qc))
	debug && println("///////////////////////////////////////////")
	##############

	ss = damping
	i = 1
	dist_sketch = zeros(max_its)
	itdp = 3
	function print_debug(i)
		println("==== Debug $i ====")
		println("NSSE C = ",NSSE(debug_X, C'))

		println("mean(P): ",mean(P))
		println("mean(Qp): ",mean(Qp))
		println("mean(QpR): ",mean(QpR))

		println("mean(Z): ",mean(Z))
		println("mean(Qz): ",mean(Qz))

		println("mean(S): ",mean(S))
		println("mean(Qs): ",mean(Qs))

		println("mean(R): ",mean(R))
		println("mean(Qr): ",mean(Qr))
		println("mean(QrR): ",mean(QrR))

		println("mean(C): ",mean(C))
		println("var(C): ",var(C))
		println("mean(Qc): ",mean(Qc))

		println("step size: ",ss)
	end
	while true
		# if i<itdp println("==========================================") end
		doprint = (debug && (i<5 || i%50 == 1) )

		dist_sketch[i] = skdist(C, s, debug_skop)

		####################
		#  Output Channel  #
		####################
		
		##### Output Linear stage
		
		Qp .= vec(mean(Qc, dims=2))
		if uniform_vars
			Qp .= mean(Qp) * ones(k)	
		end

		if i == 1
			prev_Qp .= Qp
		end
		Qp .= ss * Qp + (1 - ss) * prev_Qp

		P .= C*A - Qp.*S			

		debug && print("from mean Qp = $(mean(Qp))…")
		QpR .= max.(Qp, qpmin)
		debug && println("… to QpR = $(mean(QpR))…")

		# ===== Store Damping infos =====
		prev_Qp .= Qp
		prev_Qs .= Qs
		prev_S .= S
		prev_Cd .= Cd

		##### Output Nonlinear stage
		estim_post_output!(Z, Qz, out, P, QpR, 
						   dbg=i<itdp, 
						   clip_bad_Qs = clip_bad_Qs,
						   check_finite = check_finite,
						   uniform_integration_nrep = uniform_integration_nrep,
						   verbose=doprint) 
		debug && println("ESTIM_POST mean(QpR)=$(mean(QpR)) to mean(Qz)=$(mean(Qz))")

		debug && print("mean Qs = $(mean(Qs))…")
		Qs .= (1.0 ./ QpR) .* (ones(k)  - vec(mean(Qz, dims=2)) ./ QpR)
		debug && println(" … to = $(mean(Qs))…")
		if uniform_vars
			Qs .= mean(Qs) .* ones(k)
		end
		debug && print("mean S = $(mean(S))…")
		S .= (Z - P)./QpR
		debug && println(" … to = $(mean(S))…")

		# ===== Damping =====
		if i == 1
			prev_Qs .= Qs
			prev_Cd .= C
		end
		Qs .= ss * Qs + (1 - ss) * prev_Qs
		S  .= ss * S  + (1 - ss) * prev_S
		Cd .= ss * C  + (1 - ss) * prev_Cd
		# i<itdp && println("mean(Qs)=$(mean(Qs)), mean/std(S)=$(mean(S))/$(std(S)) cpt. from QpR=$(mean(QpR)) and Qz=$(mean(Qz))")
		debug && println("after damping")
		debug && println("mean Qs = $(mean(Qs))…")
		debug && println("mean S = $(mean(S))…")

		debug && println("INPUT")

		###################
		#  Input Channel  #
		###################
		
		##### Input Linear stage
		# step size is not used here (it wasn't in matlab either)
		debug && print("mean Qr (inverse of Qs) = $(mean(Qr))…")
		Qr .= d_over_m ./ Qs			# optimize if uniform_vars
		debug && println(" … to $(mean(Qr))…")
		debug && print("mean QrR = $(mean(QrR))…")
		QrR .= max.(Qr, qrmin)
		debug && println(" … to $(mean(QrR))…")
		debug && print("mean R = $(mean(R))…")
		R .= Cd + Qr .* S * A'
		debug && println(" … to $(mean(R))…")

		##### Input Non-Linear stage
		C_hist .= C
		debug && print("mean C = $(mean(C)), Qc=$(mean(Qc))…")
		estim_post_input!(C, Qc, C_prior, R, QrR)
		debug && println(" … to $(mean(C)), $(mean(Qc))…")
		if uniform_vars
			Qc .= mean(Qc) .* ones(size(Qc))
		end

		# ===== Update Step Size =====
		ss = min(max(damping_incr * ss, damping_min), damping_max)

		#####################
		#  Track evolution  #
		#####################
		
		diff = norm(C - C_hist)/norm(C)

		if i ≥ max_its || diff < tol
			@debug "Stopping SHyGAMP at iteration $i, diff=$diff (tol=$tol)."
			break
		end
		doprint && print_debug(i)
		i += 1
	end
	tdsp = min(i, 5)
	return
end

skdist(C, s, A) = norm(sketch(A, C') - s)/norm(s)
skdist_weighted(C, s, A, α) = norm(sketchcolumns(A, C')*α - s)/norm(s)
function skdist_secondorder(C, s, A, out::ExpSketchOut)
	sk = sum(sketchcolumns(A, C').*(out.α'.*exp.(-0.5*out.g².*out.τ')), dims=2)
	norm(sk - s)/norm(s)
end

struct ConstantPrior end
struct MvNormalDiagCov{T}
	μ::T
	Σ::T
end

function estim_post_input!(C, Qc, p::ConstantPrior, R, Qr)
	C .= R
	Qc .= repeat(Qr, 1, size(Qc, 2))
end
function estim_post_input!(x, Qx, p::MvNormalDiagCov, R, Qr)
	g = p.Σ ./ (p.Σ .+ Qr)
	x .= g.*(R .- p.μ) .+ p.μ
	Qx .= g.*Qr
end

#######################################################################
#                Output Channel & Posterior Estimation                #
#######################################################################

"""
$(SIGNATURES)

Estimates the expectation and variance of `Z` (output channel).
"""
function estim_post_output!(Z, Qz, o::ExpSketchOut, P, Qp; 
					verbose = false,
					clip_bad_Qs = false, 
					check_finite = false,
					uniform_integration_nrep = true,
					dbg=false)	

	k, m = size(P)
	gP = P .* o.g'						# k × m
	g²Qp = Qp * o.g²' 					# k × m
	gstd = sqrt.(g²Qp)
	e_g²Qp = exp.(-g²Qp)				# k × m
	β = exp.(-0.5*o.τ*o.g²') .* o.α		# k × m
	β² = β.^2							# k × m

	# ===== Generalized Von Mises parameters =====
	μ = β.*exp.(-0.5*g²Qp).*cis.(gP)  	# k × m
	μ .= sum(μ, dims = 1) .- μ			# k × m, each row is a μ_k
	ν = (transpose(o.s) .- μ)./β					# k × m

	c2gP = cos.(2*gP)
	s2gP = sin.(2*gP)					# k × m
	η = 1 .- e_g²Qp						# k × m
	idx = η .< 1e-10
	η[idx] = g²Qp[idx]		# 1st order Taylor approx
	half_β²η = 0.5 .* β² .* η			# k × m

	# Just the contribution for each l, not the sums yet!
	Σ₁₁ = half_β²η .* (1 .- e_g²Qp.*c2gP)	# k × m
	Σ₂₂ = half_β²η .* (1 .+ e_g²Qp.*c2gP)	# k × m
	Σ₁₂ = -half_β²η .* e_g²Qp.*s2gP			# k × m
	σ₁ = sqrt.((sum(Σ₁₁, dims=1) .- Σ₁₁) ./ β²)
	σ₂ = sqrt.((sum(Σ₂₂, dims=1) .- Σ₂₂) ./ β²)
	ρ = (sum(Σ₁₂, dims=1) .- Σ₁₂) ./ (σ₁ .* σ₂ .* β²)

	# Everything k × m
	κ₁, κ₂, ζ₁, ζ₂ = prmsVonMises(real(ν), imag(ν), σ₁, σ₂, ρ)

	if check_finite
		@assert all(isfinite.(κ₁))
		@assert all(isfinite.(κ₂))
		@assert all(isfinite.(ζ₁))
		@assert all(isfinite.(ζ₂))
	end
	
	cntS = 0
	cntM = 0
	cntL = 0
	# Loop on all elements of the k × m matrices
	nrepM = []

	max_nrepM = if uniform_integration_nrep
		for iCol = 1:m, iRow = 1:k
			i = (iCol-1)k + iRow
			if gstd[i] < o.min_rep*π/o.num_std
			elseif o.min_rep*π/o.num_std ≤ gstd[i] < o.max_rep*π/o.num_std
				nrep = ceil(Int, (o.num_std*gstd[i] - π)/(2π))
				push!(nrepM, nrep)
			else
			end
		end
		isempty(nrepM) ? nothing : maximum(nrepM)
	else nothing
	end

	# println("num_rep going from $(minimum(nrepM)) to $(maximum(nrepM))")
	for iCol = 1:m, iRow = 1:k
		i = (iCol-1)k + iRow
		# println("k=$k, m=$m, iRow=$iRow, iCol=$iCol, size(Z)=$(size(Z)), size(QZ)=$(size(Qz))")

		# ===== Small g²Qp =====
		if gstd[i] < o.min_rep*π/o.num_std
			cntS += 1
			x = range(-1, 1, length = o.n₁)
			x = gP[i] .+ o.num_std.*gstd[i].*x
			logLhd = κ₁[i].*cos.(x .- ζ₁[i]) .+ κ₂[i].*cos.(2*(x .- ζ₂[i]))
			logPrior = -(x .- gP[i]).^2 .* 0.5./g²Qp[i]
			logPost = logLhd + logPrior
			offset = maximum(logPost)
			post = exp.(logPost .- offset)
			C = sum(post) + eps()

			θ = sum(x .* post) ./ C
			Qθ = sum(x.^2 .* post) ./ C - θ.^2

		# ===== Medius g²Qp =====
		elseif o.min_rep*π/o.num_std ≤ gstd[i] < o.max_rep*π/o.num_std
			cntM += 1
			# This value nrep would be common to all "medium" cases in matlab code
			nrep = uniform_integration_nrep ? max_nrepM : ceil(Int, (o.num_std*gstd[i] - π)/(2π))
			cbin = floor((gP[i] - π)/(2π)) + 1
			x = range(-π, π, length = o.n₂+1) # MODIFIED
			x = x[1:end-1]
			logLhd = κ₁[i].*cos.(x .- ζ₁[i]) .+ κ₂[i].*cos.(2*(x .- ζ₂[i]))
			x = 2*cbin*π .+ range(-(2nrep+1)π, (2nrep+1)π, length=(2nrep+1)*o.n₂+1)[1:end-1]
			logPrior =  -(x .- gP[i]).^2 .* 0.5./g²Qp[i]

			logPost = repeat(logLhd, 2nrep+1) + logPrior
			offset = maximum(logPost)
			post = exp.(logPost .- offset)
			C = sum(post) + eps()

			θ = sum(x .* post) / C
			Qθ = sum(x.^2 .* post) / C - θ.^2

		# ===== Large g²Qp =====
		else
			cntL += 1
			θ = gP[i]
			Qθ = g²Qp[i]

		end
		# Convert θ > Z
		Z[i] = θ ./ o.g[iCol]
		Qz[i] = Qθ ./ o.g²[iCol]
	end
	tot =cntS+cntM+cntL
	# println("prop small: $(cntS/tot), prop med: $(cntM/tot), prop L: $(cntL/tot)")

	if check_finite
		@assert all(isfinite.(Z))
		@assert all(isfinite.(Qz))
	end

	if clip_bad_Qs
		# Set some Qs to 0 so that the average is positive.
		Qsi = 1 ./ Qp .* (1 .- Qz ./ Qp)	# k × m
		for iRow in 1:k
			# QsiR = Qsi[iRow, :]
			QsiR = convert(Array{BigFloat}, Qsi[iRow, :]) # TODO probably slow
			@assert isfinite(sum(QsiR))
			if sum(QsiR) < 0 # Don't do anything if the mean is already positive
				Qsi_sorted = sort(QsiR, rev = true)
				csum = cumsum(Qsi_sorted)
				idx = findfirst(csum .< 0) # exists because of the conditional
				thr = Qsi_sorted[max(idx - 1, 1)]
				idxs = vec(QsiR .≤ thr)
				# println("size(Qz)=$(size(Qz)), size(Qp)=$(size(Qp))")
				# println("size(idxs)=$(size(idxs))")
				# println("$(size(idxs))")
				# println("$(size(Qz[iRow, idxs])) VS $(size(Qp[idxs]))")
				Qz[iRow, idxs] .= Qp[iRow]
				# println("sum(QsiR) AFTER = ",sum(QsiR))
			end
		end
	end

	return Z, Qz
end

"""
Computes the parameters κ₁, κ₂, ζ₁ and ζ₂ of the Von Mises distribution.
"""
function prmsVonMises(ν₁, ν₂, σ₁, σ₂, ρ)
	iσ₁² = 1 ./ σ₁.^2
	iσ₂² = 1 ./ σ₂.^2
	iOmρ² = 1 ./ (1.0 .- ρ.^2)
	ρ_σ₁σ₂ = ρ ./ (σ₁ .* σ₂)
	a₁ = ν₂ .* iσ₂² - ν₁ .* ρ_σ₁σ₂
	a₂ = ν₁ .* iσ₁² - ν₂ .* ρ_σ₁σ₂
	κ₁ = iOmρ² .* sqrt.(a₁.^2 + a₂.^2)
	κ₂ = iOmρ² .* sqrt.((.25*(iσ₁² - iσ₂²)).^2 + (.5*ρ_σ₁σ₂).^2)
	ζ₁ = atan.(a₁, a₂)
	ζ₂ = 0.5*atan.(ρ_σ₁σ₂, -0.5*(iσ₁² - iσ₂²))
	return κ₁, κ₂, ζ₁, ζ₂
end


#######################################################################
#                       Hyperparameters Tuning                        #
#######################################################################


@doc doc"""
	tune_hyperprms!(out_est::ExpSketchOut, Z, Qz)

Tunes the hyperparameters `out_est.α` and `out_est.τ` 
"""
function tune_hyperprms!(out_est::ExpSketchOut, Z, Qz,
							 stepsize_incr = 1.1,
							 stepsize_decr = 0.6,
							 stepsize_min = 1e-6, # was 1e-8 in Matlab
							 stepsize_max = 1e3,
							 max_its = 3e2,
							 tol = 1e-6,
							 max_bad_steps = 5,
							 τ_min = 0.0)

	k, m = size(Z)
	τ, α, g, g² = out_est.τ, out_est.α, out_est.g, out_est.g²

	# Work only on a subset of 1:m indices
	m_ss = min(m, 20k)
	idxs = sample(1:m, m_ss, replace = false)
	g²_ss = g²[idxs]
	g_ss = g[idxs]
	s_ss = out_est.s[idxs]
	ρ = exp.((g_ss'.*Z[:, idxs])*im - 0.5*g²_ss'.*Qz[:, idxs])	# k × m_ss

	function compute_gradients!()
		∇α .= -2*vec(mean(Qγ, dims=2))
		∇τ .= vec(α.*mean(Qγ.*g²_ss', dims=2))
	end

	function aux!(α, τ)
		# Everything is k × m_ss
		Q = exp.(-0.5*τ*g²_ss')	
		Qρ = Q .* ρ
		αQ = α .* Q
		αQρ = α .* Qρ
		ΣαQρ = sum(αQρ, dims = 1) .- αQρ
		sQρ = s_ss' .* Qρ
		Qγ .= real.(sQρ - αQ.*Q - conj(Qρ).*ΣαQρ) # For gradients
		# Objective
		mean(real(sum(-2α.*sQρ  + α.*conj(Qρ).*ΣαQρ  + αQ.^2, dims=1)))
	end
	
	# Initialize everything
	∇α, ∇τ = Vector(undef, length(α)), Vector(undef, length(τ))
	Qγ = Matrix{Float64}(undef, k, m_ss)
	obj = aux!(α, τ)	# Pre-compute Qγ, required for ∇α and ∇τ.
	old_obj = obj
	compute_gradients!()
	α_best = copy(α)	# Will store the last "good" α_new, and used for damping
	τ_best = copy(τ)
	η = out_est.tuning_stepsize
	it = 1
	bad_steps = 0
	α_diff, τ_diff = Inf, Inf

	while it < max_its && bad_steps < max_bad_steps && (α_diff > tol || τ_diff > tol)

		α_new = proj_simplex(α_best - η * ∇α)
		τ_new = max.(τ_best - η * ∇τ, τ_min)
		obj = aux!(α_new, τ_new)
		# println("Objective is $obj.")

		if obj < old_obj
			α_diff = norm(α_new - α_best)/norm(α_best)
			τ_diff = norm(τ_new - τ_best)/norm(τ_best)
			α_best .= α_new
			τ_best .= τ_new
			compute_gradients!()
			old_obj = obj
			η = min(stepsize_incr * max(η, stepsize_min), stepsize_max)
			bad_steps = 0

		else	# Fail, decrease stepsize and retry
			η = max(η * stepsize_decr, stepsize_min)
			bad_steps += 1
			if η ≈ stepsize_min
				break
			end
		end
		it += 1
	end

	@debug "Tuned for $it iterations. Changing η from $(out_est.tuning_stepsize) to $η."
	ν = out_est.tuning_damping
	α .= ν * α_best + (1-ν) * out_est.α
	τ .= ν * τ_best + (1-ν) * out_est.τ
	out_est.tuning_stepsize = η
	return
end
