# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#######################################################################
#                   Type definition and constructor                   #
#######################################################################

function nb_blocks(d, m)
	dpad = nextpow(2, d)
	b = ceil(Int, m/dpad)
	mpad = dpad*b
	return (dpad, b, mpad)
end

"""
	WHOrthoRandom(d, m, R; s=3, deterministic=nothing, gaussian_first_block)

Creates a fast transforms of the form `Ω = [B₁ᵀ, …, B_rᵀ]` with each `Bᵢ ~iid R[Π_{i=1}^s (HDᵢ)]`, where:
- `R` is a diagonal matrix with entries provided as argument on the diagonal
- `H` denotes the Walsh-Hadamard (WH) matrix
- `Dᵢ` are diagonal matrices with iid Rademacher entries. 
  When `gaussian_first_block` is set, then the first block `D₁` has Gaussian entries instead of Rademacher.

Orthogonormality holds when the radiuses are set to one.
"""
struct WHOrthoRandom{T} <: AbstractMatrix{T}
	d::Int				# Dimension
	m::Int 				# Nb. of frequencies
	gaussian_first_block::Bool
	dpad::Int			# dpad≥d, must be a power of 2
	mpad::Int			# mpad≥m, must be a multiple of dpad
	b::Int				# Nb. of blocks, = mpad/dpad
	s::Int				# Nb. of spinners used to build a block
	R::Vector{T}		# Radiuses 
	Rf::Vector{T}		# Radiuses (padded and multiplied by sqrt(dpad^s) or sqrt(dpad^(s-1)))
	D::Array{Float64, 2}	# Diagonal elements
	function WHOrthoRandom(d, m, R; 
						   s=3,
						   deterministic=nothing,
						   gaussian_first_block = false)
		@assert s==3 # Not supported yet in the C version of the code. 
		dpad, b, mpad = nb_blocks(d, m)
		if ~isa(deterministic, Nothing)
			D = convert(Array{Bool}, digits(deterministic, base=2))
			D = [D; zeros(d*s-size(D,1))]
			D = reshape(D, (mpad, s))
		else	# In the general case, we want a random D
			D = rand(Bool, mpad, s)		# Rademacher
		end
		D = 2.0*D.-1
		if gaussian_first_block
			G = randn(mpad)
			D[:,1] .= G
		end
		# zero-pad and add the normalization factor
		# Rf = Rf_from_R(gaussian_first_block, dpad, mpad, b, D, R)
		# if gaussian_first_block
			# rows_norms = repeat([sqrt(sum(D[(i-1)*dpad+1:i*dpad, 1].^2)) for i in 1:b], inner=dpad)
			# Rf = [R./(sqrt(dpad)^(s-1) * rows_norms); zeros(mpad-size(R,1))]
		# else
			# Rf = [R./(sqrt(dpad)^s); zeros(mpad-size(R,1))]
		# end
		# new{eltype(R)}(d, m, gaussian_first_block, dpad, mpad, b, s, R, Rf, D)
		Ω = new{eltype(R)}(d, m, gaussian_first_block, dpad, mpad, b, s, R, zeros(mpad), D)
		set_radiuses!(Ω, R)
		return Ω
	end
end
Base.Array(Ω::WHOrthoRandom) = (Ω'*Matrix(1.0I, size(Ω,1), size(Ω,1)))'

# Set Rf such that the block is equivalent to having norms R
function set_radiuses!(Ω::WHOrthoRandom, R) 
	if Ω.gaussian_first_block
		rows_norms = repeat([sqrt(sum(Ω.D[(i-1)*Ω.dpad+1:i*Ω.dpad, 1].^2)) for i in 1:Ω.b], inner=Ω.dpad)[1:Ω.m]
		Ω.Rf .= [R./(sqrt(Ω.dpad)^(Ω.s-1) * rows_norms); zeros(Ω.mpad-size(R,1))]
	else
		Ω.Rf .= [R./(sqrt(Ω.dpad)^Ω.s); zeros(Ω.mpad-size(R,1))]
	end
	Ω.R .= R
	nothing
end

#######################################################################
#                          Linear Operations                          #
#######################################################################

function Base.:*(Ω::Adjoint{<:Any,<:WHOrthoRandom}, X::StridedArray)
	Ωp = Ω.parent
	X = [X; zeros(Ωp.dpad-size(X,1), size(X,2))]
	l = log2(Ωp.dpad)
	n = size(X, 2)
	Y = Matrix{Float64}(undef, Ωp.mpad, n);
	c_triplespin_sequential(Y, X, Ωp.D, Ωp.Rf, l, Ωp.b, n)
	T = Y[1:Ωp.m, :]		
	# @assert(all(.~isnan.(T)))
	# @assert(all(isfinite.(T)))
	Y[1:Ωp.m, :]		# Remove padding
end

function Base.:*(Ω::WHOrthoRandom, X::StridedArray)
	X = [X; zeros(Ω.mpad-size(X,1), size(X,2))]
	l = log2(Ω.dpad)
	n = size(X, 2)
	Y = Matrix{Float64}(undef, Ω.dpad, n);
	c_triplespin_t(Y, X, Ω.D, Ω.Rf, l, Ω.b, n)
	Y[1:Ω.d, :]		# Remove padding
end
function c_triplespin_sequential(Y, X, D, R, l, b, n)
	ccall( (:triplespin_double_batch, libfht),
			Cint,
			(Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Cint, Cint, Cint),
			Y, X, D, R, l, b, n)
end
function c_triplespin_t(Y, X, D, R, l, b, n)
	ccall( (:triplespin_transpose_double_batch, libfht),
			Cint,
			(Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Cint, Cint, Cint),
			Y, X, D, R, l, b, n)
end

# Base.:*(X::Adjoint{<:Any, <:Matrix}, Ω::WHOrthoRandom) = (Ω'*X')'
Base.:*(X::StridedArray, Ω::WHOrthoRandom) = (Ω'*X')'

# function c_triplespin_sequential_wr( D, R, X)
	# d = size(X, 1)
	# l = (Int)(log2(d))
	# n = size(X, 2)
	# m = size(R, 1)
	# b = (Int)(m/d)
	# Y = Matrix{Float64}(undef, m, n);
	# c_triplespin_sequential(Y, X, D, R, l, b, n)
# end
# function c_triplespin_parallel_blocks_wr( D, R, X)
	# d = size(X, 1)
	# l = (Int)(log2(d))
	# n = size(X, 2)
	# m = size(R, 1)
	# b = (Int)(m/d)
	# Y = Matrix{Float64}(undef, m, n);
	# c_triplespin_parallel_blocks(Y, X, D, R, l, b, n)
# end
# function c_triplespin_parallel_cols_wr( D, R, X)
	# d = size(X, 1)
	# l = (Int)(log2(d))
	# n = size(X, 2)
	# m = size(R, 1)
	# b = (Int)(m/d)
	# Y = Matrix{Float64}(undef, m, n);
	# c_triplespin_parallel_cols(Y, X, D, R, l, b, n)
# end

# function c_triplespin_parallel_oneloop(Y, X, D, R, l, b, n)
	# ccall( (:triplespin_parallel_oneloop, libfht),
			# Cint,
			# (Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Cint, Cint, Cint),
			# Y, X, D, R, l, b, n)
# end
# function c_triplespin_parallel_blocks(Y, X, D, R, l, b, n)
	# ccall( (:triplespin_parallel_blocks, libfht),
			# Cint,
			# (Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Cint, Cint, Cint),
			# Y, X, D, R, l, b, n)
# end
# function c_triplespin_parallel_cols(Y, X, D, R, l, b, n)
	# ccall( (:triplespin_parallel_cols, libfht),
			# Cint,
			# (Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Cint, Cint, Cint),
			# Y, X, D, R, l, b, n)
# end
# function c_triplespin_t_parallel_cols(Y, X, D, R, l, b, n)
	# ccall( (:triplespin_t_parallel_cols, libfht),
			# Cint,
			# (Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Ref{Cdouble}, Cint, Cint, Cint),
			# Y, X, D, R, l, b, n)
# end




#######################################################################
#                               Helpers                               #
#######################################################################

Base.:display(Ω::WHOrthoRandom) = println(Ω)
Base.:display(Ω::Adjoint{<:Any,<:WHOrthoRandom}) = println(Ω)
Base.show(io::IO, Ω::WHOrthoRandom) = print(io, "Walsh-Hadamard Matrix")
Base.show(io::IO, Ω::Adjoint{<:Any,<:WHOrthoRandom}) = print(io, "Adjoint Walsh-Hadamard Matrix")

function Base.:size(Ω::WHOrthoRandom)
	return (Ω.d, Ω.m)
end
function Base.:size(Ω::Adjoint{<:Any,<:WHOrthoRandom})
	return (Ω.parent.m, Ω.parent.d)
end


#######################################################################
#                              Fastfood                               #
#######################################################################

"""
$(SIGNATURES)
Generates a vector/matrix of Rademacher-distributed (float) entries.
"""
function rademacher(args...)
	Db = rand(Bool, args...)		
	return 2.0*Db .- 1.0
end

"""
	Fastfood(d, m, radiuses)

Creates a Fastfood transforms of the form `Ω = [B₁ᵀ, …, B_rᵀ]` with each `Bᵢ ~iid RHGΠHD`, where:
- `R` is a diagonal matrix chosen such that the l₂-norms of the rows of the blocks are the one provided in `radiuses`;
- `H` is the Walsh-Hadamard (WH) matrix;
- `D` is a diagonal matrices with iid Rademacher entries on the diagonal. 

Orthogonormality holds when the radiuses are set to one.
"""
struct Fastfood{T} <: AbstractMatrix{T}
	d::Int				# Dimension
	m::Int 				# Nb. of frequencies
	dpad::Int			# dpad≥d, must be a power of 2
	mpad::Int			# mpad≥m, must be a multiple of dpad
	b::Int				# Nb. of blocks, = mpad/dpad
	R::Vector{T}		# Radiuses 
	Rf::Vector{T}		# Radiuses (padded & multiplied by sqrt(dpad^s))
	D::Vector{Float64}	# Diagonal Rademacher elements
	G::Vector{Float64}	# Diagonal Gaussian elements
	Π::Vector{Int64}	# Random permutation
	Πi::Vector{Int64}	# Inverse of Π
	function Fastfood(d, m, R; )
		dpad, b, mpad = nb_blocks(d, m)
		D = rademacher(mpad)
		G = randn(mpad)
		Π = vcat([randperm(dpad) for i in 1:b]...)
		Πi = vcat([[findfirst(Π[(i-1)*dpad+1:i*dpad] .== j) for j in 1:dpad] for i in 1:b]...)
		Ω = new{eltype(R)}(d, m, dpad, mpad, b, R, zeros(mpad), D, G, Π, Πi)
		set_radiuses!(Ω, R)
		return Ω
	end
end
function set_radiuses!(Ω::Fastfood, R)
	rows_norms = repeat([sqrt(sum(Ω.G[(i-1)*Ω.dpad+1:i*Ω.dpad].^2)) for i in 1:Ω.b], inner=Ω.dpad)[1:Ω.m]
	Ω.Rf .= [R./(sqrt(Ω.dpad) * rows_norms); zeros(Ω.mpad-size(R,1))]
	Ω.R .= R
	nothing
end

Base.:display(Ω::Fastfood) = println(Ω)
Base.:display(Ω::Adjoint{<:Any,<:Fastfood}) = println(Ω)
Base.show(io::IO, Ω::Fastfood) = print(io, "Fastfood Matrix")
Base.show(io::IO, Ω::Adjoint{<:Any,<:Fastfood}) = print(io, "Adjoint Fastfood matrix")
Base.:size(Ω::Fastfood) = (Ω.d, Ω.m)
Base.:size(Ω::Adjoint{<:Any,<:Fastfood}) = (Ω.parent.m, Ω.parent.d)

function Base.:*(Ω::Adjoint{<:Any,<:Fastfood}, X::StridedArray)
	Ωp = Ω.parent
	X = [X; zeros(Ωp.dpad-size(X,1), size(X,2))]
	l = log2(Ωp.dpad)
	b = Ωp.b
	n = size(X, 2)
	Y = Matrix{Float64}(undef, Ωp.mpad, n);
	# multiply by D
	for i = 1:n
		for j = 1:b
			idx_block = ((j-1)*Ωp.dpad+1):(j*Ωp.dpad)
			Y[idx_block, i] .= Ωp.D[idx_block] .* X[:,i]
		end
	end
	# Multiplication by H
	fht_double_columns(Y, l, b, n)
	for i = 1:n
		for j = 1:b
			idx_block = (j-1)*Ωp.dpad+1:j*Ωp.dpad
			Btmp = Y[idx_block, i]
			# Multiplication by Π
			for k = 1:Ωp.dpad
				Y[(j-1)*Ωp.dpad+k, i] = Btmp[Ωp.Π[(j-1)*Ωp.dpad+k]]
			end
		end
	end
	# Multiplication by G
	Y .= Y .* Ωp.G 
	# Multiplication by H
	fht_double_columns(Y, l, b, n)
	# Multiplication by R
	Y .= Y .* Ωp.Rf
	# Clipping
	T = Y[1:Ωp.m, :]		
	@assert(all(.~isnan.(T)))
	@assert(all(isfinite.(T)))
	Y[1:Ωp.m, :]		# Remove padding
end

function Base.:*(Ω::Fastfood, X::StridedArray)
	X = [X; zeros(Ω.mpad-size(X,1), size(X,2))]
	l = log2(Ω.dpad)
	dpad = Ω.dpad
	n = size(X, 2)
	Y = zeros(Ω.dpad, n)
	# Multiplication by R'
	Xc = X .* Ω.Rf
	# Multiplication by H'
	fht_double_columns(Xc, l, Ω.b, n)
	# Multiplication by G'
	Xc .= Xc .* Ω.G 
	for i = 1:n
		for j = 1:Ω.b
			idx_block = (j-1)*Ω.dpad+1:j*Ω.dpad
			Btmp = Xc[idx_block, i]
			for k = 1:Ω.dpad
				Xc[(j-1)*dpad+k, i] = Btmp[Ω.Πi[(j-1)*Ω.dpad+k]]
			end
		end
	end
	# Multiplication by H'
	fht_double_columns(Xc, l, Ω.b, n)
	# Multiplication by G'
	Xc .= Xc .* Ω.D 
	for j = 1:Ω.b
		idx_block = (j-1)*Ω.dpad+1:j*Ω.dpad
		Y .+= Xc[idx_block, :]
	end
	Y[1:Ω.d, :]		# Remove padding
end

function fht_double_columns(X, l, b, n)
	ccall( (:fht_double_batch, libfht),
			Cint,
			(Ref{Cdouble}, Cint, Cint, Cint),
			X, l, b, n)
end


###### Generic ######

"""
$(TYPEDEF)
Alias type for a generic fast transforms.
"""
const FastTransform = Union{WHOrthoRandom,Fastfood} # Not super clean, but each fast transforms already extends the AbstractMatrix type. TODO Switch to traits in the future?

# Temporary, to avoid coding all the cases but at the cost of extra allocations. 
# These aliases are (normally) not used in core functions of the package.
# TODO
Base.:*(X::AbstractMatrix, Ω::FastTransform) = (Ω' * X')' 
Base.:*(X::AbstractMatrix, Ω::Adjoint{<:Any,<:FastTransform}) = (Ω' * X')' 
Base.:*(Ω::Adjoint{<:Any,<:FastTransform}, X::Adjoint) = Ω*Matrix(X) 
Base.:*(Ω::FastTransform, X::Adjoint) = Ω*Matrix(X) 


#######################################################################
#                  Structured Landmarks for Nyström                   #
#######################################################################

abstract type StructuredSquareBlock{T} end
struct WalshHadamard{T} <: StructuredSquareBlock{T} end
struct Haar{T} <: StructuredSquareBlock{T} end

# TODO: note that without indexing, we do not satisfy the AbstractArray interface…

"""
StructuredLandmarks(Y)
This is a structured matrix built from a set of fixed landmarks vector as described in [1]. Only Walsh-Hadamard structured blocks are supported for now (the Falconn FFHT implementation is used [2]).

[1] Computationally Efficient Nyström Approximation using Fast Transforms
	S. Si, C. Hsieh, I. Dhillon
	ICML 2016

[2] Practical and Optimal LSH for Angular Distance
	Alexandr Andoni, Piotr Indyk, Thijs Laarhoven, Ilya Razenshteyn and Ludwig Schmidt
	NIPS 2015
	arXiv:1509.02897
	https://github.com/FALCONN-LIB/FFHT
"""
struct StructuredLandmarks{T<:AbstractFloat, S<:StructuredSquareBlock{T}} <: AbstractMatrix{T}
	d::Int				# Dimension
	m::Int 				# Nb. of frequencies
	dpad::Int			# dpad≥d, must be a power of 2
	mpad::Int			# mpad≥m, must be a multiple of dpad
	l::Int
	b::Int				# Nb. of blocks, = mpad/dpad
	L::Matrix{T}		# Columns=landmarks ("seeds"), size d×b
	B::S		
	function StructuredLandmarks(Y; 
				type = :WalshHadamard, 
				m::Int=size(Y,2)*nextpow(2,size(Y,1)),)
		B = structured_square_block(type, eltype(Y))
		d = size(Y,1)
		b = size(Y,2) # One square block by seed
		dpad = nextpow(2, d)
		l = log2(dpad)
		mmin = (b-1)*dpad+1
		mpad = b*dpad
		Yp = [Y; zeros(eltype(Y), dpad - size(Y,1), size(Y,2))]
		if m<mmin || m>mpad
			throw(DimensionMismatch("Provided m=$m but the landmark matrix Y has only $b columns, hence m is expected to be between $mmin and $mpad (d=$d)."))
		end
		return new{eltype(Y),typeof(B)}(d, m, dpad, mpad, l, b, Yp, B)
	end
end
# StructuredLandmarks(Y; m, type::Symbol) = 
	# StructuredLandmarks(Y; m, B=structured_square_block(type, Y))
function structured_square_block(s::Symbol, ET)
	if s == :WalshHadamard
		WalshHadamard{ET}()
	else
		throw(ArgumentError("Unrecognized type '$s'. Only `:WalshHadamard` is supported for now."))
	end
end
Base.display(Ω::StructuredLandmarks) = println(Ω)
Base.display(Ω::Adjoint{<:Any,<:StructuredLandmarks}) = println(Ω)
Base.show(io::IO, Ω::StructuredLandmarks) = print(io, "StructuredLandmarks Matrix")
Base.show(io::IO, Ω::Adjoint{<:Any,<:StructuredLandmarks}) = print(io, "Adjoint of a StructuredLandmarks matrix")
Base.size(Ω::StructuredLandmarks) = (Ω.d, Ω.m)
Base.size(Ω::Adjoint{<:Any,<:StructuredLandmarks}) = (Ω.parent.m, Ω.parent.d)

function Base.:*(Ω::Adjoint{T,<:StructuredLandmarks{T, WalshHadamard{T}}}, X::StridedMatrix{T}) where {T<:AbstractFloat}
	Ωp = Ω.parent
	(Ωp.d == size(X,1)) || throw(DimensionMismatch())
	X = [X; zeros(T, Ωp.dpad-size(X,1), size(X,2))] # zero-padding
	n = size(X, 2)
	Y = repeat(X, Ωp.b) .* reshape(Ωp.L, :, 1) 
	c_fht_batch!(Y, Ωp.l, Ωp.b, n)
	return Y[1:Ωp.m, :]		# Remove padding
end

function Base.:*(Ω::StructuredLandmarks{T, WalshHadamard{T}}, X::StridedMatrix{T}) where {T<:AbstractFloat}
	(Ω.m == size(X,1)) || throw(DimensionMismatch("Cannot multiply a $(Ω.d)×$(Ω.m) StructuredLandmarks by a $(size(X)) matrix."))
	X = [X; zeros(Ω.mpad-size(X,1), size(X,2))]
	n = size(X, 2)
	c_fht_batch!(X, Ω.l, Ω.b, n)
	X = X .* reshape(Ω.L, :, 1) 
	SX = sum(X[(i-1)*Ω.dpad+1:i*Ω.dpad,:] for i in 1:Ω.b)
	return SX[1:Ω.d,:]
end
# fht_double_batch(double *restrict X, int log_n, int b, int cols);
function c_fht_batch!(X::Matrix{Float64}, l, b, n)
	ccall( (:fht_double_batch, libfht),
			Cint, # Return
			(Ref{Cdouble}, Cint, Cint, Cint), 
			X, l, b, n)
end
Base.Matrix(Ω::StructuredLandmarks) = transpose(Ω'*Matrix(1.0I, Ω.d, Ω.d))

function sq_norms_columns(Ω::StructuredLandmarks{T,WalshHadamard{T}} where T)
	repeat(sum(Ω.L .^2, dims=1), inner=(1,Ω.dpad))[1:1, 1:Ω.m]
end
sq_norms_columns(X::AbstractMatrix) = sum(X.^2, dims=1)

function Distances.pairwise(metric::SqEuclidean, Ω::StructuredLandmarks, Y::AbstractMatrix; dims=2)
	@assert dims==2
	sqΩ = transpose(sq_norms_columns(Ω))
	sqY = sq_norms_columns(Y)
	return  sqΩ .+ sqY .- 2*(Ω'*Y)
end


# TODO See if moving to traits is makes more sense in the future, or to LinearMaps.jl type
"""
LinearTransform{T}
A linear operator with elements of type `T`, which does not necessarily support indexing (contrarily to the `AbstractMatrix` interface).
"""
const LinearOperator{T} = Union{AbstractMatrix{T}, StructuredLandmarks{T,BT} where BT, WHOrthoRandom{T}, Fastfood{T}}


