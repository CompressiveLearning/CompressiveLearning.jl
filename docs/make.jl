using Documenter, DocumenterMarkdown, CompressiveLearning

makedocs(
	modules = [CompressiveLearning], 
	doctest = false,
	# format = Markdown()
	# format = Documenter.HTML(),
	sitename = "CompressiveLearning.jl",
	pages = [ 
		"Home" => "index.md",	
		"Manual" => [
			"Sketching" => "sketching.md",	
			"Learning from the sketch" => "learning.md",	
			"Learning Tasks" => "learning_tasks.md",
		],
		"API" => [
			"High-lever wrappers" => "api_highlevel.md",
			"Sketching" => "api_sketching.md",
			"Learning" => "api_learning.md",
			"Fast transforms" => "api_fasttransforms.md",
		],
		"Index" => "names_index.md",	
	]
)
# makedocs(
    # modules = [CompressiveLearning],
    # clean = false,
    # # assets = ["assets/favicon.ico"],
    # sitename = "CompressiveLearning.jl",
    # authors = "Antoine Chatalic",
    # # linkcheck = !("skiplinks" in ARGS),
    # # theme = 
    # pages = [
        # "Home" => "index.md",
    # ],
    # Documenter.HTML(
        # # Use clean URLs, unless built as a "local" build
        # # prettyurls = !("local" in ARGS),
    # ),
# )
# run(`mkdocs build`)

    # deps = Deps.pip("pygments", "mkdocs", "python-markdown-math"),

# deploydocs(
    # repo = "github.com/….git",
    # target = "build",
# )
