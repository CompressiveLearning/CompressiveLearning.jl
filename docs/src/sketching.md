# Sketching

The sketch of a dataset can be obtained by defining a [`SketchingOperator`](@ref) object, and calling the [`sketch`](@ref) function. The latter essentially takes a sketching operator and a dataset (or a distribution), and returns the corresponding sketch.

Bounds of the data can be computed in one pass (while sketching) using [`sketch_and_bounds`](@ref).

## Sketching Operators

### Type Hierarchy

Here is the main hierarchy of sketching operators used in the package.

- [`SketchingOperator`](@ref)
    - [`CompositeSkOp`](@ref)
    - [`BaseSkOp`](@ref)
        - [`MinMaxSkOp`](@ref)
        - [`LinearSkOp`](@ref)
            - [`FourierSkOp`](@ref)
            - [`QuantizedFourierSkOp`](@ref)
            - [`SqRandomSkOp`](@ref)
            - [`ComposedLinearSkOp`](@ref) 
                - [`FeaturesSubsamplingSkOp`](@ref)
                - [`SamplesSubsamplingSkOp`](@ref)
                - [`UDPAverageSkOp`](@ref)

The type [`LinearSkOp`](@ref) refers to operators computing a linear (w.r.t. probability disrtibutions) sketch of the data, i.e. each entry of the sketch will be an average over the samples of some function.

The operators subtyping [`ComposedLinearSkOp`](@ref) have a field `p` corresponding to some “parent” operator.

The constructors are however only seldom directly called. Indeed, wrappers can be used to speed-up the construction of common sketching operators.

### Wrapper constructors

Functions [`base_skop`](@ref), [`composed_skop`](@ref) (noise and/or subsampling), [`skops_pair`](@ref) can be used to easly generate common sketching operators.

Sometimes, the sketching operator used for learning is different from the one used for sketching: see [`CompressiveLearning.learning_skop`](@ref).

In order to generate differentially private sketching operators, look at [`DP_skop`](@ref).

### Generation of the random matrix

The previous wrappers will (unless specified otherwise) use default parameters to generate the random matrix. It is however possible to specify direcly a kernel from which the frequecies should be generated. See [`CompressiveLearning.drawfrequencies`](@ref), [`CompressiveLearning.drawradiuses`](@ref), but also [`GaussianKernel`](@ref), [`AdaptedRadiusKernel`](@ref), [`LaplacianKernel`](@ref).

## Defining New Sketches and Sketching Operators

New types of sketches should subtype [`CompressiveLearning.BaseSketch`](@ref) and contain a main field names `s`.
New sketching operators can be defined by subtyping [`CompressiveLearning.BaseSkOp`](@ref) or, for linear operator, directly [`LinearSkOp`](@ref).

### Linear Sketching Operators

Your type should be a subtype of `LinearSkOp`, and you should define the following functions:
- A constructor
- [`CompressiveLearning.sketch_eltype`](@ref) should return the type of the elements (e.g. `Float64` or `Complex{Float64}`) of the sketches produced by your operator.
- [`sketchcolumns`](@ref) should return a matrix whose columns are the individual sketches of the columns of `X`. 

### Generic Sketching Operators

If your operator is not linear, define directly [`sketch_onebatch`](@ref) instead of `sketchcolumns`.
