# Supported Learning Tasks

## Compressive Clustering 

The main wrapper for clustering is the [`CKM`](@ref) function.

The standard error metric for k-means is the sum of squared errors (SSE), which can be computed in its normalized form with the following function [`NSSE`](@ref). When the ground truth is known, one can also measure the adjusted Rand index with the [`ARI`](@ref) function. Multiple indicators can also be computed at once with [`evaluate_kmeans`](@ref).

## Compressive Gaussian modeling

The main wrapper for clustering is the [`CGMM`](@ref) function.
Performance can be evaluated with [`evaluate_GMM`](@ref).

## Compressive PCA (CPCA)

The main wrapper for principal component analysis is the [`CPCA`](@ref) function.
Performance can be evaluated with [`evaluate_pca`](@ref).
