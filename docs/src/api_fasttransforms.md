# Fast Transforms

## Supported types of structured matrices

```@docs
FastTransform
WHOrthoRandom
Fastfood
```

## Helper functions

```@docs
rademacher
```
