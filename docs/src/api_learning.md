# Learning API

## Supported Low-Dimensional Models

```@docs
CompressiveLearning.MixtureType
CompressiveLearning.DiracsMixture
CompressiveLearning.GaussianMixture
CompressiveLearning.LowRankSymFactor
```
## Supported decoders 

### Helper functions

```@docs
CompressiveLearning.kmeans_inverse_problem
CompressiveLearning.GMM_inverse_problem
CompressiveLearning.pca_inverse_problem
```

### Generic decoders

```@docs
CLOMPR
CLAMP
```

Here are some internal functions for CLOMPR which can potentially be useful for modification:
```@docs
CompressiveLearning.sketchparams
CompressiveLearning.sketchparams!
CompressiveLearning.init_param
CompressiveLearning.findbestatom!
CompressiveLearning.mincostfun_θα!
CompressiveLearning.mincostfun_α
CompressiveLearning.keep_k!
```

Some internal functions for CLAMP:
```@docs
CompressiveLearning.SHyGAMP!
CompressiveLearning.estim_post_output!
CompressiveLearning.prmsVonMises
CompressiveLearning.tune_hyperprms!
```


### Task-specific decoders

```@docs
CompressiveLearning.lr_recovery_SCS
CompressiveLearning.RFRM_lbfgs
CompressiveLearning.SVP
CompressiveLearning.Huang2018
CompressiveLearning.Liu2019_AGD
```

## Losses

```@docs
evaluate_pca
NSSE
```

## Reference functions to learn without compression
```@docs
kmeans_multitrials
```
