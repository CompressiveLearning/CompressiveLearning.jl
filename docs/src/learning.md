# Learning

Learning can be performed from the sketch using either directly the desired decoder, or via the helper functions [`kmeans_inverse_problem`](@ref), [`GMM_inverse_problem`](@ref), [`pca_inverse_problem`](@ref).

## Continuous OMP (CL-OMPR)

The most generic algorithm for learning from a sketch is [`CLOMPR`](@ref), which stands for “Compressive Learning Orthogonal Matching Pursuit with Replacement”.

### Adapting CL-OMP for new tasks

It can be adapted for any new relevant parameter type subtyping `MixtureType`, by defining the following functions:
- `sketchparams!`, which computes the sketch of the parameters and associated gradients (see below);
- `init_param`, which computes a new atom (column of θ) from which optimization in `findbestatom!` will start.

## Message Passing (CL-AMP)

The “Compressive learning via approximate message passing” (CL-AMP) algorithm instantiates the simplified hybrid generalized approximate message passing (SHyGAMP) algorithm to recover mixtures of diracs. See [`CLAMP`](@ref) for the documentation.

Some internal functions are described in the API.
