# Sketching API

## Sketching operators 

```@docs
SketchingOperator
BaseSkOp
MinMaxSkOp
LinearSkOp
FourierSkOp
QuantizedFourierSkOp
SqRandomSkOp
ComposedLinearSkOp
FeaturesSubsamplingSkOp
SamplesSubsamplingSkOp
UDPAverageSkOp
CompositeSkOp
```

## Types of sketches (wrappers for internal manipulation)

```@docs
CompressiveLearning.Sketch
CompressiveLearning.BaseSketch
CompressiveLearning.LinearSketch
CompressiveLearning.CompositeSketch
```

## Functions

### Functions for building sketching operators

```@docs 
base_skop
composed_skop
skops_pair
CompressiveLearning.learning_skop
DP_skop
```

### Functions for sketching

```@docs 
sketch
CompressiveLearning.sketchcolumns
CompressiveLearning.sketch_onebatch
CompressiveLearning.sketch_eltype
CompressiveLearning.sketch_and_bounds
```

### Miscellaneous

```@docs 
CompressiveLearning.sketchsize
```

## Frequency generation

```@docs 
GaussianKernel
AdaptedRadiusKernel
LaplacianKernel
CompressiveLearning.dense_isotrope
CompressiveLearning.drawfrequencies
CompressiveLearning.drawradiuses
```

