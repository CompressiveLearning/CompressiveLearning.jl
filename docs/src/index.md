# Compressive Learning 

This module provides functions to perform some statistical learning tasks on large numerical datasets by first compressing (also known as sketching) the whole dataset down to a single vector of generalized rando moments, and then learning from this compressed representations without using the initial data.

## Supported features

- Sketching with random Fourier features, quantized Fourier features.
- Differentially-private sketching operator.
- Generic [`CLOMPR`](@ref) function to solve the inverse problem (learn from the sketch).
- Wrapper function [`CKM`](@ref) for compressive clustering.
- Fast Transforms for high-dimensional settings.
- Utilities to easily run and compare different methods across whole parameter spaces.
- Support for Intel VML through the VML.jl module.
