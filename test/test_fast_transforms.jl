approx_or_low(x,y;tol=1e-10) = ((x==0 && y<tol) || (y==0 && x<tol) || x≈y)

@testset "WHOrthoRandom" begin
	b = 3
	# Check that the dense representation of Ω' has expected row norms
	r = rand(b*d)
	Ω = WHOrthoRandom(d, b*d, r)
	denseΩ = Ω'*Matrix(1.0I, d, d)
	@test sqrt.(sum(denseΩ.^2, dims=2)) ≈ r 
end

@testset "StructuredLandmarks" begin
	d = 5
	nseeds = 31
	L = randn(d, nseeds)
	Ω = StructuredLandmarks(L)
	Ωd = Ω * Matrix(1.0I, Ω.m, Ω.m)
	Ωtd = Ω' * Matrix(1.0I, d, d)
	@test size(Ωtd,1) == Ω.m
	@test all(Ωd'.≈Ωtd)
	isacolumn(x, M) = any([any(x.≈M[:,i]) for i in 1:size(M,2)])
	@test all(isacolumn(L[:,i],Ωd) for i in 1:size(L,2))
	@test_throws DimensionMismatch StructuredLandmarks(L; m=3)
	@test_throws DimensionMismatch Ω*Matrix(1.0I, Ω.d, Ω.d)
	ker = CompressiveLearning.build_kernel_kf(:Gaussian, 2.0)
	@testset "Custom kernel computations" begin
		Ωd = Ω * Matrix(1.0I, Ω.m, Ω.m)
		@test all(sum(Ωd.^2, dims=1) .≈ CompressiveLearning.sq_norms_columns(Ω))
		Y = randn(d, 4);
		metric = SqEuclidean()
		@test all(approx_or_low.(Distances.pairwise(metric, Ω, Y; dims=2), Distances.pairwise(metric, Ωd, Y; dims=2)))
		# Test custom implem using the fast transform vs KernelFunctions implem
		@test all(kernelmatrix(ker, Ω, Y; obsdim=2) .≈ kernelmatrix(ker, Ωd, Y; obsdim=2))
	end

	X = randn(d, 10^2)
	m = 17
	skop_de = NyströmSkOp(m, X, ker, linop_type = :dense)
	@test size(skop_de.L) == (d, m)
	skop_wh = NyströmSkOp(m, X, ker, linop_type = :WalshHadamard)
	@test size(skop_wh.L) == (d, m)
	@test length(sketch(skop_wh, randn(d,10))) == m
end
