# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

using Test
using CompressiveLearning
CL = CompressiveLearning
using LinearAlgebra
using ForwardDiff
using Distances
using KernelFunctions

k = 3
d = 8
n = 933
m = 33
X = randn(d, n)

@testset "CKM" begin
	C = CKM(X, k)	# Check that it runs without errors
	@test size(C) == (d, k)
end

@testset "GMM" begin
	M = CGMM(X, k)	# Check that it runs without errors
	@test length(M.components) == k
end

@testset "Privacy" begin
	# Check that it runs without errors
	C = CKM(X, k; DP_ε = 1.0, DP_δ = 1e-4, DP_γ = 0.98)
	@test size(C) == (d, k)
	C = CKM(X, k; DP_ε = 1.0, DP_γ = 0.98)
	@test size(C) == (d, k)
end

@testset "Sketching Operators" begin
	for dist in [:Chi, :AdaptedRadius]
	for linop_type in [:dense, :HDHDHD, :HGHDHD, :fastfood]
		@test size(drawfrequencies(m, d, 1.0, 
								   linop_type = linop_type,
								   radialdist = dist)) == (d, m)
	end
	end
end

@testset "Dimension 1" begin
	d = 1
	X = randn(d, n)
	C = CKM(X, k)	# Check that it runs without errors
	@test size(C) == (d, k)
end

@testset "Freqs" begin
	k = CompressiveLearning.build_kernel_kf(:Gaussian, 2.0)
	@test k(ones(10), zeros(10)) ≈ 0.2865047968601901
end

const tests = ["test_sketching", "test_fast_transforms", "test_Nyström"]

for t in tests
	@testset "Test file $t.jl" begin
		include(joinpath(@__DIR__, "$t.jl"))
	end
end


