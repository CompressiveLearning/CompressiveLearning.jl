d = 4
n = 19
m = 20
X = randn(d, n)
A = FourierSkOp(m, d, CompressiveLearning.GaussianKernel(1.0))

@testset "Batch Sketching" begin
	# Ensure we get the same results with different batch sizes
	s1 = sketch(A, X)
	s2 = sketch(A, X, 1)
	s3 = sketch(A, X, 3)
	s4 = sketch(A, X, 1298981223)
	@test s1 ≈ s2
	@test s1 ≈ s3
	@test s1 ≈ s4
end
@testset "Subsampling" begin
	# Test that the number of computed values per sample correspond to what is asked
	for i in [1, 3]
		Ass = CL.FeaturesSubsamplingSkOp(A, i)
		X = randn(d, 1)
		s = sketch(Ass, X)
		@test sum(.~(s .≈ 0.0)) == i
	end
	# Test that with all observations, we get the same thing
	Ass = CL.FeaturesSubsamplingSkOp(A, m)
	X = randn(d, n)
	@test all(sketch(Ass, X) .≈ sketch(A, X))
end
