# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

k = 3
d = 8
n = 933
m = 33
X = randn(d, n)

C = CKM(X, k; approximation_type = :Nyström)
@test size(C) == (d, k)
@test_throws ArgumentError CKM(X, k; approximation_type = :ShouldBeUnrecognized)
@test_throws ArgumentError CKM(X, k; approximation_type = :Nyström, Nyström_sampling_type = :ShouldBeUnrecognized)

function auto_gradient(A, M)
	out    = Array{CL.sketch_eltype(A)}(undef, A.m, 1) # Tmp for one param
	sketchp!(out, θ) = CL.sketchparams!(out, A, θ, M)
	onejac(θ, y) = ForwardDiff.jacobian(sketchp!, out, θ)'*y
	finalf(θ, y) = hcat([onejac(reshape(θ[(i-1)*M.p+1:i*M.p],:,1), y) for i in 1:k]...)
	return finalf
end
function both_grads(A, θ, M, y)
	(_, ∇skθ!) = CL.sketchparams(A, θ, M)
	g_manual = zeros(M.p, k)
	∇skθ!(g_manual, y)
	auto_grad = auto_gradient(A, M)
	g_auto = auto_grad(θ, y)
	return g_manual, g_auto
end

@testset "Jacobians" begin
	@testset "Clustering" begin
		θ = randn(d, k)
		M = CL.DiracsMixture(d)
		# Nyström
		A = NyströmSkOp(CL.GaussianKernel(2.0), randn(d,m))
		y = randn(CL.sketchsize(A))
		g_manual, g_auto = both_grads(A, θ, M, y)
		@test all(g_manual .≈ g_auto)
		# Fourier (TODO deal with complex output)
		# A = FourierSkOp(m, d, GaussianKernel(2.0))
		# y = randn(CL.sketchsize(A))
		# g_manual, g_auto = both_grads(A, θ, M, y)
		# @test all(g_manual .≈ g_auto)
	end
	@testset "GMM" begin
		θ = vcat(randn(d, k), rand(d,k))
		M = CL.GaussianMixtureDiagCov(2d, d)
		# Nyström
		A = NyströmSkOp(CL.GaussianKernel(2.0), randn(d,m))
		y = randn(CL.sketchsize(A))
		g_manual, g_auto = both_grads(A, θ, M, y)
		@test all(g_manual .≈ g_auto)
		# TODO Fourier (deal with complex numbers)
	end
end
