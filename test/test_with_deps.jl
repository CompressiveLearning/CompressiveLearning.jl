using Test
using CompressiveLearning
using LinearAlgebra
using PyCall
using FatDatasets

k = 5
d = 10
n = Int(1e4)
X = GMM_dataset(k, d, n)

@testset "BLESS" begin
	C = CKM_BLESS(X, k, 1.0;)	
	@test size(C) == (d, k) # Just to test that it runs
	M = CGMM_BLESS(X, k, 1.0;)	
	@test length(M.components) == k
end

@testset "DPPY leverage scores" begin
	C = CKM(X, k;
			m_factor = 1.0, 
			approximation_type = :Nyström,
			Nyström_sampling_type = :DPPY_leverage_scores)	
	@test size(C) == (d, k) # Just to test that it runs
end
