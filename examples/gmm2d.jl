# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

using CompressiveLearning
using Plots
using PyPlot
using Distributions
using GaussianMixtures
using FatDatasets
pyplot()

d = 2
k = 5
n = 5000
sep = 2.5

function plot_dataset(X)
	Plots.scatter(X[1,:], X[2,:], reuse=false)
end
function plot_history(d)
	function plot_onestep(i)
		x, y, Z = d[:bestatom_history][i]
		θ = d[:prms_beforebestatom][i]
		new = d[:newatoms][i]
		p = Plots.contour(x, y, Z, fill = true)
		Plots.scatter!(θ[1,:], θ[2,:], markersize=3, markerstrokewidth=1, color=:green, legend=false)
		Plots.scatter!(new[1,:], new[2,:], markersize=3, markerstrokewidth=1, color=:red, legend=false)
		p
	end
	Plots.plot([plot_onestep(i) for i in 1:length(d[:bestatom_history])]..., reuse = true)
end

function gmm2d(k, d, n)

	X = GMM_dataset(k, d, n; separation = sep)		# Generate dataset from GMM
	# Compressive k-means
	kvar = 7sep^2
	println("Calling CGMM")
	GMM = CGMM(X, k; 
			   m_factor = 30, 
			   kernel_var = kvar,
			   decoder_kwargs = Dict(:maxits_inloop => 8000,
			   						 :maxits_final => 10000,
									 :save_history => false,
									 # :init_method => :atzero
									 ),
			   )
	GMM_EM = CompressiveLearning.EM_GMM(X, k)

	pts = 30
	mx, Mx, my, My = minimum(X[1, :]), maximum(X[1, :]), minimum(X[2, :]), maximum(X[2, :])

	# Plot results
	μ_CGMM = hcat([e.μ for e in GMM.components]...)
	μ_EM = hcat([e.μ for e in GMM_EM.components]...)

	# Density CGMM
	p_est(x, y) = logpdf(GMM, [x; y])	
	p1 = Plots.contour(range(mx, Mx, length=pts), range(my, My, length=pts), p_est, levels=10, fill=true)
	p1 = Plots.scatter!(μ_CGMM[1,:], μ_CGMM[2,:], legend=false)
	title!("CGMM")

	# Density EM
	p_est_EM(x, y) = logpdf(GMM_EM, [x; y])	
	p3 = Plots.contour(range(mx, Mx, length=pts), range(my, My, length=pts), p_est_EM, levels=10, fill=true)
	p3 = Plots.scatter!(μ_EM[1,:], μ_EM[2,:], legend=false)
	title!("EM")

	# # True points & centroids
	p2 = Plots.scatter(X[1,:], X[2,:], markersize=1)
	p2 = Plots.scatter!(μ_CGMM[1,:], μ_CGMM[2,:])
	p = Plots.plot(p1, p2, p3, reuse=true)
	display(p)
end

gmm2d(k, d, n)
