# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

using CompressiveLearning
using FatDatasets
using Plots
gr()

d = 15
k = 5
n = 10000

function test_clamp(k, d, n)

	dsinfos = Dict()
	# Generate dataset from GMM
	sep = 2.5
	X = GMM_dataset(k, d, n, separation = sep, out_dic = dsinfos)
	σ² = FatDatasets.opt_kvar_from_sep(sep, d, k)

	#@time 
	θ = CKM(X, k;
				decoder = :CLAMP,
				linop_type = :HDHDHD, 
				kernel_var = σ²,
				out_dic = dsinfos, 
				m_factor = 4)				# Compressive k-means

	println("RSSE: ", RSSE(X, θ))	# Compute sum-of-squared errors

	# θ2 = CKM(X, k; decoder = :CLOMPR,
				   # kernel_var = σ²,
				   # m_factor = 10,
				   # out_dic = dsinfos)				# Compressive k-means
	# println("RSSE CLOMPR: ", RSSE(X, θ2))

	# Plot results
	# scatter(X[1,:], X[2,:], label="dataset", markersize=1)
	# p = scatter!(θ[1,:], θ[2,:], label="Solution")
	# display(p)
end

test_clamp(k, d, n)
