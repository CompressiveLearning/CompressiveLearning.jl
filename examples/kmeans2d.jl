# This file is part of CompressiveLearning.jl, a Julia package for 
# large-scale machine learning using sketches of generalized moments.
# Copyright (C) 2020 Antoine Chatalic
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ########################################################################

using CompressiveLearning
using FatDatasets
using Plots
gr()

d = 2
k = 5
n = 10000

function kmeans2d(k, d, n)

	X = GMM_dataset(k, d, n)		# Generate dataset from GMM
	θ = CKM(X, k)				# Compressive k-means

	println("NSSE: ",NSSE(X,θ))	# Compute sum-of-squared errors

	# Plot results
	scatter(X[1,:], X[2,:], label="dataset", markersize=1)
	p = scatter!(θ[1,:], θ[2,:], label="Solution")
	display(p)
end

kmeans2d(k, d, n)
